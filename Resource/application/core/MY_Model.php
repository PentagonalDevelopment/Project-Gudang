<?php
namespace {

    use App\Libraries\Arguments;
    use App\Libraries\MetaDataCollection;
    use App\Libraries\StringSanity;
    use App\Libraries\HookAble;
    use App\Libraries\Pass;
    use App\Model\UserRole;

    /**
     * Class MY_Model
     *
     * @property CI_DB_query_builder $db
     * @property CI_Session $session
     * @property CI_Input $input
     * @property CI_Output $output
     *
     * @property MY_URI $uri
     * @property MY_Loader $load
     *
     * @property Pass $l_pass
     * @property MetaDataCollection $l_metaDataCollection
     * @property Arguments $l_arguments
     * @property StringSanity $l_stringSanity
     * @property HookAble $l_hookAble
     * @property UserRole $l_userRole
     */
    class MY_Model extends CI_Model
    {
        /**
         * MY_Model constructor.
         * @Override
         */
        public function __construct()
        {
            parent::__construct();
            // load initial
            $this
                ->load
                ->database()
                ->helper(['date', 'url'])
                ->library(['session']);
            // set security
            $config =& get_config();
            if (empty($config['encryption_key'])
                || (is_string($config['encryption_key']) || is_numeric($config['encryption_key']))
                    && trim($config['encryption_key']) == ''
            ) {
                // set encryption key
                get_config(['encryption_key' => sha1(__DIR__)]);
            }

            $this->initBeforeLoad();
        }

        protected function initBeforeLoad()
        {
        }

        /**
         * {@inheritdoc}
         */
        public function getPackage($name)
        {
            return $this->load->getPackageFrom($name);
        }
    }
}