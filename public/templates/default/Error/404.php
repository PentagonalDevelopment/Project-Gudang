<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
$this->setArgument('title', '404 Page Not Found');
$this->headerPart();
?>
    <section class="error">
        <div class="error__inner">
            <h1>404</h1>
            <h2>The page you were looking for doesn't exist!</h2>
            <?= $this->getArgument('error_message'); ?>
        </div>
    </section>
<?php
$this->footerPart();