<?php
namespace {

    /**
     * Class MY_Security
     * Use CSRF
     */
    class MY_Security extends CI_Security
    {
        /**
         * MY_Security constructor.
         * Always Use CSRF
         */
        public function __construct()
        {
            // Is CSRF protection enabled?
            if (!config_item('csrf_protection')) {
                // CSRF config
                foreach (array('csrf_expire', 'csrf_token_name', 'csrf_cookie_name') as $key) {
                    if (null !== ($val = config_item($key))) {
                        $this->{'_' . $key} = $val;
                    }
                }

                // Set the CSRF hash
                $this->_csrf_set_hash();
                // add manually
                $expire = time() + $this->_csrf_expire;
                setcookie(
                    $this->_csrf_cookie_name,
                    $this->_csrf_hash,
                    $expire,
                    config_item('cookie_path'),
                    config_item('cookie_domain')
                );
            }
            parent::__construct();
        }

        /**
         * @param string $value
         * @return bool|CI_Security
         */
        public function verifyCSRFToken($value)
        {
            return is_string($value)
                && isset($value, $_COOKIE[$this->_csrf_cookie_name])
                && hash_equals($value, $_COOKIE[$this->_csrf_cookie_name]);
        }
    }
}
