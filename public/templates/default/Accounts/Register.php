<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
$this->headerPart();
?>
    <div id="page_forgot" class="login">
        <!-- Register -->
        <div class="login__block active" id="l-register">
            <div class="login__block__header palette-Blue bg">
                <i class="zmdi zmdi-account-circle"></i>
                Create an account
                <div class="actions actions--inverse login__block__actions">
                    <div class="dropdown">
                        <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="<?= base_url(get_instance()->__login_route_path);?>">Already have an account?</a>
                            <a class="dropdown-item" href="<?= base_url(get_instance()->__forgot_route_path);?>">Forgot password?</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .login__block__header -->
            <div class="login__block__body"><?php
                    generateBlockForm(
                        (array) $this->getPackageFrom(\App\Factory\Form\Register::class)->getFormDefinition(),
                        'zmdi-plus'
                    );
                ?></div>
            <!-- .login__block__body -->
        </div>
        <!-- .login__block -->
    </div>
<?php
$this->footerPart();
