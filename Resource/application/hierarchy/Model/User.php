<?php
namespace App\Model;

use App\Factory\VerifyRandom;
use App\Libraries\Loader;
use App\Libraries\MetaDataCollection;
use App\Libraries\Pass;
use App\Libraries\UserMeta;
use App\Model\Base\ModelDbAbstract;
use Exception;

/**
 * Class User
 * @package App\Model
 */
class User extends ModelDbAbstract
{
    /* --------------------------------------------------
     * DEFINITION
     * ----------------------------------------------- */
    // users table
    const TABLE_NAME = 'users';

    const COLUMN_ID             = 'id';
    const COLUMN_USER_NAME      = 'user_name';
    const COLUMN_EMAIL          = 'email';
    const COLUMN_FIRST_NAME     = 'first_name';
    const COLUMN_LAST_NAME      = 'last_name';
    const COLUMN_PASSWORD       = 'password';
    const COLUMN_PUBLIC_TOKEN   = 'public_token';
    const COLUMN_PRIVATE_TOKEN  = 'private_token';
    const COLUMN_PROPERTY       = 'property';
    const COLUMN_STATUS         = 'status';
    const COLUMN_LEVEL          = 'level';
    const COLUMN_CREATED_AT     = 'created_at';
    const COLUMN_UPDATED_AT     = 'updated_at';

    /* --------------------------------------------------
     * PASSWORD RESULT POINT
     * ----------------------------------------------- */

    const PASSWORD_SHA1 = 64;
    const PASSWORD_INVALID_DATA_TYPE = -1;
    const PASSWORD_PLAIN = 1;
    const PASSWORD_VALID = true;

    const DUPLICATE_EMAIL    = E_USER_ERROR;
    const DUPLICATE_USERNAME = E_USER_WARNING;
    const INVALID_USERNAME   = E_USER_NOTICE;
    const INVALID_EMAIL      = E_COMPILE_ERROR;
    const FAIL_SAVED         = E_ALL;
    const FAIL_CURRENT_USER  = E_USER_DEPRECATED;

    /**
     * @var string
     */
    protected $databaseTableName = self::TABLE_NAME;

    /**
     * Column that can be change able
     *
     * @var array
     */
    protected $validIdentifierChangeAble = [
        self::COLUMN_USER_NAME,
        self::COLUMN_EMAIL,
        self::COLUMN_FIRST_NAME,
        self::COLUMN_LAST_NAME,
        self::COLUMN_PASSWORD,
        self::COLUMN_PUBLIC_TOKEN,
        self::COLUMN_PRIVATE_TOKEN,
        self::COLUMN_STATUS,
        self::COLUMN_LEVEL,
        self::COLUMN_CREATED_AT,
        self::COLUMN_UPDATED_AT,
        self::COLUMN_PROPERTY,
    ];

    /**
     * Column selector that must be exists
     *
     * @var array
     */
    protected $mustBeExist = [
        self::COLUMN_USER_NAME,
        self::COLUMN_EMAIL,
    ];

    /**
     * Determine selector that has been set
     *
     * @var array
     */
    protected $dataHasSet = [];

    /**
     * Determine selector that has new value
     *
     * @var array
     */
    protected $dataHasNew = [];

    /**
     * @var array
     */
    protected $collectionData = [
        self::COLUMN_ID => null,
        self::COLUMN_USER_NAME => null,
        self::COLUMN_EMAIL => null,
        self::COLUMN_FIRST_NAME => null,
        self::COLUMN_LAST_NAME => null,
        self::COLUMN_PASSWORD => null,
        self::COLUMN_PUBLIC_TOKEN => null,
        self::COLUMN_PRIVATE_TOKEN => null,
        self::COLUMN_STATUS => null,
        self::COLUMN_LEVEL => null,
        self::COLUMN_CREATED_AT => null,
        self::COLUMN_UPDATED_AT => null,
        self::COLUMN_PROPERTY => null,
    ];

    /**
     * @var UserRole
     */
    protected $userRole;

    /**
     * @var UserMeta
     */
    protected $userMeta;

    /**
     * @var array
     */
    protected $collectionDataOriginal;

    /**
     * @var boolean
     */
    protected $isResultFromDatabase = false;

    /**
     * @var bool
     */
    protected $hasChange = false;

    /**
     * @var bool
     */
    protected $isExists = false;

    /**
     * {@inheritdoc}
     */
    protected function initBeforeLoad()
    {
        parent::initBeforeLoad();
        // sanity
        $this->databaseTableName = $this->db->dbprefix(self::TABLE_NAME);
    }

    /**
     * @param boolean $bool
     * @return $this
     */
    private function internalSetExist($bool)
    {
        $this->isExists = (bool) $bool;
        return $this;
    }

    /**
     * @param boolean $result
     * @return User
     */
    private function internalSetFromDatabaseResult($result)
    {
        $this->isResultFromDatabase = (boolean)$result;
        return $this;
    }

    /**
     * @access internal
     */
    protected function internalReset()
    {
        $this->isResultFromDatabase = false;
        $this->collectionData = [
            self::COLUMN_ID => null,
            self::COLUMN_USER_NAME => null,
            self::COLUMN_EMAIL => null,
            self::COLUMN_FIRST_NAME => null,
            self::COLUMN_LAST_NAME => null,
            self::COLUMN_PASSWORD => null,
            self::COLUMN_PUBLIC_TOKEN => null,
            self::COLUMN_PRIVATE_TOKEN => null,
            self::COLUMN_STATUS => null,
            self::COLUMN_LEVEL => null,
            self::COLUMN_CREATED_AT => null,
            self::COLUMN_UPDATED_AT => null,
            self::COLUMN_PROPERTY => null
        ];

        $this->collectionDataOriginal = null;
        $this->hasChange = false;
        $this->isExists = false;
        $this->dataHasSet = [];
        $this->dataHasNew = [];
        $this->userRole = null;
        $this->userMeta = null;
    }

    /**
     * @return UserRole
     */
    public function getRole()
    {
        if (!isset($this->userRole)) {
            $this->userRole = UserRole::fromUser($this);
        }

        return $this->userRole;
    }

    /**
     * @return UserMeta|UserMeta[]
     */
    public function getMeta()
    {
        if (!isset($this->userMeta)) {
            $this->userMeta = UserMeta::fromUser($this);
        }

        return $this->userMeta;
    }

    /**
     * @param string $password
     * @return bool
     */
    protected function internalValidatePassword($password)
    {
        if ($this->getPackage(Pass::class)->isMaybeHash($password)) {
            return self::PASSWORD_VALID;
        }

        if (!is_string($password)) {
            return self::PASSWORD_INVALID_DATA_TYPE;
        }

        if (strlen($password) === 40) {
            if (!preg_match('/[^a-f0-9]/', $password)) {
                return self::PASSWORD_SHA1;
            }
        }

        return self::PASSWORD_PLAIN;
    }

    /**
     * @param array $args
     * @return User
     */
    public function newInstance(array $args = [])
    {
        $object = clone $this;
        $object->internalReset();
        foreach ($args as $key => $value) {
            $object->dataHasNew[$key] = true;
            $object->collectionData[$key] = $value;
        }

        return $object;
    }

    /**
     * @return $this
     */
    protected function sanityResult()
    {
        $toUpdate = [];
        foreach ($this->collectionData as $key => $value) {
            $this->collectionData[$key] = $this->unSerial($value);
            switch ($key) :
                case self::COLUMN_PASSWORD:
                    $needUpdatePassword = $this->internalValidatePassword($this->collectionData[$key]);
                    if ($needUpdatePassword === self::PASSWORD_INVALID_DATA_TYPE) {
                        // automatically create password
                        $toUpdate[$key] = $this
                            ->getPackage(Pass::class)
                            ->hash(sha1($this->createRandomStringUniqueSha1()));
                        $this->collectionData[$key] =  $toUpdate[$key];
                    } elseif ($needUpdatePassword === self::PASSWORD_PLAIN) {
                        $toUpdate[$key] =$this
                            ->getPackage(Pass::class)
                            ->hash(sha1($this->collectionData[$key]));
                        $this->collectionData[$key] =  $toUpdate[$key];
                    } elseif ($needUpdatePassword === self::PASSWORD_SHA1) {
                        $toUpdate[$key] = $this
                            ->getPackage(Pass::class)
                            ->hash($this->collectionData[$key]);
                        $this->collectionData[$key] =  $toUpdate[$key];
                    }
                    break;
                case self::COLUMN_PRIVATE_TOKEN:
                case self::COLUMN_PUBLIC_TOKEN:
                    if (!is_string($this->collectionData[$key])
                        || strlen($this->collectionData[$key]) <> 128
                        || preg_match('/[^a-f0-9]/', $this->collectionData[$key])
                    ) {
                        $this->collectionData[$key] = hash('sha512', $this->createRandomStringUniqueSha1());
                        $toUpdate[$key] = $this->collectionData[$key];
                    }
                    break;
                case self::COLUMN_USER_NAME:
                case self::COLUMN_EMAIL:
                    if (!is_string($this->collectionData[$key]) || trim($this->collectionData[$key]) == '') {
                        $this->collectionData[$key] = 'invalid_'.sha1($this->createRandomStringUniqueSha1());
                        if ($key == self::COLUMN_EMAIL) {
                            $this->collectionData[$key] = $this->collectionData[$key] .'@invalid.email';
                        }
                    }
                    if ($this->collectionData[$key] != strtolower($this->collectionData[$key])) {
                        $this->collectionData[$key] = strtolower($this->collectionData[$key]);
                        $toUpdate[$key] = $this->collectionData[$value];
                    }
                    break;
            endswitch;
        }

        if (!empty($toUpdate)) {
            $toUpdate[self::COLUMN_UPDATED_AT] = $this->collectionData[self::COLUMN_UPDATED_AT];
            $this->db->update($this->getTableName(), $toUpdate);
        }

        return $this;
    }

    /**
     * @param string $whereName
     * @param string $whereValue
     * @return array
     */
    private function internalSelect($whereName, $whereValue)
    {
        $table = $this->getTableName();
        $whereName = str_replace(
            '{table}',
            $table,
            $whereName
        );

        $get = $this
            ->db
            ->select()
            ->from($table)
            ->where($whereName, $this->serial($whereValue))
            ->get();
        $result = $get->result_array();
        $get->free_result();
        return $result;
    }

    /**
     * @return bool
     */
    public function isExists()
    {
        return $this->isResultFromDatabase() && $this->isExists;
    }

    /**
     * @return mixed
     */
    public function isResultFromDatabase()
    {
        return $this->isResultFromDatabase;
    }

    /**
     * @return MetaDataCollection
     */
    public function all()
    {
        return $this->getPackage(MetaDataCollection::class)->newInstance($this->collectionData);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->all()->toArray();
    }

    /**
     * @param int $id
     * @return bool|null|User
     */
    public function getById($id)
    {
        if (!is_numeric($id) || !is_int(abs($id))) {
            return null;
        }

        $result = $this->internalSelect(self::COLUMN_ID, $id);
        if (!empty($result[0])) {
            return $this
                ->newInstance($result[0])
                ->internalSetFromDatabaseResult(true)
                ->sanityResult()
                ->internalSetExist(true);
        }

        return $this->newInstance();
    }

    /**
     * @param string $email
     * @return bool|null|User
     */
    public function getByEmail($email)
    {
        if (!is_string($email) || trim($email) == '') {
            return null;
        }

        $result = $this
            ->internalSelect(
                $this->createLowerColumn(self::COLUMN_EMAIL),
                strtolower($email)
            );
        if (!empty($result[0])) {
            return $this
                ->newInstance($result[0])
                ->internalSetFromDatabaseResult(true)
                ->sanityResult()
                ->internalSetExist(true);
        }

        return $this->newInstance();
    }

    /**
     * @param String $userName
     * @return bool|User
     */
    public function getByUserName($userName)
    {
        if (!is_string($userName) || trim($userName) == '') {
            return null;
        }

        $result = $this
            ->internalSelect(
                $this->createLowerColumn($this->getTableName() . '.user_name'),
                strtolower($userName)
            );
        if (!empty($result[0])) {
            return $this
                ->newInstance($result[0])
                ->internalSetFromDatabaseResult(true)
                ->sanityResult()
                ->internalSetExist(true);
        }

        return $this->newInstance();
    }

    /**
     * @param String $userName
     * @return null|User
     */
    public function getByUserNameFromVerify($userName)
    {
        if (!is_string($userName) || trim($userName) == '') {
            return null;
        }

        $result = $this->internalSelect(
            'md5('.$this->createLowerColumn($this->getTableName() . '.user_name').')',
            (strlen($userName) === 32 ? strtolower($userName) : VerifyRandom::hashUserName(strtolower($userName)))
        );
        if (!empty($result[0])) {
            return $this
                ->newInstance($result[0])
                ->internalSetFromDatabaseResult(true)
                ->sanityResult()
                ->internalSetExist(true);
        }

        return $this->newInstance();
    }

    /**
     * @param $username
     * @return User|null
     */
    public static function fromUserVerify($username)
    {
        return $user = Loader::get(User::class)->getByUserNameFromVerify($username);
    }
    /**
     * @param string $key
     * @param mixed $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        if (!is_string($key)) {
            return $default;
        }

        return array_key_exists($key, $this->collectionData)
            ? $this->collectionData[$key]
            : $default;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function set($name, $value)
    {
        if (!isset($this->collectionDataOriginal) && $this->isResultFromDatabase()) {
            $this->collectionDataOriginal = $this->collectionData;
        }

        if (!is_string($name)) {
            throw new Exception('Invalid KeyName for database');
        }

        if (in_array($name, $this->validIdentifierChangeAble)) {
            if (in_array($name, [self::COLUMN_STATUS, self::COLUMN_LEVEL])) {
                if (!is_int(abs($value))) {
                    return $this;
                }
                $value = abs($value);
            }

            if ($name == self::COLUMN_USER_NAME) {
                if (!is_string($value)) {
                    throw new Exception(
                        'Username must be as a string!',
                        self::INVALID_USERNAME
                    );
                }

                $value = strtolower(trim($value));
                if ($value == '') {
                    throw new Exception(
                        'Username is empty!',
                        self::INVALID_USERNAME
                    );
                }
            }

            if ($name == self::COLUMN_EMAIL) {
                if (!is_string($value)) {
                    throw new Exception(
                        'Email must be as a string!',
                        self::INVALID_EMAIL
                    );
                }

                $value = strtolower(trim($value));
                if ($value == '') {
                    throw new Exception(
                        'Email is empty!',
                        self::INVALID_EMAIL
                    );
                }

                if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
                    throw new Exception(
                        'Email has invalid!',
                        self::INVALID_EMAIL
                    );
                }

                if ($value != $this->collectionData[$name]) {
                    $this->hasChange = true;
                }
            }

            $this->dataHasSet[$name] = true;
            $this->collectionData[$name] = $value;
        }

        return $this;
    }

    /**
     * @param string $by
     * @param mixed $value
     * @param array $resultOriginal
     * @return bool
     */
    private function updateData($by, $value, array $resultOriginal = null)
    {
        $this->collectionData[self::COLUMN_UPDATED_AT] =
            isset($this->collectionData[self::COLUMN_UPDATED_AT])
            || @strtotime($this->collectionData[self::COLUMN_UPDATED_AT]) < $this->pastTimeInteger()
                ? $this->collectionData[self::COLUMN_UPDATED_AT]
                : $this->timeSQLNow();

        if (isset($this->collectionData[self::COLUMN_CREATED_AT])) {
            $this->collectionData[self::COLUMN_CREATED_AT] =
                @strtotime($this->collectionData[self::COLUMN_CREATED_AT]) < $this->pastTimeInteger()
                ? $resultOriginal[self::COLUMN_CREATED_AT]
                : $this->collectionData[self::COLUMN_CREATED_AT];
        }

        if (empty($this->dataHasSet) && !$this->isResultFromDatabase()) {
            $dataNew = $this->collectionData;
            $this->dataHasSet = array_intersect_key($this->dataHasNew, $dataNew);
        }

        $data = [];
        foreach ($this->collectionData as $key => $v) {
            if (in_array($key, $this->validIdentifierChangeAble)
                && isset($this->dataHasSet[$key])
            ) {
                if ($key === self::COLUMN_PASSWORD && $v !== $this->collectionDataOriginal[self::COLUMN_PASSWORD]) {
                    $data[$key] = $this->getPackage(Pass::class)->hash(sha1($this->serial($v)));
                    continue;
                }
                $data[$key] = $this->serial($v);
            }
        }

        if (!isset($data[self::COLUMN_UPDATED_AT])) {
            $data[self::COLUMN_UPDATED_AT] = $this->timeSQLNow();
        }

        return $this->db->update(
            $this->getTableName(),
            $data,
            [$by => $value]
        );
    }

    public function save()
    {
        if ($this->hasChange) {
            if ($this->isResultFromDatabase()) {
                return $this->updateData(
                    self::COLUMN_ID,
                    $this->collectionDataOriginal[self::COLUMN_ID],
                    $this->collectionDataOriginal
                );
            }
        }

        if (isset($this->collectionData[self::COLUMN_USER_NAME]) && $this->collectionData[self::COLUMN_EMAIL]) {
            $get = $this
                ->db
                ->select()
                ->from($this->getTableName())
                ->where(
                    $this->createLowerColumn(self::COLUMN_USER_NAME),
                    $this->escape($this->collectionData[self::COLUMN_USER_NAME])
                )
                ->or_where(
                    $this->createLowerColumn(self::COLUMN_EMAIL),
                    $this->escape($this->collectionData[self::COLUMN_EMAIL])
                )
                ->get();
            $result = $get->result_array();
            $get->free_result();
            $count = count($result);
            if ($count > 1) {
                foreach ($result as $value) {
                    if ($value[self::COLUMN_USER_NAME] == $this->collectionData[self::COLUMN_USER_NAME]
                        && $value[self::COLUMN_EMAIL] != $this->collectionData[self::COLUMN_EMAIL]
                    ) {
                        throw new Exception(
                            'There was duplicate of data! email has exists on another user!',
                            self::DUPLICATE_EMAIL
                        );
                    }

                    if ($value[self::COLUMN_USER_NAME] != $this->collectionData[self::COLUMN_USER_NAME]
                        && $value[self::COLUMN_EMAIL] == $this->collectionData[self::COLUMN_EMAIL]
                    ) {
                        throw new Exception(
                            'There was duplicate of data! username has exists to another user!',
                            self::DUPLICATE_USERNAME
                        );
                    }
                }

                throw new Exception(
                    'There was duplicate of data! username or email!'
                );
            }

            if ($count === 1) {
                return $this->updateData(self::COLUMN_ID, $result[0][self::COLUMN_ID], $result);
            }
            /**
             * @var Pass $passHash
             */
            $passHash = $this->getPackage(Pass::class);
            $password = isset($this->collectionData[self::COLUMN_PASSWORD])
                ? $passHash->hash(sha1($this->serial($this->collectionData[self::COLUMN_PASSWORD])))
                : $passHash->hash(sha1($this->createRandomStringUniqueSha1()));

            $data = $this->collectionData;
            $data[self::COLUMN_PASSWORD] = $password;
            $data[self::COLUMN_PRIVATE_TOKEN] = isset($this->collectionData[self::COLUMN_PRIVATE_TOKEN])
                ? $this->collectionData[self::COLUMN_PRIVATE_TOKEN]
                : hash('sha512', $this->createRandomStringUniqueSha1());
            $data[self::COLUMN_PUBLIC_TOKEN]  = isset($this->collectionData[self::COLUMN_PUBLIC_TOKEN])
                ? $this->collectionData[self::COLUMN_PUBLIC_TOKEN]
                : hash('sha512', $this->createRandomStringUniqueSha1());

            if (empty($this->dataHasSet) && !$this->isResultFromDatabase()) {
                $dataNew = $this->collectionData;
                $this->dataHasSet = array_intersect_key($this->dataHasNew, $dataNew);
            }

            $this->dataHasSet[self::COLUMN_PASSWORD] = true;
            $this->dataHasSet[self::COLUMN_PUBLIC_TOKEN] = true;
            $this->dataHasSet[self::COLUMN_PRIVATE_TOKEN] = true;

            foreach ($data as $key => $value) {
                if (! in_array($key, $this->validIdentifierChangeAble)
                    || !isset($this->dataHasSet[$key])
                ) {
                    unset($data[$key]);
                    continue;
                }
                if ($key == self::COLUMN_USER_NAME) {
                    if (!is_string($value)) {
                        throw new Exception(
                            'Username must be as a string!',
                            self::INVALID_USERNAME
                        );
                    }

                    $value = strtolower(trim($value));
                    if ($value == '') {
                        throw new Exception(
                            'Username is empty!',
                            self::INVALID_USERNAME
                        );
                    }
                }

                if ($key == self::COLUMN_EMAIL) {
                    if (!is_string($value)) {
                        throw new Exception(
                            'Email must be as a string!',
                            self::INVALID_EMAIL
                        );
                    }

                    $value = strtolower(trim($value));
                    if ($value == '') {
                        throw new Exception(
                            'Email is empty!',
                            self::INVALID_EMAIL
                        );
                    }

                    if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
                        throw new Exception(
                            'Email has invalid!',
                            self::INVALID_EMAIL
                        );
                    }
                }

                $data[$key] = $this->serial($value);
            }

            return $this->db->insert($this->getTableName(), $data);
        }

        throw new Exception(
            'Can not save data! Data user must be contains email & username',
            self::FAIL_SAVED
        );
    }

    /**
     * @return null
     * @throws Exception
     */
    public function delete()
    {
        if (!$this->isResultFromDatabase()) {
            return null;
        }
        /**
         * @var CurrentUser $currentUser
         */
        $currentUser = Loader::get(CurrentUser::class);
        if (($userData = $currentUser->getUserData())) {
            if (abs($userData->get(self::COLUMN_ID)) === abs($this->collectionData[self::COLUMN_ID])) {
                throw new Exception(
                    'Can not delete your data!',
                    self::FAIL_CURRENT_USER
                );
            }
        }

        $result = $this->db->delete(
            $this->getTableName(),
            [
                self::COLUMN_ID => $this->collectionData[self::COLUMN_ID]
            ]
        );

        if ($result) {
            // reset data
            $this->internalReset();
        }

        return $result;
    }

    /**
     * @param int $id
     * @return User|bool|null
     */
    public static function fromId($id)
    {
        /**
         * @var User $user
         */
        $user = Loader::get(User::class);

        return $user->getById($id);
    }

    /**
     * @param int $email
     * @return User|bool|null
     */
    public static function fromEmail($email)
    {
        /**
         * @var User $user
         */
        $user = Loader::get(User::class);

        return $user->getByEmail($email);
    }

    /**
     * @param int $userName
     * @return User|bool|null
     */
    public static function fromUserName($userName)
    {
        /**
         * @var User $user
         */
        $user = Loader::get(User::class);

        return $user->getByUserName($userName);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        return isset($this->collectionData[$key]);
    }

    public function offsetSet($offset, $value)
    {
        // none
    }

    /**
     * @param string $offset
     */
    public function remove($offset)
    {
        if (!empty($this->collectionDataOriginal)) {
            unset($this->collectionData[$offset], $this->dataHasSet[$offset]);
        }
    }
}
