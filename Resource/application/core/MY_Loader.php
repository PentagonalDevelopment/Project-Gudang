<?php
namespace {

    use App\Factory\EncryptSimple;
    use App\Factory\SimpleShortCutHtmlParser;
    use App\Factory\TemplateMailer;
    use App\Libraries\Arguments;
    use App\Libraries\HookAble;
    use App\Libraries\Loader;
    use App\Libraries\MetaDataCollection;
    use App\Libraries\ModuleCollection;
    use App\Libraries\Pass;
    use App\Libraries\StringSanity;
    use App\Libraries\TemplateCollection;
    use App\Libraries\UserMeta;
    use App\Libraries\UserStatus;
    use App\Model\CurrentUser;
    use App\Model\Info;
    use App\Model\Model;
    use App\Model\Options;
    use App\Model\User;
    use App\Model\UserRole;

    /**
     * Class MY_Loader
     * @Override
     *
     * @property CI_Loader $load
     * @property CI_DB_query_builder $db
     * @property CI_Session $session
     * @property CI_Output $output
     * @property MY_URI $uri
     *
     * @property Pass $l_pass
     * @property MetaDataCollection $l_metaDataCollection
     * @property Arguments $l_arguments
     * @property HookAble $l_hookAble
     * @property StringSanity $l_stringSanity
     */
    class MY_Loader extends CI_Loader
    {
        const HOOK_BODY_CLASS       = 'class:body';
        const HOOK_CONTENT_HEADER   = 'content:header';
        const HOOK_CONTENT_FOOTER   = 'content:footer';
        const HOOK_PREFIX           = 'hook:';

        /**
         * Predefined Libraries Object Name
         *
         * @var array
         */
        protected $predefinedLibraries = [
            Arguments::class => 'l_arguments',
            HookAble::class => 'l_hookAble',
            MetaDataCollection::class => 'l_metaDataCollection',
            ModuleCollection::class => 'l_moduleCollection',
            Pass::class => 'l_pass',
            StringSanity::class => 'l_stringSanity',
            TemplateCollection::class => 'l_templateCollection',
            UserMeta::class => 'l_userMeta',
            UserStatus::class => 'l_userStatus',

            // model
            CurrentUser::class => 'm_currentUser',
            Info::class => 'm_info',
            Model::class => 'm_model',
            Options::class => 'm_options',
            User::class => 'm_user',
            UserRole::class => 'm_userRole',

            // factory
            EncryptSimple::class => 'f_encryptSimple',
            TemplateMailer::class => 'f_templateMailer',
            SimpleShortCutHtmlParser::class => 'f_simpleShortCutHtmlParser',
        ];

        final public function __construct()
        {
            // create auto Loader for model & libraries
            spl_autoload_register(function ($class) {
                $nameSpaceLibraries = 'App\\';
                $class = ltrim($class, '\\');
                if (stripos($class, $nameSpaceLibraries) !== 0) {
                    return;
                }

                $classArray = explode('\\', $class);
                array_shift($classArray);
                if (strtolower(reset($classArray)) == 'model' && !class_exists('CI_Model', false)) {
                    $app_path = APPPATH . 'core' . DIRECTORY_SEPARATOR;
                    if (file_exists($app_path . 'Model.php')) {
                        /** @noinspection PhpIncludeInspection */
                        require_once($app_path . 'Model.php');
                        if (!class_exists('CI_Model', false)) {
                            throw new RuntimeException($app_path . "Model.php exists, but doesn't declare class CI_Model");
                        }
                    } elseif (!class_exists('CI_Model', false)) {
                        require_once(BASEPATH . 'core' . DIRECTORY_SEPARATOR . 'Model.php');
                    }

                    $class = config_item('subclass_prefix') . 'Model';
                    if (file_exists($app_path . $class . '.php')) {
                        /** @noinspection PhpIncludeInspection */
                        require_once($app_path . $class . '.php');
                        if (!class_exists($class, false)) {
                            throw new RuntimeException($app_path . $class . ".php exists, but doesn't declare class " . $class);
                        }
                    }
                }

                $class = implode('/', $classArray);
                $fileName = APPPATH_HIERARCHY . $class . '.php';
                if (file_exists($fileName)) {
                    /** @noinspection PhpIncludeInspection */
                    require $fileName;
                }
            }, true, false);

            // construct
            parent::__construct();
        }

        public function initialize()
        {
            $pathView = VIEWPATH;
            if (is_dir($pathView . 'errors')) {
                get_config(['error_views_path' => $pathView . 'errors' . DIRECTORY_SEPARATOR]);
            }
            $this->_ci_view_paths = [
                TEMPLATE_DIR . 'default' . DIRECTORY_SEPARATOR => true,
                $pathView => true,
            ];

            parent::initialize();
            /** @noinspection PhpParamsInspection */
            $this->library([
                Arguments::class => 'l_arguments',
                HookAble::class => 'l_hookAble',
                StringSanity::class => 'l_stringSanity',
                MetaDataCollection::class => 'l_metaDataCollection',
            ]);
        }

        /**
         * @var int
         */
        protected $headerLoadedCount = 0;

        /**
         * @var int
         */
        protected $footerLoadedCount = 0;

        /**
         * @var bool
         */
        protected $viewLoadedOnce = false;

        /**
         * @var array
         */
        protected $_ci_view_paths = [
            VIEWPATH => true
        ];

        /**
         * @return int
         */
        public function getCountFooterLoaded()
        {
            return $this->footerLoadedCount;
        }

        /**
         * @param string $view
         * @param array $vars
         * @param bool $return
         * @return object|string
         */
        public function view($view, $vars = [], $return = false)
        {
            if (!$this->viewLoadedOnce) {
                $this->viewLoadedOnce = true;
                foreach ($this->_ci_view_paths as $key => $value) {
                    if (file_exists($key . 'Template.php') && is_file($key . 'Template.php')) {
                        /** @noinspection PhpIncludeInspection */
                        require $key . 'Template.php';
                        break;
                    }
                }
            }

            return parent::view($view, $vars, $return);
        }

        public function includeRequire($file)
        {
            if (is_string($file) && file_exists($file)) {
                /** @noinspection PhpIncludeInspection */
                return require $file;
            }

            return false;
        }

        /**
         * @return array
         */
        public function getViewPaths()
        {
            return $this->_ci_view_paths;
        }

        /**
         * @return int
         */
        public function getCountHeaderLoaded()
        {
            return $this->footerLoadedCount;
        }

        /**
         * @param String $path
         */
        public function setViewPath($path)
        {
            if (!is_string($path)) {
                return;
            }

            $path = rtrim($path, '\\/') . DIRECTORY_SEPARATOR;
            if (is_dir($path)) {
                $path = rtrim(
                    preg_replace('`(\/|\\\)+`', DIRECTORY_SEPARATOR, $path),
                    DIRECTORY_SEPARATOR
                ) . DIRECTORY_SEPARATOR;
                $default = [
                    TEMPLATE_DIR . 'default' . DIRECTORY_SEPARATOR => true,
                    VIEWPATH => true
                ];
                $this->_ci_view_paths = [$path => true] + $default;
            }
        }

        /**
         * @return mixed
         */
        public function getTemplateDirectory()
        {
            foreach ($this->_ci_view_paths as $path => $val) {
                if (is_dir($path)) {
                    return realpath($path) ?: $path;
                }
            }

            $keys = reset(array_keys($this->_ci_view_paths));
            return reset($keys);
        }

        /**
         * @param string $path
         * @return string
         */
        public function getTemplateDirectoryUrl($path = '')
        {
            $rPath = $this->getTemplateDirectory();
            if (strpos(realpath($rPath), PUBLIC_DIR) === 0) {
                $this->helper('url');
                $rPath = rtrim(substr($rPath, strlen(PUBLIC_DIR)), DIRECTORY_SEPARATOR);
                if (DIRECTORY_SEPARATOR != '/') {
                    $rPath = str_replace(DIRECTORY_SEPARATOR, '/', $path);
                }
                if (trim($path) != '') {
                    $rPath .= $path[0] == '?'
                        ? $path
                        : '/' . ltrim($path, '/');
                }
                return base_url($rPath);
            }

            return base_url($path);
        }

        /**
         * @return string
         */
        public function getHeaderHook()
        {
            $instance =& get_instance();
            if (isset($instance->l_hookAble)) {
                return $instance->l_hookAble->apply(self::HOOK_CONTENT_HEADER, '', $this);
            }

            return null;
        }

        /**
         * @return string|mixed
         */
        public function getFooterHook()
        {
            $instance =& get_instance();
            if (isset($instance->l_hookAble)) {
                return $instance->l_hookAble->apply(self::HOOK_CONTENT_FOOTER, '', $this);
            }

            return null;
        }

        /**
         * Load Header Count
         */
        public function headerPart()
        {
            $this->view('header');
            $this->headerLoadedCount = true;
        }

        /**
         * Loop count
         */
        public function footerPart()
        {
            $this->view('footer');
            $this->footerLoadedCount++;
        }

        /**
         * @return array|mixed
         */
        public function getBodyClassArray()
        {
            $instance =& get_instance();
            $class = [];
            if (isset($instance->l_hookAble)) {
                $class = $instance->l_hookAble->apply(self::HOOK_BODY_CLASS, [], $this);
                $class = !is_array($class) ? [] : $class;
            }

            return $class;
        }

        /**
         * @param bool $withClassTag
         * @return string
         */
        public function getBodyClass($withClassTag = false)
        {
            $body = array_values($this->getBodyClassArray());
            /**
             * @var StringSanity $sanitizer
             */
            $sanitizer =  Loader::get(StringSanity::class);
            $body = array_unique(
                array_map(
                    function ($value) use ($sanitizer) {
                        return is_string($value)
                            ? $sanitizer->filterHtmlClass($value)
                            : gettype($value);
                    },
                    $body
                )
            );

            $value  = $withClassTag? ' class="': '';
            $value .=  implode(' ', array_filter($body));
            $value .= $withClassTag ? '"': '';
            return $value;
        }

        /**
         * @param string $string
         * @param mixed $value
         * @return $this
         */
        final public function setArgument($string, $value)
        {
            $instance =& get_instance();
            if (!isset($instance->l_arguments)) {
                $instance->load->library([
                    Arguments::class => 'l_arguments'
                ]);
            }

            if (isset($instance->l_arguments) && $instance->l_arguments instanceof Arguments) {
                $instance->l_arguments->set($string, $value);
            }

            return $this;
        }

        /**
         * @param array $values
         * @return $this
         */
        final public function replaceArgument(array $values)
        {
            $instance =& get_instance();
            if (!isset($instance->l_arguments)) {
                $instance->load->library([
                    Arguments::class => 'l_arguments'
                ]);
            }

            if (isset($instance->l_arguments) && $instance->l_arguments instanceof Arguments) {
                $instance->l_arguments->replace($values);
            }

            return $this;
        }

        /**
         * @param string $string
         * @param mixed $default
         * @return mixed
         */
        final public function getArgument($string, $default = null)
        {
            $instance =& get_instance();
            if (!isset($instance->l_arguments)) {
                $instance->load->library([
                    Arguments::class => 'l_arguments'
                ]);
            }

            if (isset($instance->l_arguments) && $instance->l_arguments instanceof Arguments) {
                return $instance->l_arguments->get($string, $default);
            }
            return $default;
        }

        /**
         * @param string|array $params
         * @param bool $return
         * @param boolean $query_builder
         * @return $this|bool
         * @Override
         * Self resolve
         */
        public function database($params = '', $return = false, $query_builder = null)
        {
            // Grab the super object
            $CI =& get_instance();
            $active_group = config_item('selected_database');

            if ($query_builder === null) {
                $query_builder = config_item('query_builder');
                $query_builder = isset($query_builder) ? $query_builder : true;
            }

            if (func_num_args() === 0) {
                $new_params = config_item('database') ?: [];
                $active_group = is_string($active_group) ? $active_group : null;
                $params = is_string($active_group) && isset($new_params[$active_group]) && is_array($new_params[$active_group])
                    ? $new_params[$active_group]
                    : (isset($new_params[ENVIRONMENT]) && is_array($new_params[ENVIRONMENT])
                        ? $new_params[ENVIRONMENT]
                        : -1
                    );
                if ($params === -1) {
                    $active_group = 'default';
                    $params = !empty($new_params[$active_group])
                    && is_array($new_params[$active_group])
                        ? $new_params[$active_group]
                        : '';
                }

                if (!empty($params['dbdriver']) && is_string($params['dbdriver']) && strtolower($params['dbdriver']) == 'pdo') {
                    $params['dbdriver'] = strtolower($params['dbdriver']);
                    if (empty($params['subdriver'])) {
                        $params['subdriver'] = 'mysql';
                    }
                }
            }

            // Do we even need to load the database class?
            if ($return === false && $query_builder === null
                && isset($CI->db) && is_object($CI->db) && !empty($CI->db->conn_id)
            ) {
                return false;
            }

            require_once(BASEPATH . 'database/DB.php');

            if ($return === true) {
                return DB($params, $query_builder);
            }

            // Initialize the db variable. Needed to prevent
            // reference errors with some configurations
            $CI->db = '';

            // Load the DB class
            $CI->db =& DB($params, $query_builder);
            return $this;
        }

        /**
         * {@inheritdoc}
         * @param array|string $library
         */
        public function library($library, $params = null, $object_name = null)
        {
            return parent::library($library, $params, $object_name); // TODO: Change the autogenerated stub
        }

        /**
         * @param string $name
         * @return bool
         */
        public function isModel($name)
        {
            $name = $this->getNamePackageLibraryFor($name);
            return substr($name, 0, 2) == 'm_';
        }

        /**
         * @param string $name
         * @return bool
         */
        public function isLibrary($name)
        {
            $name = $this->getNamePackageLibraryFor($name, $name);
            return substr($name, 0, 2) == 'l_' || substr($name, 0, 3) == 'CI_' || substr($name, 0, 3) == 'MY_';
        }

        /**
         * @param string $name
         * @return bool
         */
        public function isFactory($name)
        {
            $name = $this->getNamePackageLibraryFor($name);
            return substr($name, 0, 2) == 'f_';
        }

        /**
         * Get Package Only for Embed
         *
         * @param string $name
         * @return object|MY_Model
         * @throws Exception
         */
        public function getPackageFrom($name)
        {
            if (is_object($name)) {
                $name = get_class($name);
            }
            if (is_string($name)) {
                $instance =& get_instance();
                $class = $name;
                if (isset($instance->$name)) {
                    return $instance->$name;
                }

                if ($this->isModel($name)) {
                    $name = $this->getNamePackageLibraryFor($name);
                    if ($name && ! isset($instance->$name)) {
                        $this->model($class);
                    }
                    if (isset($instance->$name)) {
                        return $instance->$name;
                    }

                    $name = $class;
                }

                if ($this->isFactory($name)) {
                    $name = $this->getNamePackageLibraryFor($name);
                    if ($name && !isset($instance->$name)) {
                        $this->factory($class);
                    }
                    if (isset($instance->$name)) {
                        return $instance->$name;
                    }

                    $name = $class;
                }

                if ($this->isLibrary($name)) {
                    $name = $this->getNamePackageLibraryFor($name);
                    if (!isset($instance->$name) && ($name || substr($class, 0, 3) == 'CI_') || substr($class, 0, 3) == 'MY_') {
                        if ($name) {
                            $this->library($class);
                        } else {
                            $className = strtolower(substr($class, 3));
                            if (!isset($instance->$className)) {
                                $this->library($className);
                            }
                            if (isset($instance->$className)) {
                                return $instance->$className;
                            }
                        }
                    }

                    if (isset($instance->$name)) {
                        return $instance->$name;
                    }

                    $name = $class;
                }
            }

            throw new Exception(
                sprintf(
                    'Package for %s has not exists!',
                    is_string($name) || is_numeric($name)
                        ? $name
                        : gettype($name)
                ),
                E_USER_ERROR
            );
        }

        public function getNamePackageLibraryFor($keyName, $default = null)
        {
            if (!is_string($keyName)) {
                return $default;
            }

            $keyName = ltrim($keyName, '\\');
            if (isset($this->predefinedLibraries[$keyName])) {
                return $this->predefinedLibraries[$keyName];
            }

            if (stripos($keyName, 'App\\Model\\') === 0) {
                $object_name = 'm_' . trim(lcfirst(str_replace('\\', '_', substr($keyName, 10))), '_');
                $this->predefinedLibraries[$keyName] = $object_name;
            } elseif (stripos($keyName, 'App\\Libraries\\') === 0) {
                $object_name = 'l_' . trim(lcfirst(str_replace('\\', '_', substr($keyName, 14))), '_');
                $this->predefinedLibraries[$keyName] = $object_name;
            } elseif (stripos($keyName, 'App\\Factory\\') === 0) {
                $object_name = 'f_' . trim(lcfirst(str_replace('\\', '_', substr($keyName, 12))), '_');
                $this->predefinedLibraries[$keyName] = $object_name;
            }

            return isset($this->predefinedLibraries[$keyName])
                ? $this->predefinedLibraries[$keyName]
                : $default;
        }

        /**
         * @param string $class
         * @param mixed $params
         * @param string $object_name
         * @return object|mixed
         */
        protected function _ci_load_library($class, $params = null, $object_name = null)
        {
            // use for check
            if (is_string($class) && stripos(ltrim($class, '\\'), 'App\\Libraries\\') === 0) {
                if (class_exists(ltrim($class, '\\'))) {
                    $class = ltrim($class, '\\');
                    if (!$object_name) {
                        $object_name = $this->getNamePackageLibraryFor($class, null);
                        if (!$object_name) {
                            $object_name = 'l_' . trim(lcfirst(str_replace('\\', '_', substr($class, 14))), '_');
                            $this->predefinedLibraries[$class] = $object_name;
                        }
                    }

                    /** @noinspection PhpVoidFunctionResultUsedInspection */
                    return $this->_ci_init_library($class, '', $params, $object_name);
                }

                // If we got this far we were unable to find the requested class.
                log_message('error', 'Unable to load the requested class: ' . $class);
                show_error('Unable to load the requested class: ' . $class);
            }

            /** @noinspection PhpVoidFunctionResultUsedInspection */
            return parent::_ci_load_library($class, $params, $object_name);
        }

        /**
         * {@inheritdoc}
         * @param string|array $model
         */
        final public function model($model, $name = '', $db_conn = false)
        {
            if (!empty($model) && is_string($model) && stripos(ltrim($model, '\\'), 'App\\Model\\') === 0) {
                if (class_exists($model)) {
                    $model = ltrim($model, '\\');
                    if (empty($name)) {
                        $name = $this->getNamePackageLibraryFor($model) ?: $model;
                        if (!$name) {
                            $name = 'm_' . trim(lcfirst(str_replace('\\', '_', substr($model, 10))), '_');
                            $this->predefinedLibraries[$model] = $name;
                        }
                    }

                    if (in_array($name, $this->_ci_models, true)) {
                        return $this;
                    }

                    $CI =& get_instance();
                    if (isset($CI->$name)) {
                        throw new RuntimeException('The model name you are loading is the name of a resource that is already being used: ' . $name);
                    }

                    $this->_ci_models[] = $name;
                    $CI->$name = new $model();
                    return $this;
                }
            }

            return parent::model($model, $name, $db_conn);
        }

        /**
         * @var array
         */
        protected $_ci_factory = [];

        /**
         * {@inheritdoc}
         * @param string|array $factory
         */
        final public function factory($factory, $name = '')
        {
            if (is_array($factory)) {
                foreach ($factory as $factorySelector => $factoryName) {
                    if (is_int($factorySelector)) {
                        $this->factory($factoryName);
                    } else {
                        $this->factory($factorySelector, $factoryName);
                    }
                }

                return $this;
            }

            if (!empty($factory) && is_string($factory) && stripos(ltrim($factory, '\\'), 'App\\Factory\\') === 0) {
                if (class_exists($factory)) {
                    $factory = ltrim($factory, '\\');
                    if (empty($name)) {
                        $name = $this->getNamePackageLibraryFor($factory) ?: $factory;
                        if (!$name) {
                            $name = 'f_' . trim(lcfirst(str_replace('\\', '_', substr($factory, 10))), '_');
                            $this->predefinedLibraries[$factory] = $name;
                        }
                    }

                    if (in_array($name, $this->_ci_factory, true)) {
                        return $this;
                    }

                    $CI =& get_instance();
                    if (isset($CI->$name)) {
                        throw new RuntimeException(
                            'The Factory name you are loading is the name of a resource that is already being used: ' . $name
                        );
                    }

                    $this->_ci_factory[] = $name;
                    $CI->$name = new $factory();
                    return $this;
                }
            }

            throw new RuntimeException(
                sprintf(
                    'Can not get factory from %s!',
                    is_string($name) || is_numeric($name) ? $name : gettype($name)
                )
            );
        }

        /**
         * @param string|string[]   $helpers    Helper name(s)
         * @return MY_Loader
         */
        public function helper($helpers = [])
        {
            /**
             * @var MY_Loader $Helper
             */
            $Helper = parent::helper($helpers);
            return $Helper;
        }

        /**
         * @return CurrentUser
         */
        public function currentUser()
        {
            /**
             * @var CurrentUser $current
             */
            $current =  $this->getPackageFrom(CurrentUser::class);
            return $current;
        }
    }
}
