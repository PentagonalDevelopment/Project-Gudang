<?php
namespace App\Libraries;

/**
 * Class Loader
 * @package App\Libraries
 */
class Loader
{
    private static $instance;

    /**
     * Loader constructor.
     */
    public function __construct()
    {
        self::$instance =& get_instance();
    }

    /**
     * @param $name
     * @return object|\MY_Model
     */
    public static function get($name)
    {
        if (!isset(self::$instance)) {
            self::$instance =& get_instance();
        }

        /**
         * @var \MY_Loader $loader
         */
        $loader = self::$instance->load;

        return $loader->getPackageFrom($name);
    }
}
