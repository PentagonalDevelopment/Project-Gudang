<!--
 * template base
 * make all have style to prevent override of style
 * style : style="width: 100%;border-collapse:collapse;border-width:0;border-color: transparent;max-width: 100%;vertical-align: middle;margin:0;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color:#444444;"
 * table wrap style : width: 100%;margin:0 auto;border-collapse:collapse;border-width:0;border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;
 * td style: border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;
 * color: #444444;
 * bg base :
 *      - green : #32c787
 *      - grey-light  : #f1f1f1
 *      - grey : #dbddde
 *      - dark : #232121
 *
 * short hand available :
 * {{current_year}} : current year
 * {{company_name}} : company name
 * {{logo_text}}    : logo as text
 * {{verify_url}}   : verify url forgot
 -->
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="background: #f1f1f1;background-color: #f1f1f1;word-break:break-all;word-wrap:break-word;margin:0;padding:0;line-height: 1.2;color: #444444;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
<div id="tpl-mail-wrapper" style="margin:0 auto;min-width:320px;line-height: 1.2;color: #444444;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;width:100%;height: 100%;max-width: 720px;padding: 0;">
  <div style="padding: 0 10px;margin:0;min-width: 300px">
    <table id="tpl-mail-wrap" border="0" valign="middle"  align="center" width="300" style="border-collapse:collapse;border-width:0;border-color: transparent;width:100%;min-width: 320px;vertical-align: middle;margin:0 auto;padding:0 20px;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
      <tbody id="tpl-mail-wrap-main">
      <tr id="tpl-mail-wrap-start">
        <td id="tpl-mail-wrap-start-container" width="100%" valign="middle" style="width: 100%;border-collapse:collapse;border-width:0;border-color: transparent;max-width: 100%;vertical-align: middle;margin:0;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color:#444444;">
          <table id="tpl-mail-wrap-start-container-table" width="100%" border="0" style="width: 100%;border-collapse:collapse;border-width:0;border-color: transparent;max-width: 100%;vertical-align: middle;margin:0;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color:#444444;">
            <tbody>
            <tr id="tpl-mail-header" valign="middle" style="vertical-align: middle">
              <td style="border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
                <!-- header goes here -->
                <table width="100%" style="width: 100%;margin:30px auto 0 auto;border-collapse:collapse;border-width:0;border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
                  <tbody>
                  <tr>
                    <td width="100" style="padding: 20px 10px 20px 20px;width: 250px;height: 40px;background: #32c787;color: #ffffff;">
                      <h1 style="font-size: 18px;">{{logo_text}}</h1>
                    </td>
                    <td style="padding: 10px 20px 10px 10px;height: 40px;background: #32c787;color: #ffffff;">

                    </td>
                  </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!-- #tpl-mail-header -->
            <tr id="tpl-mail-content" valign="middle" style="vertical-align: middle">
              <td style="border-color: transparent;vertical-align: baseline;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
                <!-- content goes here -->
                <table width="100%" style="width: 100%;margin:0 auto;border-collapse:collapse;border-width:0;border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
                  <tbody>
                  <tr>
                    <td style="padding: 10px 20px;width: 100%;height: 200px;background: #ffffff;color: #444444;word-break:normal;word-wrap:break-word;vertical-align: top;font-size: 13px;">
                      <div style="text-align: center;margin: 20px 0">
                        <h2 style="text-align: center;color: #555555;font-size: 17px;">PASSWORD RESET REQUEST</h2>
                      </div>
                      <p>&nbsp;</p>
                      <p style="font-size: 13px;color: #555555;">
                        <strong>Hello <em>{{user_first_name}}</em></strong>
                      </p>
                      <p style="font-size: 13px;color: #555555;">
                        We have been receive request forgot password on your account at <em>{{user_email}}</em>.
                        <br />
                        If you you never request it, simply you could <strong>ignore</strong> this message.
                      </p>
                      <p>&nbsp;</p>
                      <p style="text-align: center;margin: 40px 0 40px 0;">
                        <a href="{{verify_url}}" style="background-color:#32c787;text-decoration: none;letter-spacing: 1px;border-radius: 2px;border:0;font-size: 14px;cursor: pointer;">
                                                <span style="color: #fff;text-decoration: none;padding: 10px 20px;width: 10px;letter-spacing: 1px;border-radius: 2px;border:0;background-color:#32c787;font-size: 14px;cursor: pointer;">
                                                    Reset Your Password
                                                </span>
                        </a>
                      </p>
                      <p>&nbsp;</p>
                      <p style="margin: 30px 0 5px;font-size: 12px;color: #666666">
                        or copy link below and paste onto your browser address bar
                      </p>
                      <pre style="word-break:break-all;margin: 5px 0 10px 0;word-wrap:break-word;border:1px solid #ddd;background-color: #f1f1f1;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;padding: 10px;font-size: 11px;">{{verify_url}}</pre>
                      <p>&nbsp;</p>
                      <p style="font-size: 12px;color: #555555">
                        <em>Request password link will be expired after : {{expired_time_human}}. Please follow this link before {{expired_time_clock}} or link will be detect as invalid by our system.</em>
                      </p>
                      <p>&nbsp;</p>
                      <p style="text-align: center;font-size: 11px;color: #777777">
                        This message is automatic sent by our system.
                        <br />
                        Please does not direct reply to the email adddress, or your message will be ignore or deny by our system.
                      </p>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!-- #tpl-mail-content -->
            <tr id="tpl-mail-footer" valign="middle" style="vertical-align: middle">
              <td style="border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
                <!-- footer goes here -->
                <table id="tpl-mail-footer-info" width="100%" style="width: 100%;margin:0 auto;border-collapse:collapse;border-width:0;border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
                  <tbody>
                  <tr>
                    <td style="padding: 20px 20px;width: 100%;height: 20px;background: #32c787;color: #ffffff;">

                    </td>
                  </tr>
                  </tbody>
                </table>
                <!-- #tpl-mail-footer-info -->
                <table id="tpl-mail-footer-copyright" width="100%" style="width: 100%;margin:0 auto;border-collapse:collapse;border-width:0;border-color: transparent;vertical-align: middle;padding:0;line-height: 1.2;font-size: 14px;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #444444;">
                  <tbody>
                  <tr>
                    <td style="padding: 5px 20px;width: 100%;height: 20px;color: #666666;margin-top: 15px;margin-bottom: 30px;font-size: 11px;text-align: center;">
                      <p style="text-align: center;font-size: 11px;">
                        &copy; {{current_year}} {{company_name}}
                      </p>
                    </td>
                  </tr>
                  </tbody>
                </table>
                <!-- #tpl-mail-footer-copyright -->
              </td>
            </tr>
            </tbody>
            <!-- #tpl-mail-footer-copyright -->
          </table>
          <!-- #tpl-mail-wrap-start-container-table -->
        </td>
        <!-- #tpl-mail-wrap-start-container -->
      </tr>
      <!-- #tpl-mail-wrap-start -->
      </tbody>
      <!-- #tpl-mail-wrap-main -->
    </table>
    <!-- #tpl-mail-wrap -->
  </div>
</div>
<!-- #tpl-mail-wrapper -->
</body>
</html>