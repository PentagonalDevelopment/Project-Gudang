<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}

$exception = $this->getArgument('error_exception');
?>

An uncaught Exception was encountered

Type:        <?= get_class($exception), "\n"; ?>
Message:     <?= $this->getArgument('error_message'), "\n"; ?>
Filename:    <?= $exception->getFile(), "\n"; ?>
Line Number: <?= $exception->getLine(); ?>

<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

    Backtrace:
    <?php foreach ($exception->getTrace() as $error):
        if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0) :
    ?>
            File: <?php echo $error['file'], "\n"; ?>
            Line: <?php echo $error['line'], "\n"; ?>
            Function: <?php echo $error['function'], "\n\n"; ?>
    <?php
        endif;
    endforeach;
    ?>

<?php endif ?>
