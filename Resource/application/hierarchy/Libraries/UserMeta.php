<?php
namespace App\Libraries;

use App\Model\User;

/**
 * Class UserMeta
 * @package App\Libraries
 */
class UserMeta extends MetaDataCollection
{
    /* --------------------------------------------------
     * DEFINITION
     * ----------------------------------------------- */

    const TABLE_NAME = 'users_meta';

    const COLUMN_ID             = 'id';
    const COLUMN_USER_ID        = 'user_id';
    const COLUMN_NAME           = 'meta_name';
    const COLUMN_VALUE          = 'meta_value';
    const COLUMN_CREATED_AT     = 'created_at';
    const COLUMN_UPDATED_AT     = 'updated_at';

    /**
     * @var array
     */
    protected $collections = [];

    /**
     * Base On Table Name
     *
     * @var string
     */
    protected $databaseTableName = self::TABLE_NAME;

    /**
     * UserRole constructor.
     * @param array $array
     */
    public function __construct($array = [])
    {
        $values = [];
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (in_array($key, [self::COLUMN_ID, self::COLUMN_USER_ID])) {
                    if (is_numeric($value) && !is_int($value)) {
                        $value[$key] = abs($value);
                    }
                    continue;
                }
                if ($key == self::COLUMN_NAME) {
                    if (!is_string($value) && !is_numeric($value)) {
                        throw new \InvalidArgumentException(
                            sprintf(
                                'Invalid value for Meta Name. Value must be as a string %s given',
                                gettype($value)
                            )
                        );
                    }

                    $values[$key] = $value;
                    continue;
                }
                if (in_array($key, [self::COLUMN_UPDATED_AT, self::COLUMN_CREATED_AT])) {
                    $time = $value ? @strtotime($value) : '1970-01-01 00:00:00';
                    $values[$key] = is_int($time) ? date('Y-m-d H:i:s', $time) : $value;
                }
            }
        }
        parent::__construct($values);
        $instance =& get_instance();
        $this->databaseTableName = $instance->db->dbprefix(self::TABLE_NAME);
    }

    private function sanityResult()
    {
        if (isset($this->collections[self::COLUMN_VALUE])) {
            /**
             * @var StringSanity $sanity
             */
            $sanity = Loader::get(StringSanity::class);
            $this->collections[self::COLUMN_VALUE] = $sanity
                ->maybeUnSerialize($this->collections[self::COLUMN_VALUE]);
        }
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->databaseTableName;
    }

    /**
     * @param array $array
     * @return UserMeta
     */
    public function newInstance($array = [])
    {
        return new static($array);
    }

    /**
     * @param User $user
     * @return UserMeta[]|UserMeta
     */
    public static function fromUser(User $user)
    {
        $instance = new self();
        /**
         * @var UserMeta $result
         */
        $result = $instance->newInstance();
        if ($user->get(self::COLUMN_ID) < 1 || ! $user->isExists()) {
            return $instance;
        }

        $ci =&get_instance();
        $resultDB = $ci
            ->db
            ->select()
            ->from($instance->getTableName())
            ->where(self::COLUMN_USER_ID, $user->get(self::COLUMN_ID))
            ->get();
        foreach ($resultDB->result_array() as $key => $value) {
            $result->set($key, $result->newInstance($value)->sanityResult());
        }

        $resultDB->free_result();
        return $result;
    }
}
