<?php
namespace App\Factory;

/**
 * Class EncryptSimple
 * @package App\Factory
 */
class EncryptSimple
{
    /**
     * Current version
     */
    const VERSION            = '1.0.0';

    /**
     * Default Chipper
     */
    const DEFAULT_CHIPPER      = 'AES-256-CBC';

    /* --------------------------------------------------------------------------------*
     |                                  ENCODING                                       |
     |---------------------------------------------------------------------------------|
     */

    /**
     * Encode the values using base64_encode and replace some string
     * and could decode @uses safeBase64Decode()
     *
     * @param  string $string
     * @return string
     */
    public static function safeBase64Encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(['+', '/', '='], ['-', '_', ''], $data);
        return $data;
    }

    /**
     * Decode the safeBase64Encode() of the string values
     *
     * @see safeBase64Encode()
     *
     * @param  string $string
     * @return string
     */
    public static function safeBase64Decode($string)
    {
        $data = str_replace(['-', '_'], ['+', '/'], $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    /* --------------------------------------------------------------------------------*
     |                              Extended Helpers                                   |
     |---------------------------------------------------------------------------------|
     */

    /**
     * Rotate each string characters by n positions in ASCII table
     * To encode use positive n, to decode - negative.
     * With n = 13 (ROT13), encode and decode n can be positive.
     * @see  {@link http://php.net/str_rot13}
     *
     * @param  string  $string
     * @param  integer $n
     * @return string
     */
    public static function rotate($string, $n = 13)
    {
        if (!is_string($string)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Parameter 1 must be as string! %s given.',
                    gettype($string)
                )
            );
        }

        $length = strlen($string);
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $ascii = ord($string{$i});

            $rotated = $ascii;

            if ($ascii > 64 && $ascii < 91) {
                $rotated += $n;
                $rotated > 90 && $rotated += -90 + 64;
                $rotated < 65 && $rotated += -64 + 90;
            } elseif ($ascii > 96 && $ascii < 123) {
                $rotated += $n;
                $rotated > 122 && $rotated += -122 + 96;
                $rotated < 97 && $rotated  += -96 + 122;
            }

            $result .= chr($rotated);
        }

        return $result;
    }

    /**
     * @param string $string
     * @return string
     */
    protected static function hashStringContainer($string)
    {
        $hash_algorithms = hash_algos();
        $priority = [
            'sha512',
            'sha384',
            'ripemd320',
            'sha256',
            'ripemd256',
            'sha224',
            'ripemd160',
            'sha1',
        ];
        $chosen_algorithm = null;
        foreach ($priority as $algorithm) {
            if (in_array($algorithm, $hash_algorithms)) {
                $chosen_algorithm = $algorithm;
                break;
            }
        }

        return !$chosen_algorithm ? sha1($string) : hash($chosen_algorithm, $string);
    }

    /**
     * Create Pass Key
     *
     * @param string $key
     * @return string[]
     * @throws \ErrorException
     */
    private static function createPassKey($key)
    {
        /**
         * ------------------------------------
         * Safe Sanitized hash
         * ------------------------------------
         */
        (is_null($key) || $key === false) && $key = '';
        // safe is use array or object as hash
        $serialize_key      = @serialize($key);
        if (!$serialize_key) {
            throw new \ErrorException(
                sprintf(
                    'Could not serialize input key of %s',
                    gettype($serialize_key)
                )
            );
        }

        /**
         * ------------------------------------
         * Set Key
         * ------------------------------------
         */
        $key       = pack('H*', self::hashStringContainer($serialize_key));
        return [
            $serialize_key,
            self::safeBase64Encode(str_pad($key, 24, "\0", STR_PAD_RIGHT))
        ];
    }


    /* --------------------------------------------------------------------------------*
     |                              Encryption openSSL                                 |
     |---------------------------------------------------------------------------------|
     */

    /**
     * Decrypt With OpenSSL
     *
     * @param string $key
     * @param string $crypt_text
     * @param string $method
     * @return array
     */
    private static function cryptOpenSSl(
        $key,
        $crypt_text,
        $method = self::DEFAULT_CHIPPER
    ) {
        $iv_len = openssl_cipher_iv_length($method);
        $strong = false;
        $iv = openssl_random_pseudo_bytes($iv_len, $strong);

        return [
            @openssl_encrypt($crypt_text, $method, $key, OPENSSL_RAW_DATA, $iv),
            $method,
            $iv
        ];
    }

    /**
     * Decrypt With OpenSSL
     *
     * @param string $key
     * @param string $crypt_text
     * @param string $method
     * @param string $iv
     * @return string|bool string on success false if failure
     */
    private static function decryptOpenSSl(
        $key,
        $crypt_text,
        $method,
        $iv = null
    ) {
        $iv_len = openssl_cipher_iv_length($method);
        if (!is_string($iv) || strlen($iv) <> $iv_len) {
            $strong = false;
            $iv = openssl_random_pseudo_bytes($iv_len, $strong);
        }
        return openssl_decrypt($crypt_text, $method, $key, OPENSSL_RAW_DATA, $iv);
    }

    private static function determineChipper($method)
    {
        /**
         * ------------------------------------
         * Doing decryption
         * ------------------------------------
         */
        $chipper = openssl_get_cipher_methods();
        $method = !is_string($method) || trim(strtoupper($method)) == ''
            ? self::DEFAULT_CHIPPER
            : trim(strtoupper($method));
        return !in_array($method, $chipper)
            ? self::DEFAULT_CHIPPER
            : $method;
    }

    /**
     * @param string $string
     * @return mixed
     */
    private static function doCompress($string)
    {
        if (function_exists('gzdeflate')) {
            return @gzdeflate($string, 9)?: $string;
        }

        return $string;
    }

    /**
     * @param string $string
     * @return string
     */
    private static function doDeCompress($string)
    {
        if (function_exists('gzinflate')) {
            return @gzinflate($string);
        }

        return $string;
    }

    /**
     * @param string $string
     * @param string $key
     * @param int $type
     * @return mixed
     */
    public static function xorIt($string, $key, $type = 0)
    {
        $sLength = strlen($string);
        $xLength = strlen($key);
        for ($i = 0; $i < $sLength; $i++) {
            for ($j = 0; $j < $xLength; $j++) {
                if ($type == 1) {
                    $string[$i] = $key[$j]^$string[$i];
                } else {
                    $string[$i] = $string[$i]^$key[$j];
                }
            }
        }

        return $string;
    }

    /**
     * @param mixed $value
     * @param mixed $pass
     * @param string $chipper
     * @return string
     */
    public static function encrypt($value, $pass = null, $chipper = self::DEFAULT_CHIPPER)
    {
        $pass = self::createPassKey($pass);
        $chipper = self::determineChipper($chipper);
        $value = serialize([$value, $chipper]);
        if ($value == false) {
            throw new \RuntimeException(
                "Can not stored given values",
                E_USER_ERROR
            );
        }

        $value = self::doCompress($value);
        $openSSL = self::cryptOpenSSl(
            self::safeBase64Encode($pass[1] . $pass[0]),
            $value,
            $chipper
        );

        $iv    = str_replace('||', '\\\\', $openSSL[2]);
        $value = str_replace('||', '\\\\', $openSSL[0]);
        $hash  = sha1($openSSL[2] . $openSSL[0] . $pass[1]);
        $now   = (strlen($pass[1]) % 13);
        $now   = $now > 0 && $now < 13 ? $now : strlen($pass[1]) - ($now*13);
        $value = self::rotate(
            self::xorIt("{$iv}||{$value}||{$hash}||{$chipper}", serialize($pass)),
            $now
        );

        // twice
        $value = self::cryptOpenSSl($pass[1], $value, self::DEFAULT_CHIPPER);
        return self::safeBase64Encode($value[0].$value[2]);
    }

    /**
     * @param mixed $value
     * @param mixed $pass
     * @return null|mixed
     */
    public static function decrypt($value, $pass = null)
    {
        if (!is_string($value) || strlen($value) < 3) {
            return null;
        }

        $value = self::safeBase64Decode($value);
        if ($value === false) {
            return null;
        }
        $iv_len = openssl_cipher_iv_length(self::DEFAULT_CHIPPER);
        $iv = substr($value, -$iv_len);
        $value = substr($value, 0, -$iv_len);
        $pass = self::createPassKey($pass);
        if (!($value = self::decryptOpenSSl($pass[1], $value, self::DEFAULT_CHIPPER, $iv))) {
            return null;
        }

        $now  = (strlen($pass[1]) % 13);
        $now = -($now > 0 && $now < 13 ? $now : strlen($pass[1]) - ($now*13));
        $value = self::xorIt(self::rotate($value, $now), serialize($pass));
        $value = explode('||', $value);
        if (count($value) <> 4) {
            return null;
        }

        $method = array_pop($value);
        $hash  = array_pop($value);
        $iv    = str_replace('\\\\', '||', array_shift($value));
        $value = str_replace('\\\\', '||', reset($value));
        if ($hash !== sha1($iv . $value . $pass[1])) {
            return null;
        }

        if (!($decrypted = self::decryptOpenSSl(
            self::safeBase64Encode($pass[1] . $pass[0]),
            $value,
            $method,
            $iv
        ))) {
            return null;
        }

        if (!($decrypted = self::doDeCompress($decrypted))
            || strpos($decrypted, 'a:2:{') !== 0
            || ! ($decrypted = @unserialize($decrypted))
            || count($decrypted) <> 2
            || $decrypted[1] !== $method
        ) {
            return null;
        }

        return $decrypted[0];
    }
}
