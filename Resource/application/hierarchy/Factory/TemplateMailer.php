<?php
namespace App\Factory;

use App\Factory\Validator\Domain;
use App\Libraries\Loader;

/**
 * Class TemplateMailer
 * @package App\Factory
 */
class TemplateMailer
{
    /**
     * @var \CI_Email
     */
    protected $mailer;

    /**
     * @var array
     */
    protected $smtp = [];

    /**
     * @var array
     */
    protected $availableProtocols = ['mail', 'sendmail', 'smtp'];

    /**
     * @var string
     */
    protected $protocol = 'mail';

    /**
     * @var string
     */
    protected $userAgent = 'Pentagonal Mailer';

    /**
     * @var boolean
     */
    protected $isHtml = false;

    /**
     * @var bool
     */
    protected $isDSN = false;

    /**
     * @var bool
     */
    protected $isWrapped = true;

    /**
     * Whether to send messages to BCC recipients in batches.
     *
     * @var bool
     */
    public $isBatchMode = false;

    /**
     * Whether to send multipart alternatives.
     * Yahoo! does not seem to like these.
     *
     * @var bool
     */
    public $isMultiPart = false;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $from = [];

    /**
     * @var array
     */
    protected $replyTo = [];

    /**
     * @var array
     */
    protected $target = [];

    /**
     * @var array
     */
    protected $bcc = [];

    /**
     * @var array
     */
    protected $cc = [];

    /**
     * @var array
     */
    protected $attachments = [];

    /**
     * @var string
     */
    protected $body = '';

    /**
     * @var string
     */
    protected $altBody = '';

    /**
     * @var int
     * 1 => Highest
     * 2 => Hight
     * 3 => Normal
     * 4 => Low
     * 5 => Lowest
     */
    protected $priority = 3;

    /**
     * BCC Batch max number size.
     *
     * @see \CI_Email::$bcc_batch_mode
     * @var int
     */
    public $batchSize = 200;

    /**
     * @var TemplateMailer
     */
    protected $lastMailer;

    /**
     * @var bool
     */
    protected $forceAuth = true;

    /**
     * @var bool
     */
    protected $isWithAltBody = true;

    /**
     * TemplateMailer constructor.
     */
    public function __construct()
    {
        $this->mailer = clone Loader::get(\CI_Email::class);
        $this->setDefault();
    }

    /**
     * Set Default
     */
    private function setDefault()
    {
        $this->mailer->clear(true);
        $this->isDSN = (bool) $this->mailer->dsn;
        $this->mailer->mailpath = ini_get('sendmail_path')?: $this->mailer->mailpath;
        if (ini_get('sendmail_from')) {
            $this->mailer->from(ini_get('sendmail_from'));
        }
    }

    /**
     * @return TemplateMailer
     */
    public static function create()
    {
        $object = new TemplateMailer();
        return $object;
    }

    /**
     * Set As Plain Text
     *
     * @return TemplateMailer
     */
    public function asText()
    {
        $this->isHtml = false;
        return $this;
    }

    /**
     * Set As HTML Type
     *
     * @return TemplateMailer
     */
    public function asHtml()
    {
        $this->isHtml = true;
        return $this;
    }

    /**
     * Set Wrapped Body
     * @return TemplateMailer
     */
    public function setAsWrap()
    {
        $this->isWrapped = true;
        return $this;
    }

    /**
     * Set No Wrap On Body
     * @return TemplateMailer
     */
    public function setNoWrap()
    {
        $this->isWrapped = false;

        return $this;
    }

    /**
     * @param int $prior
     * @return $this
     */
    public function setPriority($prior)
    {
        if (!is_numeric($prior)) {
            return $this;
        }

        $prior = intval(abs($prior));
        $prior = $prior < 1 ? 1 : ($prior > 5 ? 5 : $prior);
        $this->priority = $prior;

        return $this;
    }

    /**
     * @param string $from
     * @param string $name
     * @param string|null $returnPath
     *
     * @return TemplateMailer
     */
    public function setFrom($from, $name = '', $returnPath = null)
    {
        if (!is_string($from)) {
            throw new \InvalidArgumentException(
                "From address must be as a string!",
                E_USER_ERROR
            );
        }

        $name = !is_string($name) ? '' : trim($name);

        $from = $this->splitFilterArrayMail($from);
        if (empty($from)) {
            throw new \InvalidArgumentException(
                "From address has invalid email address!",
                E_USER_ERROR
            );
        }

        $returnPath = $this->splitFilterArrayMail($returnPath);
        $returnPath = !empty($returnPath) ? reset($returnPath) : null;
        $this->from = [$from[0], $name, $returnPath];

        return $this;
    }

    /**
     * @param string $name
     * @return TemplateMailer
     */
    public function setFromName($name)
    {
        if (!is_string($name) || empty($this->from)) {
            return $this;
        }

        $this->from[1] = trim($name);
        return $this;
    }

    /**
     * @param string $from
     * @param string $name
     * @return TemplateMailer
     */
    public function setReplyTo($from, $name = '')
    {
        if (!is_string($from)) {
            throw new \InvalidArgumentException(
                "From address must be as a string!",
                E_USER_ERROR
            );
        }
        $from = $this->splitFilterArrayMail($from);
        if (empty($from)) {
            return $this;
        }
        $name = is_string($name) ? trim($name) : '';
        $this->replyTo = [$from[0], $name];

        return $this;
    }

    /**
     * Clear Reply To
     * @return TemplateMailer
     */
    public function clearReplyTo()
    {
        $this->replyTo = [];
        return $this;
    }

    /**
     * @param array|string $str
     * @param string $sep
     * @return array
     */
    protected function splitFilterArrayMail($str, $sep = ',')
    {
        if (!is_string($str) && !is_array($str)) {
            return [];
        }

        if (!is_array($str)) {
            $str = explode($sep, $str);
        }

        $domain = new Domain();
        $str = array_map(
            function ($address) use ($domain) {
                $address = trim($address);
                if ($address == '') {
                    return false;
                }
                $address = strtolower($address);
                if (substr_count($address, '@') <> 1) {
                    return false;
                }

                $emailAddress = $domain->validateEmail($address);
                $addressArray = explode('@', $address);
                if (!$emailAddress && ($addressArray[1] == 'localhost'
                        || $addressArray[1] == 'localhost.localdomain'
                        || gethostbyname($addressArray[1]) === '127.0.0.1'
                    )
                ) {
                    $address = !preg_match('/[^a-z0-9\.\-\_]/', $addressArray[1], $match)
                        ? $address
                        : false;
                    return $address;
                }

                return $emailAddress;
            },
            $str
        );

        return array_unique(array_filter($str));
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function setTarget($to)
    {
        if (!is_array($to) && !is_string($to)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid data type for target email. Value must be as a string or array %s given",
                    gettype($to)
                )
            );
        }

        $this->target = $this->splitFilterArrayMail($to);
        return $this;
    }

    /**
     * Clear The Target
     * @return TemplateMailer
     */
    public function clearTarget()
    {
        $this->target = [];
        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function removeTarget($to)
    {
        if (!is_string($to) && !is_array($to) || empty($this->target)) {
            return $this;
        }
        is_string($to) && explode(',', $to);
        foreach ($to as $target) {
            if (!is_string($target)) {
                continue;
            }
            $target = strtolower(trim($target));
            $key  = array_search($target, $this->target);
            if ($key !== false) {
                unset($this->target[$key]);
            }
        }

        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function addTarget($to)
    {
        if (!is_array($to) && !is_string($to)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid data type for target email. Value must be as a string or array %s given",
                    gettype($to)
                )
            );
        }

        $target = $this->splitFilterArrayMail($to);
        $this->target = array_unique(array_merge($this->target, $target));

        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function setBCC($to)
    {
        if (!is_array($to) && !is_string($to)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid data type for target BCC. Value must be as a string or array %s given",
                    gettype($to)
                )
            );
        }

        $this->bcc = $this->splitFilterArrayMail($to);
        return $this;
    }

    /**
     * Clear The BCC
     * @return TemplateMailer
     */
    public function clearBCC()
    {
        $this->bcc = [];
        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function setCC($to)
    {
        if (!is_array($to) && !is_string($to)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid data type for target BCC. Value must be as a string or array %s given",
                    gettype($to)
                )
            );
        }

        $this->cc = $this->splitFilterArrayMail($to);
        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function removeBCC($to)
    {
        if (!is_string($to) && !is_array($to) || empty($this->target)) {
            return $this;
        }
        is_string($to) && explode(',', $to);
        foreach ($to as $target) {
            if (!is_string($target)) {
                continue;
            }
            $target = strtolower(trim($target));
            $key  = array_search($target, $this->bcc);
            if ($key !== false) {
                unset($this->bcc[$key]);
            }
        }

        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function addBCC($to)
    {
        if (!is_array($to) && !is_string($to)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid data type for target email. Value must be as a string or array %s given",
                    gettype($to)
                )
            );
        }

        $target = $this->splitFilterArrayMail($to);
        $this->bcc = array_unique(array_merge($this->bcc, $target));

        return $this;
    }

    /**
     * Clear The BCC
     * @return TemplateMailer
     */
    public function clearCC()
    {
        $this->cc = [];
        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function removeCC($to)
    {
        if (!is_string($to) && !is_array($to) || empty($this->target)) {
            return $this;
        }
        is_string($to) && explode(',', $to);
        foreach ($to as $target) {
            if (!is_string($target)) {
                continue;
            }
            $target = strtolower(trim($target));
            $key  = array_search($target, $this->cc);
            if ($key !== false) {
                unset($this->cc[$key]);
            }
        }

        return $this;
    }

    /**
     * @param array|string $to
     * @return TemplateMailer
     */
    public function addCC($to)
    {
        if (!is_array($to) && !is_string($to)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid data type for target email. Value must be as a string or array %s given",
                    gettype($to)
                )
            );
        }

        $target = $this->splitFilterArrayMail($to);
        $this->cc = array_unique(array_merge($this->cc, $target));

        return $this;
    }

    /**
     * @param string $string
     * @return TemplateMailer
     */
    public function setSubject($string)
    {
        if (is_bool($string) || is_null($string) || is_int($string) || is_float($string)) {
            return $this;
        }

        if (! is_string($string)) {
            sprintf(
                "Subject must be as a string, %s given",
                gettype($string)
            );
        }

        $this->subject = preg_replace('/\s+/', ' ', trim($string));
        return $this;
    }

    /**
     * @param string|array $name
     * @param string|null $value
     * @return TemplateMailer
     */
    public function setHeader($name, $value = null)
    {
        if (is_string($name) && $value) {
            $name = [$name => $value];
        }
        if (is_array($name)) {
            foreach ($name as $key => $value) {
                if (!is_numeric($key) && trim($key) != '' && is_string($value) && trim($value) != '') {
                    $key = ucwords(trim($key), '-');
                    $this->headers[$key] = trim($value);
                }
            }
        }

        return $this;
    }

    /**
     * @param string|array $name
     * @return TemplateMailer
     */
    public function removeHeader($name)
    {
        if (is_string($name)) {
            $name = [$name];
        }
        if (is_array($name)) {
            foreach ($name as $value) {
                if (is_string($value) && trim($value) != '') {
                    $value = ucwords($value, '-');
                    unset($this->headers[$value]);
                }
            }
        }

        return $this;
    }

    /**
     * @param string|resource $file
     * @param string $newName
     * @return TemplateMailer
     */
    public function attach($file, $newName = '')
    {
        $this->attachments[] = [$newName, $file];

        return $this;
    }

    /**
     * Clear Attachment
     * @return TemplateMailer
     */
    public function clearAttachments()
    {
        $this->attachments = [];
        return $this;
    }

    /**
     * @param string $string
     * @return TemplateMailer
     */
    public function setUserAgent($string)
    {
        if (!is_string($string)) {
            return $this;
        }

        $this->userAgent = trim($string);

        return $this;
    }

    /**
     * @param string $string
     * @return TemplateMailer
     * @throws \InvalidArgumentException
     */
    public function setBody($string)
    {
        if (!$string) {
            $this->clearBody();
            return $this;
        }

        if (!is_string($string)) {
            throw new \InvalidArgumentException(
                "Body content must be as a string, %s given.",
                gettype($string)
            );
        }

        $this->body = $string;
        return $this;
    }

    /**
     * @param string $altBody
     * @return TemplateMailer
     * @throws \InvalidArgumentException
     */
    public function setAltBody($altBody)
    {
        if (!$altBody) {
            $this->clearAltBody();
            return $this;
        }

        if (!is_string($altBody)) {
            throw new \InvalidArgumentException(
                "Alt Body content must be as a string, %s given.",
                gettype($altBody)
            );
        }

        $this->altBody = $altBody;
        return $this;
    }

    /**
     * CLear Body Content
     * @return TemplateMailer
     */
    public function clearBody()
    {
        $this->body = null;
        return $this;
    }

    /**
     * CLear AltBody Content
     * @return TemplateMailer
     */
    public function clearAltBody()
    {
        $this->altBody = null;
        return $this;
    }

    /**
     * @var resource|null
     */
    protected $resourceSMTP;

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     * @param int $port
     * @param int $timeout
     * @param null $crypt
     * @param bool $keepAlive
     * @return TemplateMailer
     */
    public function asSmTP(
        $host,
        $user,
        $password,
        $port = 25,
        $timeout = 5,
        $crypt = null,
        $keepAlive = false
    ) {
        $this->protocol = 'smtp';
        if (empty($this->from)) {
            /**
             * @var Domain $domain
             */
            $domain = Loader::get(Domain::class);
            $newUser = $domain->validateEmail($user);
            if ($newUser) {
                $user = $newUser;
                $this->setFrom($user);
            }
        }

        $this->resourceSMTP = null;
        $this->smtp = [
            'host' => $host,
            'user' => $user,
            'password' => $password,
            'port' => $port,
            'timeout' => !is_numeric($timeout) ? 5 : (int) $timeout,
            'crypt' => is_string($crypt) ? trim(strtolower($crypt)) : '',
            'keepalive' => $keepAlive
        ];
        return $this;
    }

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     * @param int $port
     * @param int $timeout
     * @param null $crypt
     * @param bool $keepAlive
     * @return TemplateMailer
     */
    public static function SMTP(
        $host,
        $user,
        $password,
        $port = 25,
        $timeout = 5,
        $crypt = null,
        $keepAlive = false
    ) {
        return self::create()->asSmTP($host, $user, $password, $port, $timeout, $crypt, $keepAlive);
    }

    /**
     * @return TemplateMailer
     */
    public function setUseNoAuthSMTP()
    {
        $this->forceAuth = false;
        return $this;
    }

    /**
     * @return TemplateMailer
     */
    public function resetMethodAuthSMTP()
    {
        $this->forceAuth = null;
        return $this;
    }

    /**
     * @return TemplateMailer
     */
    public function withAuthSMTP()
    {
        $this->forceAuth = true;
        return $this;
    }

    /**
     * Test Connection
     *
     * @return array|bool|int|string
     */
    public function connectSMTP()
    {
        if (empty($this->smtp)) {
            return 1;
        }
        $proto = $this->smtp['crypt'] == 'ssl'
            ? 'ssl://'
            : '';
        $hostName = isset($_SERVER['SERVER_ADDR']) ? '['.$_SERVER['SERVER_ADDR'].']' : '[127.0.0.1]';
        $isTLS = $this->smtp['crypt'] == 'tls' ? 'tls' : null;
        $host = $proto . $this->smtp['host'];
        if (!isset($this->resourceSMTP)) {
            $this->resourceSMTP = @fsockopen(
                $host,
                $this->smtp['port'],
                $errCode,
                $errMessage,
                $this->smtp['timeout']
            );

            if ($this->resourceSMTP === false) {
                return $this->resourceSMTP = [
                    'errorCode' => $errCode,
                    'errorMessage' => $errMessage
                ];
            }

            stream_set_timeout($this->resourceSMTP, $this->smtp['timeout']);
            $reply = $this->smtpReadResponse($this->resourceSMTP);
            $result = fwrite($this->resourceSMTP, "EHLO {$hostName}\r\n");
            if ($result === false) {
                @fclose($this->resourceSMTP);
                $this->resourceSMTP = [
                    'errorCode'    => -1,
                    'errorMessage' => "Connect Failed : \n[trace before] : " . $reply
                ];
            }

            $reply = $this->smtpReadResponse($this->resourceSMTP);
            if ((int) substr(trim($reply), 0, 3) !== 250) {
                @fclose($this->resourceSMTP);
                $this->resourceSMTP = [
                    'errorCode' => -2,
                    'errorMessage' => "Connect Failure  :\n[trace before] : " . $reply
                ];
                return $this->resourceSMTP;
            }

            if ($isTLS) {
                $result = fwrite($this->resourceSMTP, "STARTTLS\r\n");
                if ($result === false) {
                    @fclose($this->resourceSMTP);
                    $this->resourceSMTP = [
                        'errorCode'    => -3,
                        'errorMessage' => "[STARTTLS] Connect Failure :\n[trace before] : " . $reply
                    ];
                }

                $reply = $this->smtpReadResponse($this->resourceSMTP);
                if ((int) substr($reply, 0, 3) !== 220) {
                    @fclose($this->resourceSMTP);
                    $this->resourceSMTP = [
                        'errorCode' => -4,
                        'errorMessage' => "[STARTTLS] Response Failure :\n[trace before] : " . $reply
                    ];

                    return $this->resourceSMTP;
                }

                $cryptResult = stream_socket_enable_crypto($this->resourceSMTP, true, STREAM_CRYPTO_METHOD_TLS_CLIENT);
                if ($cryptResult !== true) {
                    @fclose($this->resourceSMTP);
                    $this->resourceSMTP = [
                        'errorCode' => -5,
                        'errorMessage' => "[STARTTLS] Crypt Connection Failure :\n[trace before] : ".$reply
                    ];
                    return $this->resourceSMTP;
                }
            }

            $result = fwrite($this->resourceSMTP, "AUTH LOGIN\r\n");
            if ($result === false) {
                @fclose($this->resourceSMTP);
                $this->resourceSMTP = [
                    'errorCode'    => -6,
                    'errorMessage' => "[AUTH LOGIN] Connection Failure :\n[trace before] : " . $reply
                ];
            }

            $reply = $this->smtpReadResponse($this->resourceSMTP);
            if (strpos($reply, '503') === 0) {
                @fclose($this->resourceSMTP);
                return $reply;
            } elseif (strpos($reply, '334') !== 0) {
                @fclose($this->resourceSMTP);
                return $this->resourceSMTP = [
                    'errorCode'    => -7,
                    'errorMessage' => "[AUTH LOGIN] Auth Failed :\n[trace before] : " . $reply
                ];
            }

            $user   = base64_encode($this->smtp['user']);
            $result = fwrite($this->resourceSMTP, "{$user}\r\n");
            if ($result === false) {
                @fclose($this->resourceSMTP);
                return $this->resourceSMTP = [
                    'errorCode'    => -8,
                    'errorMessage' => '[AUTH LOGIN] Send User ('
                        . $this->smtp['user']
                        . ') Connection Failure :\n[trace before] : ' . $reply
                ];
            }

            $reply = $this->smtpReadResponse($this->resourceSMTP);
            if (strpos($reply, '334') !== 0) {
                @fclose($this->resourceSMTP);
                return $this->resourceSMTP = [
                    'errorCode'    => -9,
                    'errorMessage' => '[AUTH LOGIN] Send User Invalid Response :\n[trace before] : ' . $reply
                ];
            }

            $pass = base64_encode($this->smtp['password']);
            $result = fwrite($this->resourceSMTP, "{$pass}\r\n");
            if ($result === false) {
                @fclose($this->resourceSMTP);
                return $this->resourceSMTP = [
                    'errorCode'    => -10,
                    'errorMessage' => '[AUTH LOGIN] Send User Authenticate ('
                        . $this->smtp['user']
                        . ') Connection Failure :\n[trace before] : ' . $reply
                ];
            }

            $reply = $this->smtpReadResponse($this->resourceSMTP);
            if (strpos($reply, '235') !== 0) {
                return $this->resourceSMTP = [
                    'errorCode'    => -11,
                    'errorMessage' => '[AUTH LOGIN] Invalid Credentials ('
                        . $this->smtp['user']
                        . ') Connection Failure :\n[trace before] : ' . $reply
                ];
            }

            @fwrite($this->resourceSMTP, "QUIT\r\n");
            @fclose($this->resourceSMTP);
            $this->resourceSMTP = $reply;
            return $reply;
        }

        return is_string($this->smtp);
    }

    /**
     * @param resource $fp
     * @return string
     */
    private function smtpReadResponse(&$fp)
    {
        $reply = '';
        while ($str = fgets($fp, 512)) {
            $reply .= $str;
            if ($str[3] === ' ') {
                break;
            }
        }
        return $reply;
    }

    /**
     * @return TemplateMailer
     */
    public static function sendMail()
    {
        return self::create()->asSendMail();
    }

    /**
     * @return TemplateMailer
     */
    public function asSendMail()
    {
        $this->protocol = 'sendmail';
        $this->smtp = [];
        return $this;
    }

    /**
     * @return TemplateMailer
     */
    public static function mail()
    {
        return self::create()->asMail();
    }

    /**
     * @return TemplateMailer
     */
    public function asMail()
    {
        $this->protocol = 'mail';
        $this->smtp = [];
        return $this;
    }

    /**
     * Use DSN (Delivery Status Notification)
     *
     * @return TemplateMailer
     */
    public function enableDSN()
    {
        $this->isDSN = true;
        return $this;
    }

    /**
     * Disable DSN (Delivery Status Notification)
     *
     * @return TemplateMailer
     */
    public function disableDSN()
    {
        $this->isDSN = false;
        return $this;
    }

    /**
     * @return TemplateMailer
     */
    public function enableAltBody()
    {
        $this->isWithAltBody = true;
        return $this;
    }

    /**
     * @return TemplateMailer
     */
    public function disableAltBody()
    {
        $this->isWithAltBody = false;
        return $this;
    }

    /**
     * Enable Batch Mode
     *
     * @return TemplateMailer
     */
    public function enableBatchMode()
    {
        $this->isBatchMode = true;
        return $this;
    }

    /**
     * Disable BatchMode
     *
     * @return TemplateMailer
     */
    public function disableBatchMode()
    {
        $this->isBatchMode = false;
        return $this;
    }

    /**
     * Use Multipart
     *
     * @return TemplateMailer
     */
    public function enableMultiPart()
    {
        $this->isMultiPart = true;
        return $this;
    }

    /**
     * Disable Multipart
     *
     * @return TemplateMailer
     */
    public function disableMultiPart()
    {
        $this->isMultiPart = false;
        return $this;
    }

    /**
     * @param int $int
     * @return TemplateMailer
     */
    public function setBatchSize($int)
    {
        if (!is_numeric($int)) {
            throw new \InvalidArgumentException(
                'Batch Size must be as numeric',
                E_USER_ERROR
            );
        }
        if ($int < 10) {
            throw new \InvalidArgumentException(
                'Minimum batch size is 10',
                E_USER_ERROR
            );
        }

        $this->batchSize = $int;
        return $this;
    }

    /**
     * Build With Return Mailer
     */
    public function build()
    {
        if (empty($this->target)) {
            throw new \UnexpectedValueException(
                "Can not get Target! Please Set The Target Address",
                E_USER_WARNING
            );
        }

        if ($this->body == '') {
            trigger_error('No body for email!', E_USER_NOTICE);
        }

        $altBody = $this->altBody;

        if ($this->isWithAltBody && $altBody == '') {
            $altBody = str_replace('&nbsp;', ' ', strip_tags($this->body));
            $altBody = trim(preg_replace('/(\s)+/', '$1', $altBody));
        }

        $object = clone $this;
        $mailer =& $object->mailer;

        if ($this->protocol == 'smtp' && $this->forceAuth == true) {
            $mailer->smtp_host = ($this->smtp['host']);
            $mailer->smtp_user = ($this->smtp['user']);
            $mailer->smtp_pass = ($this->smtp['password']);
            $mailer->smtp_port = ($this->smtp['port'])?: 25;
            $mailer->smtp_timeout = ($this->smtp['timeout'])?: 5;
            $mailer->smtp_crypto = ($this->smtp['crypt'])?: '';
            if (!is_string($mailer->smtp_crypto)) {
                $mailer->smtp_crypto = $mailer->smtp_port == 25
                    ? ''
                    : ($mailer->smtp_port == 587
                        ? 'ssl'
                        : ''
                    );
            }

            $mailer->smtp_keepalive = (bool) $this->smtp['keepalive'];
        }

        // fix new Line
        /*$setNewLine = !empty($this->smtp)
            && preg_match('/
                \@ (
                    (?:gmail\.com)
                    |(
                        (?:yahoo|hotmail|outlook)
                        \.[a-z][a-z\.][a-z]+$
                    )
                )
                /ix', $this->from[0])
            ? "\r\n"
            : $mailer->newline;
        */
        $config =  [
            'protocol' => $this->protocol,
            'mailpath' => $object->mailer->mailpath,
            'mailtype' => ($this->isHtml ? 'html' : 'text'),
            'wordwrap' => $this->isWrapped,
            'priority' => $this->priority,
            // make new line using Standard http://www.ietf.org/rfc/rfc822.txt
            // microsoft , gmail & yahoo always use \r\n
            'newline'  => "\r\n",
            // always use UTF-8 as character sets
            'charset'  => $mailer->charset
        ];

        // init
        $mailer->initialize($config);
        $mailer->message($this->body);
        $mailer->set_alt_message($altBody);
        if ($this->protocol == 'smtp' && $this->forceAuth !== true) {
            $mailer->smtp_host = ($this->smtp['host']);
            $mailer->smtp_user = ($this->smtp['user']);
            $mailer->smtp_pass = ($this->smtp['password']);
            $mailer->smtp_port = ($this->smtp['port'])?: 25;
            $mailer->smtp_timeout = ($this->smtp['timeout'])?: 5;
            $mailer->smtp_crypto = ($this->smtp['crypt'])?: '';
            if (!is_string($mailer->smtp_crypto)) {
                $mailer->smtp_crypto = $mailer->smtp_port == 25
                    ? ''
                    : ($mailer->smtp_port == 587
                        ? 'ssl'
                        : ''
                    );
            }

            $mailer->smtp_keepalive = (bool) $this->smtp['keepalive'];
        }

        /**
         * Set Config
         */
        $mailer->useragent = $this->userAgent;
        $mailer->dsn = $this->isDSN;
        !empty($this->cc) && $mailer->cc($this->cc);
        !empty($this->bcc) && $mailer->bcc($this->bcc);
        $mailer->subject($this->subject);
        $mailer->to($this->target);
        !empty($this->from) && $mailer->from(
            reset($this->from),
            next($this->from),
            next($this->from)
        );
        // set mail type
        $mailer->set_mailtype($this->isHtml ? 'html' : 'text');
        foreach ($this->headers as $key => $value) {
            $mailer->set_header($key, $value);
        }
        foreach ($this->attachments as $value) {
            $mailer->attach(end($value), '', reset($value));
        }
        return $object;
    }

    /**
     * @return bool
     */
    public function send()
    {
        if (!isset($this->lastMailer)) {
            $this->lastMailer = $this->build();
        }

        return $this->lastMailer->mailer->send(true);
    }

    /**
     * @param array $from
     * @return array|bool[]|null[]
     */
    public function sendMulti(array $from)
    {
        $result = [];
        $setFromAutomated = false;
        if (empty($this->target)) {
            $setFromAutomated = true;
            $host = parse_url(base_url(), PHP_URL_HOST);
            $this->setTarget('temp@'.$host);
        }
        $object = $this->build();
        if ($setFromAutomated) {
            $this->target = [];
        }

        foreach ($from as $key => $value) {
            $object->setTarget($value);
            if (empty($object->target)) {
                $result[$key] = null;
                continue;
            }
            $object->mailer->to($object->target);
            $result[$key] = $object->mailer->send(false);
        }

        return $result;
    }

    /**
     * @return \CI_Email
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * Magic Method Clonse
     */
    public function __clone()
    {
        $this->mailer = clone Loader::get(\CI_Email::class);
        $this->setDefault();
    }

    /**
     * @return string
     */
    public function getAltBody()
    {
        return $this->altBody;
    }

    /**
     * @return array
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @return array
     */
    public function getAvailableProtocols()
    {
        return $this->availableProtocols;
    }

    /**
     * @return array
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return array
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @return array
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @return array
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @return array
     */
    public function getSmTP()
    {
        return $this->smtp;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return array
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @return bool
     */
    public function isHtml()
    {
        return $this->isHtml;
    }

    /**
     * @return bool
     */
    public function isWrapped()
    {
        return $this->isWrapped;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return int
     */
    public function getBatchSize()
    {
        return $this->batchSize;
    }

    /**
     * @return bool
     */
    public function isBatchMode()
    {
        return $this->isBatchMode;
    }

    /**
     * @return bool
     */
    public function isDSN()
    {
        return $this->isDSN;
    }

    /**
     * @return bool
     */
    public function isForceAuth()
    {
        return $this->forceAuth;
    }

    /**
     * @return bool
     */
    public function isMultiPart()
    {
        return $this->isMultiPart;
    }

    /**
     * @return bool
     */
    public function isWithAltBody()
    {
        return $this->isWithAltBody;
    }

    /**
     * Magic Method Destruct
     */
    public function __destruct()
    {
        $this->lastMailer = null;
        if (is_resource($this->resourceSMTP)) {
            @fclose($this->resourceSMTP);
            $this->resourceSMTP = null;
        }
    }
}
