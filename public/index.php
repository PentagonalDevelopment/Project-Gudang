<?php
/**
 * 0000000000000000000000000000000000000000
 * 0   DO NOT TOUCH ANYTHING HERE!!!!!!   0
 * 0000000000000000000000000000000000000000
 */

if (version_compare(PHP_VERSION, '5.6', '<')) {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    printf('The application required php 5.6 or laterto be installed on server, %s installed.', PHP_VERSION);

    exit(1);
}

require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'CI_Override.php';
