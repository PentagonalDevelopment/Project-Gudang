/*!
 * Domain Check
 * // gTLD
 * {link: https://data.iana.org/TLD/tlds-alpha-by-domain.txt}
 * // assign full TLD + sTLD
 * {top: https://publicsuffix.org/list/effective_tld_names.dat}
 * @author pentagonal <org@pentagonal.org>
 */
(function (global, factory) {
    "use strict";
    if ( typeof module === "object" && typeof module.exports === "object" ) {
        module.exports = global.document ?
            factory(global) :
            function(w) {
                return factory(w);
            };
    } else {
        factory(global);
    }
/* Pass this if window is not defined yet */
})(typeof window !== "undefined" ? window : this, function (window) {
    var domainCheck = function () {
        return this;
    };
    domainCheck.prototype = {
        /**
         * list extension
         * use unicode convert
         *
         * {link: https://data.iana.org/TLD/tlds-alpha-by-domain.txt}
         */
        extensions: ["aaa", "aarp", "abarth", "abb", "abbott", "abbvie", "abc", "able", "abogado", "abudhabi", "ac", "academy", "accenture", "accountant", "accountants", "aco", "active", "actor", "ad", "adac", "ads", "adult", "ae", "aeg", "aero", "aetna", "af", "afamilycompany", "afl", "africa", "ag", "agakhan", "agency", "ai", "aig", "aigo", "airbus", "airforce", "airtel", "akdn", "al", "alfaromeo", "alibaba", "alipay", "allfinanz", "allstate", "ally", "alsace", "alstom", "am", "americanexpress", "americanfamily", "amex", "amfam", "amica", "amsterdam", "analytics", "android", "anquan", "anz", "ao", "aol", "apartments", "app", "apple", "aq", "aquarelle", "ar", "arab", "aramco", "archi", "army", "arpa", "art", "arte", "as", "asda", "asia", "associates", "at", "athleta", "attorney", "au", "auction", "audi", "audible", "audio", "auspost", "author", "auto", "autos", "avianca", "aw", "aws", "ax", "axa", "az", "azure", "ba", "baby", "baidu", "banamex", "bananarepublic", "band", "bank", "bar", "barcelona", "barclaycard", "barclays", "barefoot", "bargains", "baseball", "basketball", "bauhaus", "bayern", "bb", "bbc", "bbt", "bbva", "bcg", "bcn", "bd", "be", "beats", "beauty", "beer", "bentley", "berlin", "best", "bestbuy", "bet", "bf", "bg", "bh", "bharti", "bi", "bible", "bid", "bike", "bing", "bingo", "bio", "biz", "bj", "black", "blackfriday", "blanco", "blockbuster", "blog", "bloomberg", "blue", "bm", "bms", "bmw", "bn", "bnl", "bnpparibas", "bo", "boats", "boehringer", "bofa", "bom", "bond", "boo", "book", "booking", "boots", "bosch", "bostik", "boston", "bot", "boutique", "box", "br", "bradesco", "bridgestone", "broadway", "broker", "brother", "brussels", "bs", "bt", "budapest", "bugatti", "build", "builders", "business", "buy", "buzz", "bv", "bw", "by", "bz", "bzh", "ca", "cab", "cafe", "cal", "call", "calvinklein", "cam", "camera", "camp", "cancerresearch", "canon", "capetown", "capital", "capitalone", "car", "caravan", "cards", "care", "career", "careers", "cars", "cartier", "casa", "case", "caseih", "cash", "casino", "cat", "catering", "catholic", "cba", "cbn", "cbre", "cbs", "cc", "cd", "ceb", "center", "ceo", "cern", "cf", "cfa", "cfd", "cg", "ch", "chanel", "channel", "chase", "chat", "cheap", "chintai", "chloe", "christmas", "chrome", "chrysler", "church", "ci", "cipriani", "circle", "cisco", "citadel", "citi", "citic", "city", "cityeats", "ck", "cl", "claims", "cleaning", "click", "clinic", "clinique", "clothing", "cloud", "club", "clubmed", "cm", "cn", "co", "coach", "codes", "coffee", "college", "cologne", "com", "comcast", "commbank", "community", "company", "compare", "computer", "comsec", "condos", "construction", "consulting", "contact", "contractors", "cooking", "cookingchannel", "cool", "coop", "corsica", "country", "coupon", "coupons", "courses", "cr", "credit", "creditcard", "creditunion", "cricket", "crown", "crs", "cruise", "cruises", "csc", "cu", "cuisinella", "cv", "cw", "cx", "cy", "cymru", "cyou", "cz", "dabur", "dad", "dance", "data", "date", "dating", "datsun", "day", "dclk", "dds", "de", "deal", "dealer", "deals", "degree", "delivery", "dell", "deloitte", "delta", "democrat", "dental", "dentist", "desi", "design", "dev", "dhl", "diamonds", "diet", "digital", "direct", "directory", "discount", "discover", "dish", "diy", "dj", "dk", "dm", "dnp", "do", "docs", "doctor", "dodge", "dog", "doha", "domains", "dot", "download", "drive", "dtv", "dubai", "duck", "dunlop", "duns", "dupont", "durban", "dvag", "dvr", "dz", "earth", "eat", "ec", "eco", "edeka", "edu", "education", "ee", "eg", "email", "emerck", "energy", "engineer", "engineering", "enterprises", "epost", "epson", "equipment", "er", "ericsson", "erni", "es", "esq", "estate", "esurance", "et", "etisalat", "eu", "eurovision", "eus", "events", "everbank", "exchange", "expert", "exposed", "express", "extraspace", "fage", "fail", "fairwinds", "faith", "family", "fan", "fans", "farm", "farmers", "fashion", "fast", "fedex", "feedback", "ferrari", "ferrero", "fi", "fiat", "fidelity", "fido", "film", "final", "finance", "financial", "fire", "firestone", "firmdale", "fish", "fishing", "fit", "fitness", "fj", "fk", "flickr", "flights", "flir", "florist", "flowers", "fly", "fm", "fo", "foo", "food", "foodnetwork", "football", "ford", "forex", "forsale", "forum", "foundation", "fox", "fr", "free", "fresenius", "frl", "frogans", "frontdoor", "frontier", "ftr", "fujitsu", "fujixerox", "fun", "fund", "furniture", "futbol", "fyi", "ga", "gal", "gallery", "gallo", "gallup", "game", "games", "gap", "garden", "gb", "gbiz", "gd", "gdn", "ge", "gea", "gent", "genting", "george", "gf", "gg", "ggee", "gh", "gi", "gift", "gifts", "gives", "giving", "gl", "glade", "glass", "gle", "global", "globo", "gm", "gmail", "gmbh", "gmo", "gmx", "gn", "godaddy", "gold", "goldpoint", "golf", "goo", "goodhands", "goodyear", "goog", "google", "gop", "got", "gov", "gp", "gq", "gr", "grainger", "graphics", "gratis", "green", "gripe", "grocery", "group", "gs", "gt", "gu", "guardian", "gucci", "guge", "guide", "guitars", "guru", "gw", "gy", "hair", "hamburg", "hangout", "haus", "hbo", "hdfc", "hdfcbank", "health", "healthcare", "help", "helsinki", "here", "hermes", "hgtv", "hiphop", "hisamitsu", "hitachi", "hiv", "hk", "hkt", "hm", "hn", "hockey", "holdings", "holiday", "homedepot", "homegoods", "homes", "homesense", "honda", "honeywell", "horse", "hospital", "host", "hosting", "hot", "hoteles", "hotels", "hotmail", "house", "how", "hr", "hsbc", "ht", "htc", "hu", "hughes", "hyatt", "hyundai", "ibm", "icbc", "ice", "icu", "id", "ie", "ieee", "ifm", "ikano", "il", "im", "imamat", "imdb", "immo", "immobilien", "in", "industries", "infiniti", "info", "ing", "ink", "institute", "insurance", "insure", "int", "intel", "international", "intuit", "investments", "io", "ipiranga", "iq", "ir", "irish", "is", "iselect", "ismaili", "ist", "istanbul", "it", "itau", "itv", "iveco", "iwc", "jaguar", "java", "jcb", "jcp", "je", "jeep", "jetzt", "jewelry", "jio", "jlc", "jll", "jm", "jmp", "jnj", "jo", "jobs", "joburg", "jot", "joy", "jp", "jpmorgan", "jprs", "juegos", "juniper", "kaufen", "kddi", "ke", "kerryhotels", "kerrylogistics", "kerryproperties", "kfh", "kg", "kh", "ki", "kia", "kim", "kinder", "kindle", "kitchen", "kiwi", "km", "kn", "koeln", "komatsu", "kosher", "kp", "kpmg", "kpn", "kr", "krd", "kred", "kuokgroup", "kw", "ky", "kyoto", "kz", "la", "lacaixa", "ladbrokes", "lamborghini", "lamer", "lancaster", "lancia", "lancome", "land", "landrover", "lanxess", "lasalle", "lat", "latino", "latrobe", "law", "lawyer", "lb", "lc", "lds", "lease", "leclerc", "lefrak", "legal", "lego", "lexus", "lgbt", "li", "liaison", "lidl", "life", "lifeinsurance", "lifestyle", "lighting", "like", "lilly", "limited", "limo", "lincoln", "linde", "link", "lipsy", "live", "living", "lixil", "lk", "loan", "loans", "locker", "locus", "loft", "lol", "london", "lotte", "lotto", "love", "lpl", "lplfinancial", "lr", "ls", "lt", "ltd", "ltda", "lu", "lundbeck", "lupin", "luxe", "luxury", "lv", "ly", "ma", "macys", "madrid", "maif", "maison", "makeup", "man", "management", "mango", "map", "market", "marketing", "markets", "marriott", "marshalls", "maserati", "mattel", "mba", "mc", "mcd", "mcdonalds", "mckinsey", "md", "me", "med", "media", "meet", "melbourne", "meme", "memorial", "men", "menu", "meo", "merckmsd", "metlife", "mg", "mh", "miami", "microsoft", "mil", "mini", "mint", "mit", "mitsubishi", "mk", "ml", "mlb", "mls", "mm", "mma", "mn", "mo", "mobi", "mobile", "mobily", "moda", "moe", "moi", "mom", "monash", "money", "monster", "montblanc", "mopar", "mormon", "mortgage", "moscow", "moto", "motorcycles", "mov", "movie", "movistar", "mp", "mq", "mr", "ms", "msd", "mt", "mtn", "mtr", "mu", "museum", "mutual", "mv", "mw", "mx", "my", "mz", "na", "nab", "nadex", "nagoya", "name", "nationwide", "natura", "navy", "nba", "nc", "ne", "nec", "net", "netbank", "netflix", "network", "neustar", "new", "newholland", "news", "next", "nextdirect", "nexus", "nf", "nfl", "ng", "ngo", "nhk", "ni", "nico", "nike", "nikon", "ninja", "nissan", "nissay", "nl", "no", "nokia", "northwesternmutual", "norton", "now", "nowruz", "nowtv", "np", "nr", "nra", "nrw", "ntt", "nu", "nyc", "nz", "obi", "observer", "off", "office", "okinawa", "olayan", "olayangroup", "oldnavy", "ollo", "om", "omega", "one", "ong", "onl", "online", "onyourside", "ooo", "open", "oracle", "orange", "org", "organic", "origins", "osaka", "otsuka", "ott", "ovh", "pa", "page", "pamperedchef", "panasonic", "panerai", "paris", "pars", "partners", "parts", "party", "passagens", "pay", "pccw", "pe", "pet", "pf", "pfizer", "pg", "ph", "pharmacy", "phd", "philips", "phone", "photo", "photography", "photos", "physio", "piaget", "pics", "pictet", "pictures", "pid", "pin", "ping", "pink", "pioneer", "pizza", "pk", "pl", "place", "play", "playstation", "plumbing", "plus", "pm", "pn", "pnc", "pohl", "poker", "politie", "porn", "post", "pr", "pramerica", "praxi", "press", "prime", "pro", "prod", "productions", "prof", "progressive", "promo", "properties", "property", "protection", "pru", "prudential", "ps", "pt", "pub", "pw", "pwc", "py", "qa", "qpon", "quebec", "quest", "qvc", "racing", "radio", "raid", "re", "read", "realestate", "realtor", "realty", "recipes", "red", "redstone", "redumbrella", "rehab", "reise", "reisen", "reit", "reliance", "ren", "rent", "rentals", "repair", "report", "republican", "rest", "restaurant", "review", "reviews", "rexroth", "rich", "richardli", "ricoh", "rightathome", "ril", "rio", "rip", "rmit", "ro", "rocher", "rocks", "rodeo", "rogers", "room", "rs", "rsvp", "ru", "rugby", "ruhr", "run", "rw", "rwe", "ryukyu", "sa", "saarland", "safe", "safety", "sakura", "sale", "salon", "samsclub", "samsung", "sandvik", "sandvikcoromant", "sanofi", "sap", "sapo", "sarl", "sas", "save", "saxo", "sb", "sbi", "sbs", "sc", "sca", "scb", "schaeffler", "schmidt", "scholarships", "school", "schule", "schwarz", "science", "scjohnson", "scor", "scot", "sd", "se", "search", "seat", "secure", "security", "seek", "select", "sener", "services", "ses", "seven", "sew", "sex", "sexy", "sfr", "sg", "sh", "shangrila", "sharp", "shaw", "shell", "shia", "shiksha", "shoes", "shop", "shopping", "shouji", "show", "showtime", "shriram", "si", "silk", "sina", "singles", "site", "sj", "sk", "ski", "skin", "sky", "skype", "sl", "sling", "sm", "smart", "smile", "sn", "sncf", "so", "soccer", "social", "softbank", "software", "sohu", "solar", "solutions", "song", "sony", "soy", "space", "spiegel", "spot", "spreadbetting", "sr", "srl", "srt", "st", "stada", "staples", "star", "starhub", "statebank", "statefarm", "statoil", "stc", "stcgroup", "stockholm", "storage", "store", "stream", "studio", "study", "style", "su", "sucks", "supplies", "supply", "support", "surf", "surgery", "suzuki", "sv", "swatch", "swiftcover", "swiss", "sx", "sy", "sydney", "symantec", "systems", "sz", "tab", "taipei", "talk", "taobao", "target", "tatamotors", "tatar", "tattoo", "tax", "taxi", "tc", "tci", "td", "tdk", "team", "tech", "technology", "tel", "telecity", "telefonica", "temasek", "tennis", "teva", "tf", "tg", "th", "thd", "theater", "theatre", "tiaa", "tickets", "tienda", "tiffany", "tips", "tires", "tirol", "tj", "tjmaxx", "tjx", "tk", "tkmaxx", "tl", "tm", "tmall", "tn", "to", "today", "tokyo", "tools", "top", "toray", "toshiba", "total", "tours", "town", "toyota", "toys", "tr", "trade", "trading", "training", "travel", "travelchannel", "travelers", "travelersinsurance", "trust", "trv", "tt", "tube", "tui", "tunes", "tushu", "tv", "tvs", "tw", "tz", "ua", "ubank", "ubs", "uconnect", "ug", "uk", "unicom", "university", "uno", "uol", "ups", "us", "uy", "uz", "va", "vacations", "vana", "vanguard", "vc", "ve", "vegas", "ventures", "verisign", "versicherung", "vet", "vg", "vi", "viajes", "video", "vig", "viking", "villas", "vin", "vip", "virgin", "visa", "vision", "vista", "vistaprint", "viva", "vivo", "vlaanderen", "vn", "vodka", "volkswagen", "volvo", "vote", "voting", "voto", "voyage", "vu", "vuelos", "wales", "walmart", "walter", "wang", "wanggou", "warman", "watch", "watches", "weather", "weatherchannel", "webcam", "weber", "website", "wed", "wedding", "weibo", "weir", "wf", "whoswho", "wien", "wiki", "williamhill", "win", "windows", "wine", "winners", "wme", "wolterskluwer", "woodside", "work", "works", "world", "wow", "ws", "wtc", "wtf", "xbox", "xerox", "xfinity", "xihuan", "xin", "\u0915\u0949\u092e", "\u30bb\u30fc\u30eb", "\u4f5b\u5c71", "\u0cad\u0cbe\u0cb0\u0ca4", "\u6148\u5584", "\u96c6\u56e2", "\u5728\u7ebf", "\ud55c\uad6d", "\u0b2d\u0b3e\u0b30\u0b24", "\u5927\u4f17\u6c7d\u8f66", "\u70b9\u770b", "\u0e04\u0e2d\u0e21", "\u09ad\u09be\u09f0\u09a4", "\u09ad\u09be\u09b0\u09a4", "\u516b\u5366", "\u0645\u0648\u0642\u0639", "\u09ac\u09be\u0982\u09b2\u09be", "\u516c\u76ca", "\u516c\u53f8", "\u9999\u683c\u91cc\u62c9", "\u7f51\u7ad9", "\u79fb\u52a8", "\u6211\u7231\u4f60", "\u043c\u043e\u0441\u043a\u0432\u0430", "\u049b\u0430\u0437", "\u043a\u0430\u0442\u043e\u043b\u0438\u043a", "\u043e\u043d\u043b\u0430\u0439\u043d", "\u0441\u0430\u0439\u0442", "\u8054\u901a", "\u0441\u0440\u0431", "\u0431\u0433", "\u0431\u0435\u043b", "\u05e7\u05d5\u05dd", "\u65f6\u5c1a", "\u5fae\u535a", "\u6de1\u9a6c\u9521", "\u30d5\u30a1\u30c3\u30b7\u30e7\u30f3", "\u043e\u0440\u0433", "\u0928\u0947\u091f", "\u30b9\u30c8\u30a2", "\uc0bc\uc131", "\u0b9a\u0bbf\u0b99\u0bcd\u0b95\u0baa\u0bcd\u0baa\u0bc2\u0bb0\u0bcd", "\u5546\u6807", "\u5546\u5e97", "\u5546\u57ce", "\u0434\u0435\u0442\u0438", "\u043c\u043a\u0434", "\u0435\u044e", "\u30dd\u30a4\u30f3\u30c8", "\u65b0\u95fb", "\u5de5\u884c", "\u5bb6\u96fb", "\u0643\u0648\u0645", "\u4e2d\u6587\u7f51", "\u4e2d\u4fe1", "\u4e2d\u56fd", "\u4e2d\u570b", "\u5a31\u4e50", "\u8c37\u6b4c", "\u0c2d\u0c3e\u0c30\u0c24\u0c4d", "\u0dbd\u0d82\u0d9a\u0dcf", "\u96fb\u8a0a\u76c8\u79d1", "\u8d2d\u7269", "\u30af\u30e9\u30a6\u30c9", "\u0aad\u0abe\u0ab0\u0aa4", "\u901a\u8ca9", "\u092d\u093e\u0930\u0924\u092e\u094d", "\u092d\u093e\u0930\u0924", "\u092d\u093e\u0930\u094b\u0924", "\u7f51\u5e97", "\u0938\u0902\u0917\u0920\u0928", "\u9910\u5385", "\u7f51\u7edc", "\u043a\u043e\u043c", "\u0443\u043a\u0440", "\u9999\u6e2f", "\u8bfa\u57fa\u4e9a", "\u98df\u54c1", "\u98de\u5229\u6d66", "\u53f0\u6e7e", "\u53f0\u7063", "\u624b\u8868", "\u624b\u673a", "\u043c\u043e\u043d", "\u0627\u0644\u062c\u0632\u0627\u0626\u0631", "\u0639\u0645\u0627\u0646", "\u0627\u0631\u0627\u0645\u0643\u0648", "\u0627\u06cc\u0631\u0627\u0646", "\u0627\u0644\u0639\u0644\u064a\u0627\u0646", "\u0627\u062a\u0635\u0627\u0644\u0627\u062a", "\u0627\u0645\u0627\u0631\u0627\u062a", "\u0628\u0627\u0632\u0627\u0631", "\u067e\u0627\u06a9\u0633\u062a\u0627\u0646", "\u0627\u0644\u0627\u0631\u062f\u0646", "\u0645\u0648\u0628\u0627\u064a\u0644\u064a", "\u0628\u0627\u0631\u062a", "\u0628\u06be\u0627\u0631\u062a", "\u0627\u0644\u0645\u063a\u0631\u0628", "\u0627\u0628\u0648\u0638\u0628\u064a", "\u0627\u0644\u0633\u0639\u0648\u062f\u064a\u0629", "\u0680\u0627\u0631\u062a", "\u0643\u0627\u062b\u0648\u0644\u064a\u0643", "\u0633\u0648\u062f\u0627\u0646", "\u0647\u0645\u0631\u0627\u0647", "\u0639\u0631\u0627\u0642", "\u0645\u0644\u064a\u0633\u064a\u0627", "\u6fb3\u9580", "\ub2f7\ucef4", "\u653f\u5e9c", "\u0634\u0628\u0643\u0629", "\u0628\u064a\u062a\u0643", "\u0639\u0631\u0628", "\u10d2\u10d4", "\u673a\u6784", "\u7ec4\u7ec7\u673a\u6784", "\u5065\u5eb7", "\u0e44\u0e17\u0e22", "\u0633\u0648\u0631\u064a\u0629", "\u0440\u0443\u0441", "\u0440\u0444", "\u73e0\u5b9d", "\u062a\u0648\u0646\u0633", "\u5927\u62ff", "\u307f\u3093\u306a", "\u30b0\u30fc\u30b0\u30eb", "\u03b5\u03bb", "\u4e16\u754c", "\u66f8\u7c4d", "\u0d2d\u0d3e\u0d30\u0d24\u0d02", "\u0a2d\u0a3e\u0a30\u0a24", "\u7f51\u5740", "\ub2f7\ub137", "\u30b3\u30e0", "\u5929\u4e3b\u6559", "\u6e38\u620f", "verm\u00f6gensberater", "verm\u00f6gensberatung", "\u4f01\u4e1a", "\u4fe1\u606f", "\u5609\u91cc\u5927\u9152\u5e97", "\u5609\u91cc", "\u0645\u0635\u0631", "\u0642\u0637\u0631", "\u5e7f\u4e1c", "\u0b87\u0bb2\u0b99\u0bcd\u0b95\u0bc8", "\u0b87\u0ba8\u0bcd\u0ba4\u0bbf\u0baf\u0bbe", "\u0570\u0561\u0575", "\u65b0\u52a0\u5761", "\u0641\u0644\u0633\u0637\u064a\u0646", "\u653f\u52a1", "xperia", "xxx", "xyz", "yachts", "yahoo", "yamaxun", "yandex", "ye", "yodobashi", "yoga", "yokohama", "you", "youtube", "yt", "yun", "za", "zappos", "zara", "zero", "zip", "zippo", "zm", "zone", "zuerich", "zw"],
        /**
         * Global recognize {@link https://marcelogibson.com/stackoverflow/unicode_hack.js}
         */
        idn_global_regex_char: /[^a-z0-9-]/,
        // just as I can!
        idn_custom_regex_allowed: {
            /* -------------------------------------- *
             * MAYBE NEXT?                          - *
             * -------------------------------------- */
        },
        validate: function (domainName) {
            if (typeof domainName !== 'string'
                || !domainName.match(/\./)
                || domainName.match(/(?:^[\-.])|[~!@#$%^&*()+`=\\|'{}\[\];":,\/<>?\s]|[\-]\.|\.\.|(?:[-.]$)/)
                || domainName.length > 255
            ) {
                return false;
            }

            // 0 = sub domain , 1 = gtLD, 2 gTLD
            var result = {
                    sub_domain: null,
                    name_domain: '',
                    extension_domain: ''
                },
                _domainArray = domainName.toLowerCase().split('.'),
                _lengthArray = _domainArray.length;

            result.name_domain = _domainArray[_lengthArray - 2];
            result.extension_domain = _domainArray[_lengthArray - 1];

            if (result.extension_domain.length < 2
                || this.extensions.indexOf(result.extension_domain) < 0
                || result.name_domain > 63
                /* just make sure example.(com|org) not used */
                /* || result[1] === 'example' && ['com', 'org'].indexOf(result.extension_domain) > -1 */
            ) {
                return false;
            }

            /**
             * validate if extension that contain for check
             */
            try {
                // if (typeof this.idn_custom_regex_allowed[result.extension_domain] !== 'undefined'
                //     && result[1].match(this.idn_custom_regex_allowed[result.name_domain])
                // ) {
                //     return false;
                // } else
                if (!result.name_domain.match(/^[a-z0-9]+(?:(?:[a-z0-9-]+)?[a-z0-9]$)?/)) {
                    return false;
                }
            } catch (err) {
                console.log(err);
                return false;
            }

            /**
             * if array of Domain split more than 2 ,
             * that means the domain name has sub domain
             */
            if (_lengthArray > 2) {
                _domainArray.splice(_lengthArray - 2, 2);
                result.sub_domain = _domainArray.join('.');
            }

            return result;
        },
        /**
         * Check if Domain valid
         *      this allowed sub domain validate
         * @param {string} domainName
         * @returns {boolean}
         */
        isDomain: function (domainName) {
            return (this.validate(domainName) !== false);
        },
        /**
         * Check if domain valid, only parent allowed
         *       just For TLD domain only, no suitable for domain validate usage
         *       for internationalization Domain Name
         *
         * @param {string} domainName
         * @returns {boolean}
         */
        isTopDomain: function (domainName) {
            var domainResult = this.validate(domainName);
            /**
             * validate if domain is not false (@type {object}) and sub domain is @type {null}
             */
            return (domainResult !== false && domainResult.sub_domain === null);
        },
        /**
         * Check if valid Email
         *
         * @param {string} emailAddress
         * @returns {boolean}
         */
        isEmail: function (emailAddress) {
            return (this.email(emailAddress) !== false);
        },
        /**
         * Check Email & return sanitized
         *
         * @param {string} emailAddress
         * @returns {boolean|string}
         */
        email: function (emailAddress) {
            if (typeof emailAddress !== 'string'
                || emailAddress.length < 6
                || emailAddress.match(/(?:^@)|[\s]/)
                || !emailAddress.match(/@/)
            ) {
                return false;
            }

            /**
             * Convert into lowercase and to Array
             * @type {Array}
             * @private
             */
            var _ea = emailAddress.toLowerCase().split('@');
            /**
             * if Email array more than 2 , that mean it has invalid
             */
            if (_ea.length > 2) {
                return false;
            }
            /**
             * Context
             * @type {{email: string, domain: string}}
             */
            var context = {
                email: _ea[0],
                domain: _ea[1]
            };

            /**
             * maximum length of email is 254
             * @see https://en.wikipedia.org/wiki/Email_address
             */
            if (context.email.length > 254
                /**
                 * validate if Domain valid
                 */
                || !this.isDomain(context.domain)
                /**
                 * for standard usage email address only contains:
                 * alphabetical & underscore (_) dash (-) and dotted (.)
                 */
                || context.email.match(/[^a-z0-9_\-.]/)
                /**
                 * Could not contains double dotted
                 */
                || context.email.match(/(?:\.\.)|(?:^[-_])|(?:[-_]$)/)
                /**
                 * Could not contain non alphabetical or numeric on start or end of email address
                 */
                || !context.email.match(/^[a-z0-9]/) || !context.email.match(/[a-z0-9]$/)
            ) {
                return false;
            }

            return context.email + '@' + context.domain;
        }
    };

    // fallback
    domainCheck.prototype.constructor = domainCheck;
    window.domainCheck = new domainCheck;
});
