<?php
/* ----------------------------------------- *
 *              CONFIGURATION                *
 * ----------------------------------------- */

$env = 'development';

return [
    'environment' => $env,
    'encryption_key' => '',
    /**
     * Session section
     */
    'sess_driver' => 'files',
    'sess_cookie_name' => 'ci_session',
    'sess_expiration' => null,
    'sess_save_path' => null,
    'sess_match_ip' => false,
    'sess_time_to_update' => null,
    'sess_regenerate_destroy' => false,

    /**
     * CSRF Section
     */
    'csrf_token_name'  => 'vi_token',
    'csrf_cookie_name' => 'vi_token',
    'csrf_protection' => false,
    'csrf_expire'     => 7200,
    'csrf_regenerate' => false,
    'csrf_exclude_uris' => [],

    /**
     * Cookie Section
     */
    'cookie_prefix'	    => '',
    'cookie_domain'	    => '*',
    'cookie_path'       => '/',
    'cookie_secure'	    => false,
    'cookie_httponly' 	=> false,

    /**
     * Misc
     */
    'standardize_newlines' => false,
    'global_xss_filtering' => false,
    'compress_output' => false,
    'time_reference' => 'local',
    'rewrite_short_tags' => false,
    'proxy_ips' => false,

    /**
     * Database Section
     */
    'query_builder' => true,
    'selected_database' => 'default',
    'database' => [
        'default' => [
            'dsn'	=> '',
            'hostname' => 'localhost',
            'username' => 'root',
            'password' => 'mysql',
            'database' => 'gudang',
            'dbdriver' => 'pdo',
            'subdriver' => 'mysql',
            'dbprefix' => '',
            'pconnect' => false,
            'db_debug' => $env,
            'cache_on' => false,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => false,
            'compress' => false,
            'stricton' => false,
            'failover' => [],
            'save_queries' => true
        ]
    ],
];
