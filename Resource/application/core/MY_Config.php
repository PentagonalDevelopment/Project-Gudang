<?php
namespace {

    /**
     * Class MY_Config
     */
    class MY_Config extends CI_Config
    {
        /**
         * @var array
         */
        public $config = [];

        /**
         * Constructor
         *
         * Override Config Core to Auto Detection URL if Config URL does not set
         */
        public function __construct()
        {
            // CI use reference to allow arguments take override nested
            $this->config =& get_config();
            $baseURL = !empty($this->config['base_url']) && is_string($this->config['base_url'])
            && trim($this->config['base_url']) != ''
                ? $this->config['base_url']
                : false;

            /**
             * Set As Core
             */
            $this->set_item('index_page', '');
            $this->set_item('uri_protocol', 'REQUEST_URI');
            $this->set_item('subclass_prefix', 'MY_');

            // Set the base_url automatically if none was provided
            if (empty($baseURL)) {
                /**
                 * Auto Detection URL on Code Igniter
                 */
                if (isset($_SERVER['HTTP_HOST'])) {
                    $base_url = $this->portUrlMessDetector($_SERVER['HTTP_HOST']);
                } elseif (isset($_SERVER['SERVER_NAME'])) {
                    $base_url = $this->portUrlMessDetector($_SERVER['SERVER_NAME']);
                } elseif (isset($_SERVER['SERVER_ADDR'])) {
                    // this is IPV 6 ipv 6 accessed by http(s)://[11:22:33:44]/
                    if (strpos($_SERVER['SERVER_ADDR'], ':') !== false) {
                        $base_url = $this->portUrlMessDetector('[' . $_SERVER['SERVER_ADDR'] . ']');
                    } else {
                        $base_url = $this->portUrlMessDetector($_SERVER['SERVER_ADDR']);
                    }
                } else {
                    // default host , but it will be almost impossible, if server config has not wrong!
                    $base_url = 'http://localhost/';
                }

                $this->set_item('base_url', $base_url);
            }

            parent::__construct();
        }

        /**
         * Fixing the server address & URL
         *
         * @param  string $server_address url
         * @return string base URL
         */
        protected function portUrlMessDetector($server_address)
        {
            /**
             * Base on Different port not 80 / 443
             */
            if (isset($_SERVER['SERVER_PORT'])) {
                $server_address .= $_SERVER['SERVER_PORT'] != 80 && $_SERVER['SERVER_PORT'] != 443
                    ? ':' . $_SERVER['SERVER_PORT']
                    : '';
            }

            $base_url = (is_https() ? 'https' : 'http') . '://' . $server_address
                . substr($_SERVER['SCRIPT_NAME'], 0,
                    strpos($_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME'])));
            return $base_url;
        }

        /**
         * Set cookie domain with .domain.ext for multi sub domain
         *
         * @param  string|RequestInterface|UriInterface  $domain
         * @return string $return domain ( .domain.com )
         */
        public function splitCrossDomain($domain)
        {
            // domain must be string
            if (! is_string($domain)) {
                return $domain;
            }

            // make it domain lower
            $domain = strtolower($domain);
            $domain = preg_replace('/((http|ftp)s?|sftp|xmp):\/\//i', '', $domain);
            $domain = preg_replace('/\/.*$/', '', $domain);
            $is_ip = filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
            if (!$is_ip) {
                $is_ip = filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
            }

            if (!$is_ip) {
                $parse  = parse_url('http://'.$domain.'/');
                $domain = isset($parse['host']) ? $parse['host'] : null;
                if ($domain === null) {
                    return null;
                }
            }

            if (!preg_match('/^((\[[0-9a-f:]+\])|(\d{1,3}(\.\d{1,3}){3})|[a-z0-9\-\.]+)(:\d+)?$/i', $domain)
                || $is_ip
                || $domain == '127.0.0.1'
                || $domain == 'localhost'
            ) {
                return $domain;
            }

            $domain = preg_replace('/[~!@#$%^&*()+`\{\}\]\[\/\\\'\;\<\>\,\"\?\=\|]/', '', $domain);
            if (strpos($domain, '.') !== false) {
                if (preg_match('/(.*\.)+(.*\.)+(.*)/', $domain)) {
                    $return     = '.'.preg_replace('/(.*\.)+(.*\.)+(.*)/', '$2$3', $domain);
                } else {
                    $return = '.'.$domain;
                }
            } else {
                $return = $domain;
            }
            return $return;
        }

        /**
         * {@inheritdoc}
         */
        public function set_item($item, $value)
        {
            if ($item == 'cookie_domain' && $value == '*') {
                $value = $this->splitCrossDomain($this->config['base_url']);
            }

            parent::set_item($item, $value);
        }
    }
}
