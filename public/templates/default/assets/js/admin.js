/*!
 * Admin
 * @author nawa <nawa@yahoo.com>
 */
+(function ($) {
    if (!$) return;

    // start ready
    $(document).ready(function () {
        /*--------------------------------------
         Animation
         ---------------------------------------*/
        var animationDuration;


        $('body').on('click', '.animation-demo .card-block .btn', function(){
            var animation = $(this).text();
            var cardImg = $(this).closest('.card').find('img');
            if (animation === "hinge") {
                animationDuration = 2100;
            }
            else {
                animationDuration = 1200;
            }

            cardImg.removeAttr('class');
            cardImg.addClass('animated '+animation);

            setTimeout(function(){
                cardImg.removeClass(animation);
            }, animationDuration);
        });

        $('[data-match-verify]').each(function() {
            $(this).on('keyup', function(e) {
                var target = $(this).data('match-verify');
                try {
                    var $target = $(target);
                    if ($target.length) {
                        var $this = $(this);
                        $target.on('keyup', function () {
                            $this.trigger('keyup');
                        });
                        var value = $target.val();
                        var currentValue = $this.val();
                        var matchText = $this.data('match-text') || "Invalid Value!";
                        if (value !== currentValue) {
                            $this[0].setCustomValidity(matchText);
                            $this.parent('.form-group').addClass('has-danger').removeClass('has-success');
                        } else {
                            $this[0].setCustomValidity("");
                            $this.parent('.form-group').removeClass('has-danger').addClass('has-success');
                        }
                    }
                } catch (e) {
                }
            });
        });

        /**
         * Email Validation
         */
        if (typeof domainCheck === 'object' && typeof domainCheck.isEmail === 'function') {
            $('input[type=email]').on('keyup', function () {
                var $this = $(this),
                    value = $this.val().replace(/\s*/g, ''),
                    $parent = $this.parent('.form-group');
                if (value.replace(/\s*/, '').length === 0) {
                    if (this.required) {
                        this.setCustomValidity("Please insert email");
                        if ($parent.length) {
                            $parent.addClass('has-danger').removeClass('has-success');
                        }
                    } else {
                        this.setCustomValidity("");
                        if ($parent.length) {
                            $parent.removeClass('has-danger').removeClass('has-success');
                        }
                    }
                } else if (!domainCheck.isEmail(value)) {
                    this.setCustomValidity("Please insert valid email");
                    if ($parent.length) {
                        $parent.addClass('has-danger').removeClass('has-success');
                    }
                } else {
                    this.setCustomValidity("");
                    if ($parent.length) {
                        $parent.removeClass('has-danger').addClass('has-success');
                    }
                }
                this.value = value;
            });
        }
    });

})(window.jQuery);
