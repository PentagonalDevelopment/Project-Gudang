<?php
namespace App\Model;

use App\Libraries\MetaDataCollection;
use App\Libraries\StringSanity;

/**
 * Class Options
 * @package App\Model
 */
class Options extends Model
{
    /* --------------------------------------------------
     * DEFINITION
     * ----------------------------------------------- */

    const TABLE_NAME       = 'options';

    const COLUMN_ID       = 'id';
    const COLUMN_NAME     = 'options_name';
    const COLUMN_VALUE    = 'options_value';
    const COLUMN_AUTOLOAD = 'auto_load';

    const VALUE_AUTOLOAD_YES     = 'yes';
    const VALUE_AUTOLOAD_NO      = 'no';

    /**
     * @var string
     */
    protected $databaseTableName = self::TABLE_NAME;

    /**
     * @var MetaDataCollection
     */
    protected $optionsCollections;

    /**
     * @var array
     */
    protected $inExistences = [];

    /**
     * {@inheritdoc}
     */
    protected function initBeforeLoad()
    {
        // load database first
        $this->load->database();

        $this->databaseTableName = $this->db->dbprefix(self::TABLE_NAME);
        $this->optionsCollections = new MetaDataCollection();
        $get = $this
            ->db
            ->select()
            ->where(
                'LOWER(TRIM('.$this->db->escape_identifiers(self::COLUMN_AUTOLOAD).'))',
                strtolower(self::VALUE_AUTOLOAD_YES)
            )
            ->get($this->databaseTableName);

        $result = $get->result_array();
        $get->free_result();
        foreach ($result as $value) {
            $this->optionsCollections->set($value[self::COLUMN_NAME], $value);
        }
    }

    /**
     * @param array $result
     * @return array
     */
    protected function sanityResult(array $result)
    {
        foreach ($result as $key => $value) {
            $result[$key]  = $this->getPackage(StringSanity::class)->maybeUnSerialize($value);
        }

        return $result;
    }

    /**
     * @param string $keyName
     * @param mixed $default
     * @return mixed
     */
    public function get($keyName, $default = null)
    {
        $data = $this->getFull($keyName, [self::COLUMN_VALUE => $default]);
        return $data[self::COLUMN_VALUE];
    }

    /**
     * @param string $keyName
     * @param mixed  $default
     * @return mixed
     */
    public function getFull($keyName, $default = null)
    {
        if (!is_string($keyName)) {
            return $default;
        }

        if ($this->optionsCollections->has($keyName)) {
            return $this->optionsCollections->get($keyName, $default);
        }

        if (isset($this->inExistences[$keyName])) {
            return $default;
        }

        $get = $this
            ->db
            ->select()
            ->where(self::COLUMN_NAME, $keyName)
            ->get($this->databaseTableName);

        $result = $get->result_array();
        $get->free_result();
        if (!empty($result[0])) {
            $this->optionsCollections[$keyName] = $this->sanityResult($result[0]);
            return $this->optionsCollections[$keyName];
        }

        $this->inExistences[$keyName] = true;
        return $default;
    }

    /**
     * @param string $keyName
     * @param mixed $value
     * @param bool $autoload
     * @return bool|null
     */
    public function set($keyName, $value, $autoload = false)
    {
        if (!is_string($keyName)) {
            return null;
        }

        $data = $this->getFull($keyName, -1);
        $autoload = $autoload ? self::VALUE_AUTOLOAD_YES : self::VALUE_AUTOLOAD_NO;
        if ($data === -1) {
            $result = $this->db->insert(
                $this->databaseTableName,
                [
                    self::COLUMN_NAME  => $keyName,
                    self::COLUMN_VALUE => $this->getPackage(StringSanity::class)->maybeSerialize($value),
                    self::COLUMN_AUTOLOAD => $autoload
                ]
            );

            unset($this->inExistences[$keyName]);

            $this->get($keyName);
            return $result;
        }

        $autoload = func_num_args() < 3
            ? strtolower($data[self::COLUMN_AUTOLOAD])
            : $autoload;
        ! in_array(
            $autoload,
            [self::VALUE_AUTOLOAD_YES, self::VALUE_AUTOLOAD_NO]
        ) && $autoload = self::VALUE_AUTOLOAD_NO;

        // set old to new data
        $data[self::COLUMN_AUTOLOAD] = $autoload;
        $data[self::COLUMN_VALUE]   = $value;
        $this->optionsCollections->set($keyName, $data);

        return $this
            ->db
            ->update(
                $this->databaseTableName,
                [
                   self::COLUMN_VALUE => $this->getPackage(StringSanity::class)->maybeSerialize($value),
                   self::COLUMN_AUTOLOAD  => $autoload
                ],
                [
                    self::COLUMN_NAME => $keyName
                ]
            );
    }
}
