<?php
namespace App\Libraries;

use App\Model\User;

/**
 * Class UserStatus
 * @package App\Libraries
 */
class UserStatus
{
    const STATUS_PENDING_VERIFICATION = 0;
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING_USER = 2;
    const STATUS_BANNED  = 3;
    const STATUS_NOT_LOGGED = false;
    const STATUS_UNKNOWN = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * UserStatus constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param User $user
     * @return UserStatus
     */
    public function newInstance($user = null)
    {
        $newObject = new static();
        $newObject->user = null;
        if ($user instanceof User) {
            $newObject->user = $user;
        }

        return $newObject;
    }

    /**
     * @param int $status
     * @return boolean|null true if match or null if user has not initalized
     */
    public function is($status)
    {
        return $status === $this->getStatus();
    }

    public function isPending()
    {
        $status = $this->getStatus();
        return $status != self::STATUS_UNKNOWN && $status !== null
            && !in_array(
                $status,
                [
                    self::STATUS_PENDING_USER,
                    self::STATUS_BANNED,
                    self::STATUS_PENDING_VERIFICATION,
                    self::STATUS_ACTIVE
                ]
            );
    }

    public function isActive()
    {
        return $this->getStatus() === self::STATUS_ACTIVE;
    }

    public function isBanned()
    {
        return $this->getStatus() === self::STATUS_BANNED;
    }

    public function isPendingVerification()
    {
        return $this->getStatus() === self::STATUS_PENDING_VERIFICATION;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getLevel()
    {
        if (!isset($this->user)) {
            return null;
        }
        $level = ($this->user->get('level'));
        $level = is_numeric($level) && is_int(abs($level))
            ? intval($level)
            : null;

        return $level;
    }

    /**
     * @return null|int null if not logged
     */
    public function getStatus()
    {
        if (!isset($this->user)) {
            return self::STATUS_UNKNOWN;
        }

        $level = intval($this->user->get('level'));
        // always active for admin
        if ($level === -1) {
            return self::STATUS_ACTIVE;
        }

        return (int) $this->user->get('status');
    }
}
