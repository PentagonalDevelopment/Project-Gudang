<?php
namespace App\Factory\Exceptions;

/**
 * Class ModuleNotFoundException
 * @package App\Factory\Exceptions
 */
class ModuleNotFoundException extends InvalidPathException
{
}
