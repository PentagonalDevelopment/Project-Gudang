<?php
namespace App\Model\Base;

use App\Libraries\StringSanity;
use App\Model\Model;

/**
 * Class ModelDbAbstract
 * @package App\Model\Base
 */
abstract class ModelDbAbstract extends Model implements \ArrayAccess
{
    /**
     * @var string
     */
    protected $databaseTableName;

    /**
     * {@inheritdoc}
     */
    protected function initBeforeLoad()
    {
        parent::initBeforeLoad();
        // load init
        $this->load->database();
    }

    /**
     * @param  string $key
     * @param  mixed $default
     * @return mixed
     */
    abstract public function get($key, $default = null);

    /**
     * @param string $key
     * @param mixed  $value
     * @return mixed
     */
    abstract public function set($key, $value);

    /**
     * @param string $key
     */
    abstract public function remove($key);

    /**
     * @param string $key
     * @return bool
     */
    abstract public function has($key);

    /**
     * @param array $args
     * @return static
     */
    abstract public function newInstance(array $args = []);

    /**
     * @param mixed $string
     * @return mixed
     */
    public function escape($string)
    {
        return $this->db->escape($string);
    }

    /**
     * @param string|array $column
     * @return string|array
     */
    public function createLowerColumn($column)
    {
        if (is_array($column)) {
            foreach ($column as $key => $value) {
                if (is_array($value)) {
                    throw new \InvalidArgumentException(
                        'Invalid value for escaping',
                        E_USER_ERROR
                    );
                }
                $column = $this->createLowerColumn($column);
            }
            return $column;
        }

        return "LOWER({$this->db->escape_identifiers($column)})";
    }

    /**
     * @return false|int
     */
    public function pastTimeInteger()
    {
        return @strtotime('1970-01-01 00:00:00');
    }

    /**
     * @return false|string
     */
    public function timeSQLNow()
    {
        return date('Y-m-d H:i:s', now());
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function unSerial($value)
    {
        return $this->getPackage(StringSanity::class)->maybeUnSerialize($value);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function serial($value)
    {
        return $this->getPackage(StringSanity::class)->maybeSerialize($value);
    }

    /**
     * @return string
     */
    public static function createRandomStringUniqueSha1()
    {
        $unique = self::createRandomStringUnique();
        return sha1($unique . microtime() .$unique);
    }

    /**
     * @return string
     */
    public static function createRandomStringUnique()
    {
        return uniqid(mt_rand(), true);
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->databaseTableName;
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }
}
