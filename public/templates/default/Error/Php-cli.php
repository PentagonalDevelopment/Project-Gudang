<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
?>

A PHP Error was encountered

Severity:    <?= $this->getArgument('error_severity'), "\n"; ?>
Message:     <?= $this->getArgument('error_message'), "\n"; ?>
Filename:    <?= $this->getArgument('error_filepath'), "\n"; ?>
Line Number: <?= $this->getArgument('error_line'); ?>

<?php
    if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE) :
        $error = $this->getArgument('error_exception');
?>

    Backtrace:
    <?php	foreach (debug_backtrace() as $error): ?>
        <?php		if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>
            File: <?= $error['file'], "\n"; ?>
            Line: <?= $error['line'], "\n"; ?>
            Function: <?= $error['function'], "\n\n"; ?>
        <?php		endif ?>
    <?php	endforeach ?>

<?php endif ?>
