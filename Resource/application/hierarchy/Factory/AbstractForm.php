<?php
namespace App\Factory;

use App\Factory\Helper\ConstantCollection;
use App\Libraries\HookAble;
use App\Libraries\Loader;

/**
 * Class AbstractForm
 * @package App\Factory
 */
abstract class AbstractForm
{
    const VALIDATION_OK = ConstantCollection::VALIDATION_OK;
    const VALIDATION_INVALID_DATA = ConstantCollection::VALIDATION_INVALID_DATA;
    const VALIDATION_USER_NOT_EXIST        = ConstantCollection::VALIDATION_USER_NOT_EXIST;
    const VALIDATION_USER_INVALID_PASSWORD = ConstantCollection::VALIDATION_USER_INVALID_PASSWORD;
    const VALIDATION_USER_INVALID_TOKEN    = ConstantCollection::VALIDATION_USER_INVALID_TOKEN;
    const VALIDATION_INVALID_EMAIL    = ConstantCollection::VALIDATION_INVALID_EMAIL;
    const VALIDATION_EMPTY_EMAIL    = ConstantCollection::VALIDATION_EMPTY_EMAIL;
    const VALIDATION_EMPTY_PASSWORD = ConstantCollection::VALIDATION_EMPTY_PASSWORD;
    const VALIDATION_EMPTY_TOKEN    = ConstantCollection::VALIDATION_EMPTY_TOKEN;
    const VALIDATION_INVALID_CAPTCHA = ConstantCollection::VALIDATION_INVALID_CAPTCHA;

    // login
    const HOOK_NAME_BEFORE_CHECK_LOGIN  = 'before:auth:login:check:' . __CLASS__;
    const HOOK_NAME_AFTER_CHECK_LOGIN   = 'after:auth:login:check:' . __CLASS__;

    const HOOK_NAME_AFTER_LOGIN_SUCCESS = 'after:auth:login:success:' . __CLASS__;
    const HOOK_NAME_AFTER_LOGIN_FAILED  = 'after:auth:login:failed:' . __CLASS__;
    // end
    // forgot
    const HOOK_NAME_AFTER_FORGOT_SUCCESS = 'after:auth:forgot:success:' . __CLASS__;
    const HOOK_NAME_AFTER_FORGOT_FAILED  = 'after:auth:forgot:failed:' . __CLASS__;

    const HOOK_NAME_BEFORE_CHECK_FORGOT  = 'before:auth:forgot:check:' . __CLASS__;
    const HOOK_NAME_AFTER_CHECK_FORGOT   = 'after:auth:forgot:check:' . __CLASS__;
    // end

    // login
    const HOOK_BEFORE_CREATE_SESSION_SUCCESS  = 'before:session:create:' . __CLASS__;

    const HOOK_BEFORE_CREATE_REDIRECT_LOGIN_SUCCESS = 'before:redirect:login:success:' . __CLASS__;
    const HOOK_BEFORE_CREATE_REDIRECT_LOGIN_FAILED  = 'before:redirect:login:failed:' . __CLASS__;

    // forgot
    const HOOK_BEFORE_SEND_MAIL_FORGOT_SUCCESS = 'before:mail:forgot:success:' . __CLASS__;
    const HOOK_BEFORE_CREATE_REDIRECT_FORGOT_SUCCESS = 'before:redirect:forgot:success:' . __CLASS__;
    const HOOK_BEFORE_CREATE_REDIRECT_FORGOT_FAILED  = 'before:redirect:forgot:failed:' . __CLASS__;

    // message
    const HOOK_MESSAGE_AUTH_ERROR             = 'message:auth:error:' . __CLASS__;
    const HOOK_MESSAGE_AUTH_SUCCESS           = 'message:auth:success:' . __CLASS__;
    const HOOK_MESSAGE_FORGOT_SUCCESS         = 'message:forgot:success:' . __CLASS__;
    const HOOK_MESSAGE_FORGOT_ERROR           = 'message:forgot:error:' . __CLASS__;
    const HOOK_MESSAGE_LOGOUT_SUCCESS         = 'message:auth:logout:success:' . __CLASS__;
    const FLASH_MESSAGE_AUTH                  = 'message:auth:flash:' . __CLASS__;

    // count
    const AUTH_FAIL_COUNT                     = 'count:auth:fail:'.__CLASS__;
    const AUTH_FAIL_COUNT_OLD                 = 'count:auth:fail:old:'.__CLASS__;
    const AUTH_RESET_COUNT                     = 'count:auth:reset:'.__CLASS__;
    const AUTH_RESET_COUNT_OLD                 = 'count:auth:reset:old:'.__CLASS__;

    const FLASH_MESSAGE_TYPE_SUCCESS = ConstantCollection::FLASH_MESSAGE_TYPE_SUCCESS;
    const FLASH_MESSAGE_TYPE_ERROR   = ConstantCollection::FLASH_MESSAGE_TYPE_ERROR;
    const FLASH_MESSAGE_TYPE_WARNING = ConstantCollection::FLASH_MESSAGE_TYPE_WARNING;
    const FLASH_MESSAGE_TYPE_INFO    = ConstantCollection::FLASH_MESSAGE_TYPE_INFO;

    protected $collection = [];

    /**
     * @param array $args
     * @param \Closure $success
     * @param \Closure $error
     * @return mixed
     */
    abstract public function validate(array $args, \Closure $success, \Closure $error = null);

    /**
     * @return array
     */
    public function getFormDefinition()
    {
        /**
         * @var HookAble $hook
         */
        $hook = Loader::get(HookAble::class);
        $class = get_class($this);
        $applied =  $hook->apply("form:{$class}", $this->collection);
        if (!is_array($applied)) {
            log_message('error', 'ConstantCollection for : ' . get_class($this) . 'Has Invalid return value! Fallback to default');
            return $this->collection;
        }

        $validCollection = [];
        foreach ($applied as $key => $value) {
            if (is_numeric($key) || !is_array($value)) {
                continue;
            }
            $validCollection[$key] = $value;
        }

        $applied = array_merge($this->collection, $validCollection);
        return $applied;
    }

    public function processValidation(\Closure $type, ...$params)
    {
        return call_user_func_array($type->bindTo($this), $params);
    }

    /**
     * @param int $type
     * @param string $message
     */
    public static function createFlashMessageAuth($type, $message)
    {
        /**
         * @var \CI_Session $session
         */
        $session = Loader::get('session');
        // set error Message
        $session->set_flashdata(
            static::FLASH_MESSAGE_AUTH,
            [
                'type' => $type,
                'message' => $message
            ]
        );
    }

    /**
     * @return mixed
     */
    public static function getFlashMessageAuth()
    {
        /**
         * @var \CI_Session $session
         */
        $session = Loader::get('session');
        // set error Message
        return $session->userdata(static::FLASH_MESSAGE_AUTH);
    }
}
