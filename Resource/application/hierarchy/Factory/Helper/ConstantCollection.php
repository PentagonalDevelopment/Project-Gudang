<?php
namespace App\Factory\Helper;

/**
 * Class ConstantCollection
 * @package App\Factory\Helper
 */
class ConstantCollection
{
    const HOOK_BODY_CLASS       = \MY_Loader::HOOK_BODY_CLASS;
    const HOOK_CONTENT_HEADER   = \MY_Loader::HOOK_CONTENT_HEADER;
    const HOOK_CONTENT_FOOTER   = \MY_Loader::HOOK_CONTENT_FOOTER;
    const HOOK_PREFIX           = \MY_Loader::HOOK_PREFIX;

    // message
    const HOOK_MESSAGE_VALIDATION_USER_NOT_EXIST = 'message:VALIDATION_USER_NOT_EXIST';
    const HOOK_MESSAGE_VALIDATION_USER_INVALID_PASSWORD = 'message:VALIDATION_USER_INVALID_PASSWORD';
    const HOOK_MESSAGE_VALIDATION_USER_INVALID_TOKEN = 'message:VALIDATION_USER_INVALID_TOKEN';
    const HOOK_MESSAGE_VALIDATION_INVALID_EMAIL = 'message:VALIDATION_INVALID_EMAIL';
    const HOOK_MESSAGE_VALIDATION_EMPTY_EMAIL = 'message:VALIDATION_EMPTY_EMAIL';
    const HOOK_MESSAGE_VALIDATION_EMPTY_PASSWORD = 'message:VALIDATION_EMPTY_PASSWORD';
    const HOOK_MESSAGE_VALIDATION_INVALID_DATA = 'message:VALIDATION_INVALID_DATA';

    // validation
    const VALIDATION_OK = true;
    const VALIDATION_INVALID_DATA = null;
    const VALIDATION_USER_NOT_EXIST        = 0;
    const VALIDATION_USER_INVALID_PASSWORD = -1;
    const VALIDATION_USER_INVALID_TOKEN    = -2;
    const VALIDATION_INVALID_EMAIL    = -3;
    const VALIDATION_EMPTY_EMAIL    = -4;
    const VALIDATION_EMPTY_PASSWORD = -5;
    const VALIDATION_EMPTY_TOKEN    = -6;
    const VALIDATION_INVALID_CAPTCHA = -7;

    // flash
    const FLASH_MESSAGE_TYPE_SUCCESS = 'success';
    const FLASH_MESSAGE_TYPE_ERROR   = 'danger';
    const FLASH_MESSAGE_TYPE_WARNING = 'warning';
    const FLASH_MESSAGE_TYPE_INFO    = 'info';

    const ACCOUNTS_PAGE_TEMPLATE_LIST = 'accounts:page_template_list';
}
