<?php
namespace App\Factory;

use App\Model\User;

/**
 * Class VerifyRandom
 * @package App\Factory
 */
class VerifyRandom
{
    const SELECTOR_USERNAME       = 'username';
    const SELECTOR_TIME           = 'time';
    const SELECTOR_TOKEN          = 'token';
    const SELECTOR_PASS           = 'password';
    const SELECTOR_PUBLIC_TOKEN   = 'public_token';

    /**
     * @param string $user
     * @return string
     */
    public static function hashUserName($user)
    {
        return md5(strtolower($user));
    }

    /**
     * @param User $user
     * @param int $expiredAfter
     * @return string
     */
    public static function generateSimpleTokenEncrypted(User $user, $expiredAfter = 7200)
    {
        return EncryptSimple::encrypt(
            self::generateSimpleTokenExpired($user, $expiredAfter),
            sha1(serialize(config_item('encryption_key')))
        );
    }

    /**
     * @param string $string
     * @return mixed
     */
    public static function decodeSimpleTokenEncrypted($string)
    {
        if (!is_string($string)) {
            return false;
        }

        return EncryptSimple::decrypt($string, sha1(serialize(config_item('encryption_key'))));
    }

    /**
     * @param boolean $string
     * @return bool
     */
    public static function verifySimpleTokenEncrypted($string)
    {
        return self::verifySimpleTokenExpired(
            self::decodeSimpleTokenEncrypted($string)
        );
    }

    /**
     * @param User $user
     * @param int $expiredAfter
     * @param mixed $securityKey
     * @return string
     */
    public static function generateTokenExpiredEncrypted(User $user, $expiredAfter = 7200, $securityKey = null)
    {
        return EncryptSimple::encrypt(
            self::generateTokenExpired($user, $expiredAfter),
            (func_num_args() > 2 ? $securityKey : config_item('encryption_key'))
        );
    }

    /**
     * @param boolean $string
     * @param mixed $securityKey
     * @return bool
     */
    public static function verifyTokenExpiredEncrypted($string, $securityKey = null)
    {
        return self::verifyTokenExpired(
            self::decodeTokenExpiredEncrypted(
                $string,
                (func_num_args() > 1 ? $securityKey : config_item('encryption_key'))
            )
        );
    }

    /**
     * @param $string
     * @param mixed $securityKey
     * @return mixed
     */
    public static function decodeTokenExpiredEncrypted($string, $securityKey = null)
    {
        if (!is_string($string)) {
            return false;
        }

        return EncryptSimple::decrypt(
            trim($string),
            (func_num_args() > 1 ? $securityKey : config_item('encryption_key'))
        );
    }

    /**
     * @param User $user
     * @param mixed $securityKey
     * @return string
     */
    public static function generateTokenEncrypted(User $user, $securityKey = null)
    {
        return EncryptSimple::encrypt(
            self::generateToken($user),
            (func_num_args() > 1 ? $securityKey : config_item('encryption_key'))
        );
    }

    /**
     * @param boolean $string
     * @param mixed $securityKey
     * @return bool
     */
    public static function verifyTokenEncrypted($string, $securityKey = null)
    {
        return self::verifyToken(
            self::decodeTokenEncrypted(
                $string,
                (func_num_args() > 1 ? $securityKey : config_item('encryption_key'))
            )
        );
    }

    /**
     * @param boolean $string
     * @param mixed $securityKey
     * @return mixed
     */
    public static function decodeTokenEncrypted($string, $securityKey = null)
    {
        if (!is_string($string)) {
            return false;
        }

        return EncryptSimple::decrypt(
            trim($string),
            (func_num_args() > 1 ? $securityKey : config_item('encryption_key'))
        );
    }

    /**
     * @param User $user
     * @param int $expiredAfter minimum is 5 minutes (300 second) maximum 3 days
     * @return array
     */
    public static function generateSimpleTokenExpired(User $user, $expiredAfter = 7200)
    {
        $expiredAfter = !is_int($expiredAfter) || $expiredAfter < 0
            ? 7200
            : (
            $expiredAfter < 300
                ? 300
                : (
            $expiredAfter > (3600*24*3)
                ? (3600*24*3)
                : $expiredAfter
            )
            );

        $expiredAfter = time() + $expiredAfter;
        $value = [
            // use md5
            self::SELECTOR_USERNAME => self::hashUserName($user->get(User::COLUMN_USER_NAME)),
            self::SELECTOR_TIME => $expiredAfter,
            self::SELECTOR_PASS => sha1($user->get(User::COLUMN_PASSWORD))
        ];

        $value[self::SELECTOR_TOKEN] = md5($value[self::SELECTOR_USERNAME] . $value[self::SELECTOR_TIME]);

        return $value;
    }

    /**
     * @param array $array
     * @return boolean
     */
    public static function verifySimpleTokenExpired($array)
    {
        if (!is_array($array)) {
            return false;
        }

        $dataToCheck = [
            self::SELECTOR_TIME,
            self::SELECTOR_TOKEN,
            self::SELECTOR_USERNAME,
        ];
        $isDiff = array_diff($dataToCheck, array_keys($array));
        if (!empty($isDiff)) {
            return false;
        }

        $checkTime = time();
        foreach ($dataToCheck as $check) {
            if (!isset($array[$check])) {
                return false;
            }
            if ($check === self::SELECTOR_TIME) {
                if (!is_numeric($array[$check]) || $array[$check] <= $checkTime) {
                    return false;
                }
                continue;
            }
            if (!is_string($array[$check])) {
                return false;
            }
            if ($check == self::SELECTOR_USERNAME && strlen($array[$check]) <> 32) {
                return false;
            }
        }

        return $array[self::SELECTOR_TOKEN] == md5($array[self::SELECTOR_USERNAME] . $array[self::SELECTOR_TIME]);
    }

    /**
     * @param User $user
     * @param int $expiredAfter minimum is 5 minutes (300 second) maximum 3 days
     * @return array
     */
    public static function generateTokenExpired(User $user, $expiredAfter = 7200)
    {
        $expiredAfter = !is_int($expiredAfter) || $expiredAfter < 0
            ? 7200
            : (
                $expiredAfter < 300
                ? 300
                : (
                    $expiredAfter > (3600*24*3)
                        ? (3600*24*3)
                        : $expiredAfter
                )
            );

        $expiredAfter = time() + $expiredAfter;
        $value = [
            // use md5
            self::SELECTOR_USERNAME => self::hashUserName($user->get(User::COLUMN_USER_NAME)),
            self::SELECTOR_TIME => $expiredAfter,
            self::SELECTOR_PASS => sha1($user->get(User::COLUMN_PASSWORD)),
            self::SELECTOR_PUBLIC_TOKEN => sha1($user->get(User::COLUMN_PUBLIC_TOKEN)),
        ];

        $value[self::SELECTOR_TOKEN] = sha1(
            $value[self::SELECTOR_USERNAME]
            . $value[self::SELECTOR_TIME]
            . $value[self::SELECTOR_PUBLIC_TOKEN]
        );
        return $value;
    }
    /**
     * @param array $array
     * @param User $user
     * @return bool
     */
    public static function verifySimpleTokenExpiredWithUser($array, User $user)
    {
        if (!self::verifySimpleTokenExpired($array)) {
            return false;
        }

        return $array[self::SELECTOR_PUBLIC_TOKEN] == sha1($user->get(User::COLUMN_PUBLIC_TOKEN, time()))
            && $array[self::SELECTOR_PASS] == sha1($user->get(User::COLUMN_PASSWORD, null));
    }

    /**
     * @param User $user
     * @return array
     */
    public static function generateToken(User $user)
    {
        $time = time();
        $value = [
            // use md5
            self::SELECTOR_USERNAME => self::hashUserName($user->get(User::COLUMN_USER_NAME)),
            self::SELECTOR_TIME => $time,
            self::SELECTOR_PASS => sha1($user->get(User::COLUMN_PASSWORD)),
            self::SELECTOR_PUBLIC_TOKEN => sha1($user->get(User::COLUMN_PUBLIC_TOKEN)),
        ];

        $value[self::SELECTOR_TOKEN] = sha1(
            $value[self::SELECTOR_USERNAME]
            . $value[self::SELECTOR_TIME]
            . $value[self::SELECTOR_PUBLIC_TOKEN]
            . $value[self::SELECTOR_PASS]
        );
        return $value;
    }

    /**
     * @param array $array
     * @return boolean
     */
    public static function verifyTokenExpired($array)
    {
        if (!is_array($array)) {
            return false;
        }

        $dataToCheck = [
            self::SELECTOR_PUBLIC_TOKEN,
            self::SELECTOR_TIME,
            self::SELECTOR_TOKEN,
            self::SELECTOR_USERNAME,
        ];
        $isDiff = array_diff($dataToCheck, array_keys($array));
        if (!empty($isDiff)) {
            return false;
        }

        $checkTime = time();
        foreach ($dataToCheck as $check) {
            if (!isset($array[$check])) {
                return false;
            }
            if ($check === self::SELECTOR_TIME) {
                if (!is_numeric($array[$check]) || $array[$check] <= $checkTime) {
                    return false;
                }
                continue;
            }
            if (!is_string($array[$check])) {
                return false;
            }
            if ($check == self::SELECTOR_USERNAME && strlen($array[$check]) <> 32) {
                return false;
            }
        }

        return $array[self::SELECTOR_TOKEN] == sha1(
            $array[self::SELECTOR_USERNAME]
                . $array[self::SELECTOR_TIME]
                . $array[self::SELECTOR_PUBLIC_TOKEN]
                . $array[self::SELECTOR_PASS]
        );
    }

    /**
     * @param array $array
     * @param User $user
     * @return bool
     */
    public static function verifyTokenExpiredWithUser($array, User $user)
    {
        if (!self::verifyTokenExpired($array)) {
            return false;
        }

        return $array[self::SELECTOR_PUBLIC_TOKEN] == sha1($user->get(User::COLUMN_PUBLIC_TOKEN, time()))
            && $array[self::SELECTOR_PASS] == sha1($user->get(User::COLUMN_PASSWORD, null));
    }

    /**
     * @param array $array
     * @return bool
     */
    public static function verifyToken($array)
    {
        if (!is_array($array)) {
            return false;
        }

        $dataToCheck = [
            self::SELECTOR_PUBLIC_TOKEN,
            self::SELECTOR_TIME,
            self::SELECTOR_TOKEN,
            self::SELECTOR_USERNAME,
        ];

        $isDiff = array_diff($dataToCheck, array_keys($array));
        if (!empty($isDiff)) {
            return false;
        }

        $checkTime = time() + 10;
        foreach ($dataToCheck as $check) {
            if (!isset($array[$check])) {
                return false;
            }
            if ($check === self::SELECTOR_TIME) {
                if (!is_numeric($array[$check]) || $array[$check] > $checkTime) {
                    return false;
                }
                continue;
            }
            if (!is_string($array[$check])) {
                return false;
            }
            if ($check == self::SELECTOR_USERNAME && strlen($array[$check]) <> 32) {
                return false;
            }
        }

        return $array[self::SELECTOR_TOKEN] == sha1(
            $array[self::SELECTOR_USERNAME]
            . $array[self::SELECTOR_TIME]
            . $array[self::SELECTOR_PUBLIC_TOKEN]
            . $array[self::SELECTOR_PASS]
        );
    }

    /**
     * @param array $array
     * @param User $user
     * @return bool
     */
    public static function verifyTokenWithUser($array, User $user)
    {
        if (!self::verifyToken($array)) {
            return false;
        }

        return $array[self::SELECTOR_PUBLIC_TOKEN] == sha1($user->get(User::COLUMN_PUBLIC_TOKEN, time()))
            && $array[self::SELECTOR_PASS] == sha1($user->get(User::COLUMN_PASSWORD, null));
    }
}
