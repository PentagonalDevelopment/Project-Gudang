<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}

$this->setArgument('title', '500 Internal Server Error');
$this->headerPart();
?>
    <section class="error">
        <div class="error__inner">
            <h1>500</h1>
            <h2>Internal Server Error!</h2>
            <?= $this->getArgument('error_message'); ?>
        </div>
    </section>
<?php
$this->footerPart();
