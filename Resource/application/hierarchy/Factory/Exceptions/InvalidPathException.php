<?php
namespace App\Factory\Exceptions;

use Exception;

/**
 * Class InvalidPathException
 * @package MongoDB\Driver\Exception
 */
class InvalidPathException extends Exception
{
    /**
     * @var string
     */
    protected $path;

    /**
     * InvalidPathException constructor.
     * @param string $path
     * @param string $message
     */
    public function __construct($path, $message = '')
    {
        $this->path = $path;
        if (func_num_args() > 1) {
            $this->message = $message;
        } else {
            $this->message = "Invalid path of {$path}";
        }
    }

    /**
     * Get Path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
