<?php
namespace {

    use App\Factory\AbstractForm;
    use App\Factory\Form\Forgot;
    use App\Factory\Form\Login;
    use App\Factory\Helper\ConstantCollection;
    use App\Factory\SimpleShortCutHtmlParser;
    use App\Factory\TemplateMailer;
    use App\Factory\Validator\Domain;
    use App\Factory\VerifyRandom;
    use App\Libraries\Arguments;
    use App\Libraries\HookAble;
    use App\Libraries\Loader;
    use App\Libraries\ModuleCollection;
    use App\Model\CurrentUser;
    use App\Model\Options;
    use App\Model\User;

    /**
     * Class Login
     */
    class Accounts extends MY_Controller
    {
        const ACCOUNTS_PATH = 'accounts';
        const LOGIN_PATH    = 'accounts/login';
        const FORGOT_PATH   = 'accounts/forgot';
        const LOGOUT_PATH   = 'accounts/logout';
        const REGISTER_PATH = 'accounts/register';
        const VERIFY_PATH   = 'accounts/verify';
        const PROFILE_PATH  = 'accounts/profile';
        const PROFILE_PATH_EDIT = 'accounts/profile/edit';

        public $__login_route_path = self::LOGIN_PATH;
        public $__forgot_route_path = self::FORGOT_PATH;
        public $__logout_route_path = self::LOGOUT_PATH;
        public $__register_route_path = self::REGISTER_PATH;
        public $__verify_route_path  = self::VERIFY_PATH;

        protected $_titles_templates = [
            self::LOGIN_PATH => 'Please Login',
            self::FORGOT_PATH => 'Reset Your Accounts',
            self::REGISTER_PATH => 'Register New Account',
            self::LOGOUT_PATH => 'Logout',
            self::PROFILE_PATH => 'My Account',
            self::PROFILE_PATH_EDIT => 'Edit Account',
            self::VERIFY_PATH => 'Verify Account',
        ];

        public function index()
        {
            $this->_validate();
        }

        public function logout()
        {
            /**
             * @var CurrentUser $currentUser
             * @var HookAble $hook
             */
            $currentUser = $this[CurrentUser::class];
            $hook = $this[HookAble::class];
            if ($currentUser->isLoggedLogin()) {
                // add message
                $message = sprintf('You Have Been Successfully Logged Out from %s', $currentUser->getUserDataBy('user_name'));
                $messageHook = $hook->apply(Login::HOOK_MESSAGE_LOGOUT_SUCCESS, $message, $currentUser);
                $messageHook = is_string($messageHook) || is_numeric($messageHook)
                || is_null($messageHook) || is_bool($messageHook)
                    ? $messageHook
                    : $message;
                Login::createFlashMessageAuth(Login::FLASH_MESSAGE_TYPE_SUCCESS, $messageHook);

                $currentUser->destroyLoggedLogin();
                redirect(base_url());
            } else {
                // add message
                $message = sprintf('You Have Been Successfully Logged Out');
                $messageHook = $hook->apply(Login::HOOK_MESSAGE_LOGOUT_SUCCESS, $message, $currentUser);
                $messageHook = is_string($messageHook) || is_numeric($messageHook)
                || is_null($messageHook) || is_bool($messageHook)
                    ? $messageHook
                    : $message;
                Login::createFlashMessageAuth(Login::FLASH_MESSAGE_TYPE_SUCCESS, $messageHook);

                $currentUser->destroyLogin();
            }

            redirect(base_url($this->__login_route_path));
        }

        public function verify()
        {
            if ($this->_isLogin()) {
                show_404();
            }

            if (count($this->uri->segments) <> 3) {
                show_404();
            }
            // $encrypted = $this->uri->segment(3);
            show_404('Token Invalid');
        }

        public function login()
        {
            $this->_isLogin() && redirect(base_url());
            $this->_validate();
        }

        public function register()
        {
            $this->_isLogin() && redirect(base_url());
            $this->_validate();
        }

        public function forgot()
        {
            $this->_isLogin() && redirect(base_url());
            $this->_validate();
        }

        public function profile()
        {
            $this->_validate();
        }

        /**
         * @param int $code
         * @return string
         */
        protected function determineErrorMessage($code)
        {
            /**
             * @var HookAble $hook
             */
            $hook = Loader::get(HookAble::class);
            switch ($code) :
                case Login::VALIDATION_USER_NOT_EXIST:
                    return $hook->apply(ConstantCollection::HOOK_MESSAGE_VALIDATION_USER_NOT_EXIST, 'User Does not Exists!');
                case Login::VALIDATION_USER_INVALID_PASSWORD:
                    return $hook->apply(ConstantCollection::HOOK_MESSAGE_VALIDATION_USER_INVALID_PASSWORD, 'Password is invalid!');
                case Login::VALIDATION_USER_INVALID_TOKEN:
                case Login::VALIDATION_EMPTY_TOKEN:
                    return $hook->apply(ConstantCollection::HOOK_MESSAGE_VALIDATION_USER_INVALID_TOKEN, 'Token invalid!');
                case Login::VALIDATION_INVALID_EMAIL:
                    return $hook->apply(ConstantCollection::HOOK_MESSAGE_VALIDATION_INVALID_EMAIL, 'Email invalid!');
                case Login::VALIDATION_EMPTY_EMAIL:
                    return $hook->apply(ConstantCollection::HOOK_MESSAGE_VALIDATION_EMPTY_EMAIL, 'Email is empty!');
                case Login::VALIDATION_EMPTY_PASSWORD:
                    return $hook->apply(ConstantCollection::HOOK_MESSAGE_VALIDATION_EMPTY_PASSWORD, 'Password could not be empty!');
            endswitch;

            return $hook->apply(ConstantCollection::HOOK_MESSAGE_VALIDATION_INVALID_DATA, 'Invalid credential data receive!');
        }

        protected function _validate()
        {
            // check & validate
            $this->_loginCheckRedirect();
            $fullUri = strtolower($this->uri->uri_string());
            $template_list = $this->_titles_templates;
            $template_list_array = $this[HookAble::class]->apply(
                ConstantCollection::ACCOUNTS_PAGE_TEMPLATE_LIST,
                $template_list
            );
            if (is_array($template_list_array)) {
                foreach ($template_list_array as $key => $value) {
                    if (is_string($key) && is_string($value)) {
                        $this->_titles_templates[$key] = $value;
                    }
                }
            }

            if ($this->input->method(true) == 'POST') {
                /**
                 * @var AbstractForm $form
                 */
                switch ($fullUri) :
                    case $this->__logout_route_path:
                            $this->logout();
                        return;
                    case $this->__verify_route_path:
                            $this->verify();
                        return;
                    case self::ACCOUNTS_PATH:
                            redirect(self::PROFILE_PATH);
                        return;
                    case $this->__register_route_path:
                            $this->_validate_registration();
                        break;
                    case $this->__login_route_path:
                            $this->_validate_login();
                        break;
                    case $this->__forgot_route_path:
                            $this->_validate_reset();
                        break;
                    case self::PROFILE_PATH_EDIT:
                            $this->_validate_edit_profile();
                        break;
                    default:
                            $callable = $this->l_hookAble->apply('accounts:action:'.$fullUri, null);
                            if (is_callable($callable) && $callable instanceof \Closure) {
                                $callable->call($this);
                            }
                endswitch;

                redirect($this->uri->uri_string());
            }

            if ($fullUri == self::ACCOUNTS_PATH) {
                redirect(self::PROFILE_PATH);
                return;
            }

            if (!$this->uri->segment(2) || !isset($this->_titles_templates[$fullUri])) {
                show_404();
            }

            $this[HookAble::class]
                ->add(
                    MY_Loader::HOOK_BODY_CLASS,
                    function ($body) use ($fullUri) {
                        $body[] = 'page-' . str_replace('/', '-', $fullUri);
                        return $body;
                    }
                );

            $this->load->view(ucwords($fullUri, '/'));
            $this->_setArgument('title', $this->_titles_templates[$fullUri]);
        }

        /* -------------------------------------------------------- *
         *                                                          *
         *                          VALIDATION                      *
         *                                                          *
         * -------------------------------------------------------- */

        protected function _validate_edit_profile()
        {
        }

        protected function _validate_reset()
        {
            $controller =& $this;
            /**
             * Validate
             * @var  AbstractForm $form
             */
            $form = $this[Forgot::class];
            $form->validate(
                $this->input->post(),
                function (User $user) use ($controller) {
                    /**
                     * @var HookAble $hook
                     */
                    $hook = Loader::get(HookAble::class);
                    // HOOK -------------------
                    $hook->apply(Login::HOOK_BEFORE_SEND_MAIL_FORGOT_SUCCESS, $user);
                    // send email
                    $controller->_sendForgotMail($user);
                    // add message -----------
                    $message = 'Please check your mail box to reset your password';
                    $messageHook = $hook->apply(Login::HOOK_MESSAGE_FORGOT_SUCCESS, $message);
                    $messageHook = is_string($messageHook) || is_numeric($messageHook)
                        || is_null($messageHook) || is_bool($messageHook)
                        ? $messageHook
                        : $message;
                    Login::createFlashMessageAuth(Login::FLASH_MESSAGE_TYPE_SUCCESS, $messageHook);
                    // HOOK -------------------
                    $uriRedirect = $hook->apply(Login::HOOK_BEFORE_CREATE_REDIRECT_LOGIN_SUCCESS, $controller->__login_route_path);
                    $uriRedirect = is_string($uriRedirect) ? $uriRedirect : $controller->__login_route_path;
                    redirect($uriRedirect);
                },
                function () {}
            );
        }

        protected function _sendForgotMail(User $user)
        {
            $args = [
                'company_name' => 'My Company',
                'current_year' => date('Y'),
                'logo_text'   => 'My Awesome Company',
                'verify_url' => base_url('verify/'.VerifyRandom::generateSimpleTokenEncrypted($user, 3600)),
                'expired_time_human' => '1 hour',
                'expired_time_clock' => date('Y-m-d H:i', strtotime('+1 hour')),
                'user_first_name' => $user->get(User::COLUMN_FIRST_NAME),
                'user_email' => $user->get(User::COLUMN_EMAIL)
            ];
            $data = SimpleShortCutHtmlParser::createFromString(
                file_get_contents(SOURCE_DIR .'/email/forgot.email.tpl'),
                new Arguments($args)
            );
            TemplateMailer::sendMail()
                ->setFromName('Pentagonal Development')
                ->setTarget($user->get(User::COLUMN_EMAIL))
                ->asHtml()
                ->setBody($data->getDataParsed())
                ->setSubject('Request Password Reset')
                ->send();
        }

        protected function _validate_registration()
        {
        }

        /**
         * Login Validation
         */
        protected function _validate_login()
        {
            $controller =& $this;
            /**
             * Validate
             * @var  AbstractForm $form
             */
            $form = $this[Login::class];
            $form->validate(
                $this->input->post(),
                function (User $user) {
                    /**
                     * @var CurrentUser $current
                     * @var CI_Session $session
                     * @var HookAble $hook
                     */
                    $session = Loader::get('session');
                    $current = Loader::get(CurrentUser::class);
                    $hook = Loader::get(HookAble::class);
                    // HOOK -------------------
                    $hook->apply(Login::HOOK_BEFORE_CREATE_SESSION_SUCCESS, $user);
                    $current->createSession($user);

                     // add message -----------
                    $message = 'Successfully Logged in';
                    $messageHook = $hook->apply(Login::HOOK_MESSAGE_AUTH_SUCCESS, $message);
                    $messageHook = is_string($messageHook) || is_numeric($messageHook)
                    || is_null($messageHook) || is_bool($messageHook)
                        ? $messageHook
                        : $message;
                    Login::createFlashMessageAuth(Login::FLASH_MESSAGE_TYPE_SUCCESS, $messageHook);

                    // set redirect
                    $redirect_uri = $session->flashdata(MY_Controller::URI_KEY_REDIRECT);
                    // HOOK -------------------
                    $uriRedirect = $hook->apply(Login::HOOK_BEFORE_CREATE_REDIRECT_LOGIN_SUCCESS, $redirect_uri);
                    $uriRedirect = is_string($uriRedirect) ? $uriRedirect : $redirect_uri;
                    redirect($uriRedirect);
                },
                function ($type) use ($controller) {
                    $fullUri = strtolower($controller->uri->uri_string());
                    /**
                     * @var CI_Session $session
                     * @var HookAble $hook
                     */
                    $session = Loader::get('session');
                    $hook = Loader::get(HookAble::class);
                    $message =  $controller->determineErrorMessage($type);
                    $messageHook = $hook->apply(Login::HOOK_MESSAGE_AUTH_ERROR, $message, $type);
                    $messageHook = is_string($messageHook) || is_numeric($messageHook)
                        || is_null($messageHook) || is_bool($messageHook)
                        ? $messageHook
                        : $message;
                    Login::createFlashMessageAuth(Login::FLASH_MESSAGE_TYPE_ERROR, $messageHook);
                    // count fail
                    $failCount = $session->userdata(Login::AUTH_FAIL_COUNT);
                    $failCount = is_numeric($failCount)
                        ? (int) $failCount
                        : 0;
                    $failCount = $failCount < 0 ? 1 : $failCount+1;
                    $session->set_userdata(Login::AUTH_FAIL_COUNT, $failCount);
                    // add hook failed before redirect
                    $hook->apply(Login::HOOK_BEFORE_CREATE_REDIRECT_LOGIN_FAILED, $type);
                    redirect($fullUri);
                }
            );
        }
    }
}
