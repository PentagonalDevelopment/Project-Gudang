-- ---------------------------------------------------- --
--                                                      --
--                    CRM (PLAN)                        --
--                                                      --
-- ---------------------------------------------------- --
--                                                      --
-- author       : pentagonal development                --
-- framework    : CodeIgniter 3.1.5                     --
-- customized   :                                       --
--      + Core                                          --
--      + Modular                                       --
--                                                      --
-- year / month : 2017 / July                           --
--                                                      --
-- Team Dev :                                           --
--      + nawa <nawa@yahoo.com>                         --
--      + Gery Permana <permana.gery@gmail.com>         --
--      + .... you!                                     --
--                                                      --
-- ---------------------------------------------------- --


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------
--
--  table `users`
--
-- --------------------------------------------------------

CREATE TABLE `users` (
  `id`            BIGINT(10),
  `user_name`     VARCHAR(64) NOT NULL,
  `email`         VARCHAR(255) NOT NULL,
  `password`      VARCHAR(60),
  `first_name`    VARCHAR(120),
  `last_name`     VARCHAR(255),
  `level`         INT(10) NOT NULL DEFAULT 0 COMMENT 'CurrentUser Level default 0 is `standard user`',
  `status`        INT(10) NOT NULL DEFAULT 0 COMMENT 'CurrentUser Status default 0 is `pending`',
  `public_token`  VARCHAR(128) DEFAULT NULL,
  `private_token` VARCHAR(128) DEFAULT NULL,
  `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    TIMESTAMP NOT NULL DEFAULT '1990-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE INDEX `unique_identity` (`user_name`, `email`);

ALTER TABLE `users`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

-- --------------------------------------------------------
--
--  table `users_meta`
--
-- --------------------------------------------------------

CREATE TABLE `users_meta` (
  `id`         BIGINT(10),
  `user_id`    BIGINT(10),
  `meta_name`  TEXT COMMENT 'CurrentUser meta name for user property',
  `meta_value` LONGTEXT,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT '1990-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `users_meta`
  ADD PRIMARY KEY (`id`),
  ADD INDEX `index_user_id` (`user_id`);

ALTER TABLE `users_meta`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;


-- --------------------------------------------------------
--
--  table `users_role`
--
-- --------------------------------------------------------

CREATE TABLE `users_role` (
  `role_id`          BIGINT(10),
  `role_name`        VARCHAR(255) COMMENT 'Unique Role Name',
  `role_identity`    BIGINT(10)   COMMENT 'Unique Role ID',
  `role_access`      TEXT,
  `role_description` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `users_role`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE INDEX `unique_identity` (`role_name`, `role_identity`);


-- --------------------------------------------------------
--
--  table `options`
--
-- --------------------------------------------------------

CREATE TABLE `options` (
  `id`            BIGINT(10),
  `options_name`  VARCHAR(255) COMMENT 'Unique options name for settings',
  `options_value` LONGTEXT,
  `auto_load` VARCHAR(100) NOT NULL DEFAULT 'no' COMMENT 'auto load for load on first init'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE `options_unique_name` (`options_name`);

ALTER TABLE `options`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

-- --------------------------------------------------------
--
--  table `email_templates`
--
-- --------------------------------------------------------

CREATE TABLE `email_templates` (
  `id`             BIGINT(10),
  `email_name`     VARCHAR(255) COMMENT 'Unique Email Name',
  `email_value`    LONGTEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE INDEX `unique_identity` (`email_name`);


-- --------------------------------------------------------
--
--  Dumping Data For table : `users`
--
-- --------------------------------------------------------

REPLACE INTO `users` (
    `id`,
    `user_name`,
    `email`,
    password,
    `first_name`,
    `last_name`,
    `level`,
    `status`,
    `public_token`,
    `private_token`
) VALUES (
    1,                        -- id
    'admin',                  -- username
    'admin@example.com',      -- email
    'password',               -- password default `password`
    'administrator',          -- first name
    '',                       -- last name
    -1,                       -- role level
    1,                        -- status
    sha2('now()', 512),       -- public token
    sha2((now() * 30), 512)   -- private token
);
