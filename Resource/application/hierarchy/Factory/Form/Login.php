<?php
namespace App\Factory\Form;

use App\Factory\AbstractForm;
use App\Libraries\HookAble;
use App\Libraries\Loader;
use App\Libraries\Pass;
use App\Model\CurrentUser;
use App\Model\User;

/**
 * Class Login
 * @package App\Factory\Form
 */
class Login extends AbstractForm
{
    protected $collection = [];

    /**
     * Login constructor.
     */
    public function __construct()
    {
        /**
         * @var \MY_Security $security
         * @var \CI_Input $input
         * @var \CI_Session $session
         */
        $security = Loader::get('security');
        $input  = Loader::get('input');
        $session = Loader::get('session');
        if ($input->method(true) == 'POST') {
            $session->set_flashdata('post:user[email]', $input->post('user[email]'));
        }

        $this->collection = [
            'user[email]' => [
                'id' => 'user_login_email',
                'label' => 'Email Address',
                'type'  => 'email',
                'value' => $session->flashdata('post:user[email]'),
                'required' => 'required',
            ],
            'user[password]' => [
                'id' => 'user_login_password',
                'label' => 'Password',
                'type' => 'password',
                'minlength' => 3,
                'value' => null,
                'required' => 'required',
            ],
            "user[{$security->get_csrf_token_name()}]" => [
                'type' => 'hidden',
                'value' => $security->get_csrf_hash()
            ],
        ];
    }

    public function validate(array $args, \Closure $success, \Closure $error = null)
    {
        $error   = $error ?: function () {
        };

        /**
         * @var \MY_Security $security
         * @var HookAble $hook
         */
        $security = Loader::get(\MY_Security::class);
        $hook = Loader::get(HookAble::class);
        $key     = [
            'email',
            'password',
            $security->get_csrf_token_name()
        ];

        if (empty($args['user']) || !is_array($args['user'])) {
            $hook
                ->apply(
                    self::HOOK_NAME_AFTER_LOGIN_FAILED,
                    self::VALIDATION_INVALID_DATA,
                    $args,
                    $success,
                    $error
                );
            return $this->processValidation($error, self::VALIDATION_INVALID_DATA);
        }

        /**
         * add ConstantCollection Before Login Check
         */
        $hook->apply(self::HOOK_NAME_BEFORE_CHECK_LOGIN, $args, $this, $success, $error);

        $args = $args['user'];
        foreach ($key as $check) {
            if (! isset($args[$check])
                || ! is_string($args[$check])
                || trim($args[$check]) == ''
            ) {
                if ($security->get_csrf_token_name() == $check) {
                    $check = 'token';
                }
                $returnValue = self::VALIDATION_INVALID_DATA;
                switch ($check) {
                    case 'email':
                            $returnValue = self::VALIDATION_EMPTY_EMAIL;
                        break;
                    case 'password':
                            $returnValue = self::VALIDATION_EMPTY_PASSWORD;
                        break;
                    case 'token':
                        $returnValue = self::VALIDATION_EMPTY_TOKEN;
                        break;
                }
                $hook
                    ->apply(
                        self::HOOK_NAME_AFTER_LOGIN_FAILED,
                        $returnValue,
                        $args,
                        $success,
                        $error
                    );
                return $this->processValidation($error, $returnValue);
            }
        }

        if (!$security->verifyCSRFToken($args[$security->get_csrf_token_name()])) {
            $hook
                ->apply(
                    self::HOOK_NAME_AFTER_LOGIN_FAILED,
                    self::VALIDATION_USER_INVALID_TOKEN,
                    $args,
                    $success,
                    $error
                );
            return $this->processValidation($error, self::VALIDATION_USER_INVALID_TOKEN);
        }

        /**
         * @var User $user
         */
        $user = User::fromEmail($args['email']);
        if (!$user instanceof User || ! $user->isExists()) {
            $hook
                ->apply(
                    self::HOOK_NAME_AFTER_LOGIN_FAILED,
                    self::VALIDATION_USER_NOT_EXIST,
                    $args,
                    $success,
                    $error
                );
            return $this->processValidation($error, self::VALIDATION_USER_NOT_EXIST);
        }

        /**
         * @var Pass $pass
         */
        $pass = Loader::get(Pass::class);
        if (!$pass->verify(sha1($args['password']), $user->get('password'))) {
            $hook
                ->apply(
                    self::HOOK_NAME_AFTER_LOGIN_FAILED,
                    self::VALIDATION_USER_INVALID_PASSWORD,
                    $args,
                    $success,
                    $error
                );

            return $this->processValidation($error, self::VALIDATION_USER_INVALID_PASSWORD);
        }

        $callback = $hook
            ->apply(
                self::HOOK_NAME_AFTER_LOGIN_SUCCESS,
                $success,
                $user,
                $args,
                $error
            );

        return $this->processValidation(
            $callback instanceof \Closure ? $callback : $success,
            $user,
            $args
        );
    }
}
