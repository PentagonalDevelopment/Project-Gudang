<?php
namespace App\Factory;

use App\Libraries\Arguments;

/**
 * Class SimpleShortCutHtmlParser
 * @package App\Factory
 */
class SimpleShortCutHtmlParser
{
    /**
     * @var string
     */
    protected $data;

    /**
     * @var array
     */
    protected $extracted = [];

    /**
     * @var Arguments
     */
    protected $arguments;

    /**
     * @var string
     */
    protected $data_parsed;

    /**
     * @var string
     */
    protected $stripped;

    /**
     * SimpleShortCutHtmlParser constructor.
     * @param string $templateString
     * @param Arguments|null $arguments
     */
    public function __construct($templateString = '', Arguments $arguments = null)
    {
        if (!is_string($templateString)) {
            throw new \InvalidArgumentException(
                "Invalid argument for string content!",
                E_USER_ERROR
            );
        }
        $this->data = $templateString;
        $this->arguments = $arguments?: new Arguments();
    }

    /**
     * @param string $string
     * @param Arguments|null $arguments
     * @return static
     */
    public static function createFromString($string, Arguments $arguments = null)
    {
        $arguments = $arguments?: new Arguments();
        $object = new static($string, $arguments);
        return $object;
    }

    /**
     * Get Arguments
     *
     * @return Arguments
     */
    public function &getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param string $string
     * @return array
     */
    protected function extractShortcut($string)
    {
        preg_match_all('/\{\{(?P<extracted>[a-z0-9\_\-\.]+)\}\}/', $string, $match);

        return isset($match['extracted'])
            ? array_unique($match['extracted'])
            : [];
    }

    /**
     * @param string $string
     * @return mixed
     */
    protected function cleanComment($string)
    {
        return preg_replace('/(?:[ \n]+)?(?:<!--(?:.*?)-->)(?:(\/[a-z][^>]>)\s*(<))?/smi', '$1$2', $string);
    }

    protected function stripParse()
    {
        if (!isset($this->stripped)) {
            $this->stripped = strip_tags($this->data_parsed);
        }

        return $this;
    }

    /**
     * @return SimpleShortCutHtmlParser
     */
    public function parse()
    {
        if (isset($this->data_parsed)) {
            return $this;
        }

        $this->data_parsed = ltrim($this->cleanComment($this->data));
        $this->extracted = $this->extractShortcut($this->data_parsed);
        if ($this->data_parsed) {
            $quote = array_map(function ($data) {
                return '/'.preg_quote('{{'.$data.'}}', '/').'/';
            }, $this->extracted);
            $values = [];
            foreach ($this->extracted as $key => $value) {
                $value = $this->arguments->get($value, '');
                settype($value, 'string');
                $values[] = $value;
            }
            $this->data_parsed = preg_replace($quote, $values, $this->data_parsed);
            $this->stripParse();
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getExtracted()
    {
        return $this->parse()->extracted;
    }

    /**
     * @return string
     */
    public function getStripped()
    {
        return $this->parse()->stripped;
    }

    /**
     * @return string
     */
    public function getDataParsed()
    {
        return $this->parse()->data_parsed;
    }

    /**
     * Magic Method to string
     * @return string
     */
    public function __toString()
    {
        return $this->parse()->data_parsed;
    }
}
