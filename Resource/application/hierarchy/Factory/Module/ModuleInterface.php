<?php
namespace App\Factory\Module;

/**
 * Interface ModuleInterface
 * @package App\Factory
 */
interface ModuleInterface
{
    const SELECTOR    = 'module_selector';
    const NAME        = 'name';
    const VERSION     = 'version';
    const AUTHOR      = 'author';
    const AUTHOR_URI  = 'author_uri';
    const URI         = 'uri';
    const DESCRIPTION = 'description';
    const CLASS_NAME  = 'class_name';
    const FILE_PATH   = 'file_path';

    /**
     * Get Module Selector Name
     *
     * @return string
     */
    public function getModuleNameSelector();

    /**
     * Get Module Info
     *
     * @return array
     */
    public function getModuleInfo();

    /**
     * Get Module Name
     *
     * @return string
     */
    public function getModuleName();

    /**
     * Get Module Version commonly string|integer|float
     *
     * @return string
     */
    public function getModuleVersion();

    /**
     * Get Module Author
     *
     * @return string
     */
    public function getModuleAuthor();

    /**
     * Get Module Author
     *
     * @return string
     */
    public function getModuleAuthorUri();

    /**
     * Get Module URL
     *
     * @return string
     */
    public function getModuleUri();

    /**
     * Get Description of Module
     *
     * @return string
     */
    public function getModuleDescription();

    /**
     * Initialize Module
     *
     * @return mixed
     */
    public function init();
}
