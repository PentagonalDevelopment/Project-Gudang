<?php
namespace App\Libraries;

use App\Factory\Exceptions\InvalidModuleException;
use App\Factory\Exceptions\ModuleNotFoundException;
use App\Factory\Module\ModuleAbstract;
use App\Factory\Module\ModuleParser;
use ArrayAccess;
use Countable;
use Exception;
use InvalidArgumentException;
use RecursiveDirectoryIterator;
use RuntimeException;
use SplFileInfo;

/**
 * Class ModuleCollection
 * @package App\Libraries
 */
class ModuleCollection implements Countable, ArrayAccess
{
    const TYPE_DIR = 'dir';
    const TYPE_SYMLINK = 'link';
    const TYPE_FILE = 'file';

    const CLASS_NAME_KEY = 'className';
    const FILE_PATH_KEY  = 'filePath';

    /**
     * @var RecursiveDirectoryIterator
     */
    protected $splFileInfo;

    /**
     * @var string[]
     */
    protected $unwantedPath = [];

    /**
     * @var ModuleAbstract[]|string[]
     * String if Has not Loaded and Instanceof @uses ModuleAbstract if loaded
     */
    protected $validModule = [];

    /**
     * @var Exception[]
     */
    protected $invalidModule = [];

    /**
     * @var string[]
     */
    protected $loadedModule = [];

    /**
     * @var bool
     */
    protected $hasScanned = false;

    /**
     * @var array
     */
    protected $moduleDefaultInfo = [
        ModuleAbstract::NAME,
        ModuleAbstract::VERSION,
        ModuleAbstract::URI,
        ModuleAbstract::AUTHOR,
        ModuleAbstract::AUTHOR_URI,
        ModuleAbstract::DESCRIPTION,
        ModuleAbstract::CLASS_NAME,
        ModuleAbstract::FILE_PATH
    ];

    /**
     * @var ModuleParser
     */
    protected $moduleParser;

    /**
     * @var ModuleCollection
     */
    protected static $instance;

    /**
     * @var string
     */
    protected $moduleDirectory = MODULE_DIR;

    /**
     * @return ModuleCollection|static
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * ModuleCollection constructor.
     */
    public function __construct()
    {
        if (!isset(self::$instance)) {
            self::$instance =& $this;
        }

        $moduleDirectory = $this->moduleDirectory;
        $this->moduleParser = new ModuleParser();
        if (!is_dir($moduleDirectory) || ! is_readable($moduleDirectory)) {
            throw new RuntimeException(
                sprintf(
                    'Invalid %s Directory. %s directory not exists or has not readable by system!',
                    $this->moduleParser->getName()
                ),
                E_COMPILE_ERROR
            );
        }

        $this->splFileInfo = new \SplFileInfo($moduleDirectory);
        if ($this->splFileInfo->isLink()) {
            throw new RuntimeException(
                sprintf(
                    'Invalid %s Directory. %s directory could not as a symlink!',
                    $this->moduleParser->getName()
                ),
                E_COMPILE_ERROR
            );
        }

        $this->moduleDirectory = $this->splFileInfo->getRealPath();
    }

    /**
     * Scan ModuleAbstract Directory
     *
     * @return ModuleCollection
     */
    public function scan()
    {
        if ($this->hasScanned) {
            return $this;
        }

        /**
         * @var SplFileInfo $path
         */
        foreach (new RecursiveDirectoryIterator($this->getModuleDirectory()) as $path) {
            $baseName = $path->getBaseName();
            // skip dotted
            if ($baseName == '.' || $baseName == '..') {
                continue;
            }

            $directory = $this->getModuleDirectory() . DIRECTORY_SEPARATOR . $baseName;
            // don't allow symlink to be execute & skip if contains file
            if ($path->isLink() || ! $path->isDir()) {
                // ignore git keep
                if ($baseName == '.gitkeep' && $path->getSize() < 2) {
                    continue;
                }
                $this->unwantedPath[$baseName] = $path->getType();
                continue;
            }

            $file = $directory . DIRECTORY_SEPARATOR . $baseName .'.php';
            if (! file_exists($file)) {
                $this->invalidModule[$baseName] = new ModuleNotFoundException(
                    $file,
                    sprintf(
                        '%1$s file for %2$s has not found',
                        $this->moduleParser->getName(),
                        $baseName
                    )
                );

                continue;
            }

            try {
                $module = $this
                    ->moduleParser
                    ->create($file)
                    ->process();
                if (! $module->isValid()) {
                    throw new InvalidModuleException(
                        sprintf(
                            '%1$s Is not valid.',
                            $this->moduleParser->getName()
                        )
                    );
                }

                $this->validModule[$this->sanitizeModuleName($baseName)] = [
                    static::CLASS_NAME_KEY => $module->getClassName(),
                    static::FILE_PATH_KEY => $module->getFile(),
                ];
            } catch (\Exception $e) {
                $this->invalidModule[$this->sanitizeModuleName($baseName)] = $e;
            }
        }

        return $this;
    }

    /**
     * Get Path Module Directory
     *
     * @return string
     */
    public function getModuleDirectory()
    {
        // no scan here
        return $this->moduleDirectory;
    }

    /**
     * @return ModuleAbstract[]
     */
    public function getAllValidModule()
    {
        // doing scan
        $this->scan();
        return $this->validModule;
    }

    /**
     * Get Invalid Module
     *
     * @return Exception[]
     */
    public function getInvalidModule()
    {
        // doing scan
        $this->scan();
        return $this->invalidModule;
    }

    /**
     * Get Unwanted Path
     * contain [file|dir|link]
     *
     * @see ModuleCollection::TYPE_SYMLINK
     * @see ModuleCollection::TYPE_DIR
     * @see ModuleCollection::TYPE_FILE
     *
     * @return string[]
     */
    public function getUnwantedPath()
    {
        // doing scan
        $this->scan();
        return $this->unwantedPath;
    }

    /**
     * @see getUnwantedPath()
     *
     * @return string[]
     */
    public function getUnwantedPaths()
    {
        return $this->getUnwantedPath();
    }

    /**
     * Get Loaded Module List base on Name
     *
     * @return string[]
     */
    public function getLoadedModuleName()
    {
        // doing scan
        $this->scan();
        return $this->loadedModule;
    }

    /**
     * @return ModuleAbstract[]
     */
    public function getLoadedModule()
    {
        $moduleCollections = [];
        foreach ($this->getLoadedModuleName() as $value) {
            if (isset($this->validModule[$value])) {
                $moduleCollections[$value] = $this->validModule[$value];
            }
        }

        return $moduleCollections;
    }

    /**
     * Sanitize Module Name
     *
     * @param string $name
     * @return string
     */
    public function sanitizeModuleName($name)
    {
        return trim(strtolower($name));
    }

    /**
     * Get Module Given By Name
     *
     * @access protected
     *
     * @param string $name
     * @return ModuleAbstract
     * @throws InvalidModuleException
     * @throws Exception
     */
    protected function &internalGetModule($name)
    {
        $moduleName = $this->sanitizeModuleName($name);
        if (!$moduleName) {
            throw new InvalidArgumentException(
                "Please insert not an empty arguments",
                E_USER_ERROR
            );
        }

        if (!$this->exist($moduleName)) {
            throw new InvalidModuleException(
                sprintf(
                    '%1$s %2$s has not found',
                    $this->moduleParser->getName(),
                    $name
                )
            );
        }

        if (is_array($this->validModule[$moduleName])) {
            $className = empty($this->validModule[$moduleName][static::CLASS_NAME_KEY])
                ? null
                : (string) $this->validModule[$moduleName][static::CLASS_NAME_KEY];

            if (! $className
                || ! class_exists($this->validModule[$moduleName][static::CLASS_NAME_KEY])
            ) {
                throw new InvalidModuleException(
                    sprintf(
                        '%1$s %2$s has not found',
                        $this->moduleParser->getName(),
                        $name
                    )
                );
            }

            $module = new $className(
                $moduleName
            );

            $this->validModule[$moduleName] = $module;
        }

        if (! $this->validModule[$moduleName] instanceof ModuleAbstract) {
            unset($this->validModule[$moduleName]);
            $e = new InvalidModuleException(
                sprintf(
                    '%1$s %2$s Is not valid.',
                    $this->moduleParser->getName(),
                    $name
                )
            );

            $this->invalidModule[$moduleName] = $e;
            throw $e;
        }

        return $this->validModule[$moduleName];
    }

    /**
     * Get ModuleAbstract's Info
     *
     * @param string $moduleName
     * @return MetaDataCollection
     */
    public function getModuleInformation($moduleName)
    {
        if (!is_string($moduleName)) {
            throw new InvalidArgumentException(
                sprintf(
                    "Module name must be as a string, %s given.",
                    gettype($moduleName)
                )
            );
        }
        return new MetaDataCollection($this->internalGetModule($moduleName)->getModuleInfo());
    }

    /**
     * Get All ModuleAbstract Info
     *
     * @return MetaDataCollection|MetaDataCollection[]
     */
    public function getAllModuleInfo()
    {
        $moduleInfo = new MetaDataCollection();
        foreach ($this->getAllValidModule() as $moduleName => $module) {
            $moduleInfo[$moduleName] = $this->getModuleInformation($moduleName);
        }

        return $moduleInfo;
    }

    /**
     * Load Module
     *
     * @param string $name
     * @return ModuleAbstract
     * @throws InvalidModuleException
     * @throws ModuleNotFoundException
     */
    public function &load($name)
    {
        $module =& $this->internalGetModule($name);
        if (!$this->hasLoaded($name)) {
            $module->init();
            $this->loadedModule[$this->sanitizeModuleName($name)] = true;
        }

        return $module;
    }

    /**
     * Check if Module Exists
     *
     * @param string $name
     * @return bool
     */
    public function exist($name)
    {
        if (!is_string($name)) {
            return false;
        }
        // doing scan
        $this->scan();
        return isset($this->validModule[$this->sanitizeModuleName($name)]);
    }

    /**
     * Check If Module Has Loaded
     *
     * @param string $name
     * @return bool
     */
    public function hasLoaded($name)
    {
        $moduleName = $this->sanitizeModuleName($name);
        return $moduleName && !empty($this->loadedModule[$moduleName]);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->validModule);
    }

    /**
     * @param string $offset
     * @return ModuleAbstract
     */
    public function offsetGet($offset)
    {
        return $this->load($offset);
    }

    /**
     * @param string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $this->exist($offset);
    }

    /**
     * {@inheritdoc}
     * no affected here
     */
    public function offsetSet($offset, $value)
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        return;
    }

    /**
     * @param string $name
     * @return ModuleAbstract
     */
    public function __get($name)
    {
        return $this->load($name);
    }
}
