<?php
namespace App\Factory\Form;

use App\Factory\AbstractForm;
use App\Libraries\HookAble;
use App\Libraries\Loader;
use App\Model\User;

/**
 * Class Forgot
 * @package App\Factory\Form
 */
class Forgot extends AbstractForm
{
    /**
     * @var array
     */
    protected $collection = [];

    /**
     * Forgot constructor.
     */
    public function __construct()
    {
        /**
         * @var \MY_Security $security
         * @var \CI_Input $input
         * @var \CI_Session $session
         */
        $security = Loader::get('security');
        $this->collection = [
            'user[email]' => [
                'id' => 'user_forgot_email',
                'label' => 'Email Address',
                'type' => 'email',
                'value' => null,
                'required' => 'required',
            ],
            "user[{$security->get_csrf_token_name()}]" => [
                'type' => 'hidden',
                'value' => $security->get_csrf_hash()
            ],
        ];
    }

    public function validate(array $args, \Closure $success, \Closure $error = null)
    {
        $error  = $error ?: function () {
        };
        /**
         * @var \MY_Security $security
         * @var HookAble $hook
         */
        $security = Loader::get(\MY_Security::class);
        $hook = Loader::get(HookAble::class);
        $key     = [
            'email',
            $security->get_csrf_token_name()
        ];
        if (empty($args['user']) || !is_array($args['user'])) {
            $hook
                ->apply(
                    self::HOOK_NAME_AFTER_FORGOT_FAILED,
                    self::VALIDATION_INVALID_DATA,
                    $args,
                    $success,
                    $error
                );
            return $this->processValidation($error, self::VALIDATION_INVALID_DATA);
        }

        /**
         * add ConstantCollection Before Login Check
         */
        $hook->apply(self::HOOK_NAME_BEFORE_CHECK_FORGOT, $args, $this, $success, $error);
        $args = $args['user'];
        foreach ($key as $check) {
            if (! isset($args[$check])
                || ! is_string($args[$check])
                || trim($args[$check]) == ''
            ) {
                if ($security->get_csrf_token_name() == $check) {
                    $check = 'token';
                }
                $returnValue = self::VALIDATION_INVALID_DATA;
                switch ($check) {
                    case 'email':
                        $returnValue = self::VALIDATION_EMPTY_EMAIL;
                        break;
                    case 'token':
                        $returnValue = self::VALIDATION_EMPTY_TOKEN;
                        break;
                }
                $hook
                    ->apply(
                        self::HOOK_NAME_AFTER_FORGOT_FAILED,
                        $returnValue,
                        $args,
                        $success,
                        $error
                    );
                return $this->processValidation($error, $returnValue);
            }
        }

        if (!$security->verifyCSRFToken($args[$security->get_csrf_token_name()])) {
            $hook
                ->apply(
                    self::HOOK_NAME_AFTER_FORGOT_FAILED,
                    self::VALIDATION_USER_INVALID_TOKEN,
                    $args,
                    $success,
                    $error
                );

            return $this->processValidation($error, self::VALIDATION_USER_INVALID_TOKEN);
        }

        /**
         * @var User $user
         */
        $user = User::fromEmail($args['email']);
        if (!$user instanceof User || ! $user->isExists()) {
            $hook
                ->apply(
                    self::HOOK_NAME_AFTER_FORGOT_FAILED,
                    self::VALIDATION_USER_NOT_EXIST,
                    $args,
                    $success,
                    $error
                );
            return $this->processValidation($error, self::VALIDATION_USER_NOT_EXIST);
        }

        $callback = $hook
            ->apply(
                self::HOOK_NAME_AFTER_FORGOT_SUCCESS,
                $success,
                $user,
                $args,
                $error
            );

        return $this->processValidation(
            $callback instanceof \Closure ? $callback : $success,
            $user,
            $args
        );
    }
}
