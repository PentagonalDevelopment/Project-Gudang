<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}

echo "\nERROR: ",
$this->getArgument('error_heading'),
"\n\n",
$this->getArgument('error_message'),
"\n\n";
