<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
$this->setArgument('title', 'Database Error');
$this->headerPart();
?>
<div id="container">
    <h1><?= $this->getArgument('error_heading'); ?></h1>
    <?= $this->getArgument('error_message'); ?>
</div>
<?php
$this->footerPart();
