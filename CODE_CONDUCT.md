# CODE OF CONDUCT

## HIERARCHY

Code Igniter that use multiple libraries & models directory.

When it was using name space it will be and it must be as is :

```php
namespace App\{TYPE}
```

that mean the `{TYPE}` is must as :
 - Model
 - Libraries

The libraries and models stored on : `Resource/application/hierarchy/`


Or if want to create third party class, please consider to create new direectory
under :

`Resource/application/hierarchy/`

and use it as `namespace`

##

Example create new collections of `namespace App\MyNameSpace\`.

It must be was on `Resource/application/hierarchy/MyNameSpace`,
the directory also support `namespace` hierarchy of `App`.

## CREATING MODEL

The application has override model that on `MY_Model` or `App\Model\Model`

## CREATING LIBRARIES

Libraries on hierarchy is must be contain namespace `App\Libraries\` and put on :

`Resource/application/hierarchy/Libraries/ClassNameWithNameSpaceLibraries.php`,

## CREATING MODULE
