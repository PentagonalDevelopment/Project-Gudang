<?php
namespace App\Factory\Form;

use App\Factory\AbstractForm;

/**
 * Class ProfileEdit
 * @package App\Factory\Form
 */
class ProfileEdit extends AbstractForm
{
    public function getFormDefinition()
    {
        return parent::getFormDefinition();
    }

    public function validate(array $args, \Closure $success, \Closure $error = null)
    {
        // TODO: Implement validate() method.
    }
}
