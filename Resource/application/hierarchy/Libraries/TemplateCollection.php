<?php
namespace App\Libraries;

/**
 * Class TemplateCollection
 * @package App\Libraries
 */
class TemplateCollection
{
    /**
     * @var ModuleCollection
     */
    protected static $instance;

    /**
     * @var string
     */
    protected $templateDirectory = TEMPLATE_DIR;

    /**
     * ModuleCollection constructor.
     */
    public function __construct()
    {
        if (!isset(self::$instance)) {
            self::$instance =& $this;
        }
    }

    /**
     * @return ModuleCollection|TemplateCollection|static
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }
}
