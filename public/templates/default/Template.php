<?php
namespace {

    use App\Factory\Form\Login;
    use App\Libraries\Loader;
    use App\Model\Options;

    /**
     * @var MY_Loader $this
     */
    if (!defined('BASEPATH')) {
        exit;
    }

    /**
     * Call in global if views has been called
     * and will called at once - Be careful to load it require,
     * because loader only call as require (not in require_once)
     *
     * @template Init before View
     * @var MY_Loader $this
     */

    /**
     * @param string $keyName
     * @param mixed $default
     * @return mixed
     */
    function get_option($keyName, $default = null)
    {
        /**
         * @var Options $option
         */
        $option = Loader::get(Options::class);
        return $option->get($keyName, $default);
    }

    /**
     * @param string $keyName
     * @param mixed $value
     * @param bool $autoload
     * @return bool
     */
    function update_option($keyName, $value, $autoload = false)
    {
        /**
         * @var Options $option
         */
        $option = Loader::get(Options::class);
        return $option->set($keyName, $value, $autoload);
    }

    /**
     * @param array $array
     * @param string|null $buttonIconType
     * @param string|null $before
     */
    function generateBlockForm(array $array, $buttonIconType = null, $before = null)
    {
        /**
         * @var CI_Session $session
         */
        $session = Loader::get('session');
        $message = $session->flashdata(Login::FLASH_MESSAGE_AUTH);
        $message = !empty($message) && isset($message['type']) && isset($message['message'])
            && is_string($message['type']) && is_string($message['message'])
            ? $message
            : [];
        echo "<div id=\"message-form\">\n";
        if (!empty($message)) {
            ?>
            <div class="alert alert-<?= $message['type'];?> alert-dismissible fade show" style="margin-bottom: 2.4em">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <?= $message['message'];?>
            </div>
            <?php
        }
        echo "</div>\n";

        echo (string) $before;
    ?>

            <form method="POST" action="<?=
            base_url(Loader::get(MY_URI::class)->uri_string()
    );?>" class="form-post-form"><?php
        foreach ($array as $key => $value) :
            $label = isset($value['label'])
                ? $value['label']
                : null;
            unset($value['class'], $value['label']);
            $type = isset($value['type'])
                ? $value['type']
                : 'text';
            $id = isset($value['id'])
                ? htmlspecialchars($value['id'], ENT_QUOTES)
                : null;
            $attribute = " name=\"{$key}\"";
            foreach ($value as $attrKey => $attributeValue) {
                $attributeValue  = is_string($attributeValue)
                    ? htmlspecialchars($attributeValue, ENT_QUOTES)
                    : (is_numeric($attributeValue) ? $attributeValue : (string) $attributeValue);
                $attribute .= " {$attrKey}=\"{$attributeValue}\"";
            }
            if ($type == 'hidden') :?>

                <input class="hidden" <?= $attribute;?>><?php
            elseif ($type == 'checkbox') : ?>

                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Accept the license agreement</span>
                    </label>
                </div>
                <!-- .form-group --><?php
            else :
                // label for {id}
                $idLabel =  $id ? " for=\"{$id}\"" : "";
            ?>

                <div class="form-group form-group--float form-group--centered">
                    <input class="form-control"<?= $attribute; ?>>
                    <?= $label ? "<label class=\"form-control-label\" {$idLabel}>{$label}</label>\n" : ""; ?>
                    <i class="form-group__bar"></i>
                </div>
                <!-- .form-group --><?php
            endif;
        endforeach; ?>

                <button type="SUBMIT" class="btn btn--icon login__block__btn"><i class="zmdi <?=
                    $buttonIconType?: 'zmdi-check';
                ?>"></i></button>
            </form>
            <!-- .form-post-form -->
        <?php
    }
}