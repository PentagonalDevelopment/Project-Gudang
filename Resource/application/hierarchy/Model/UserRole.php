<?php
namespace App\Model;

use App\Libraries\Loader;
use App\Libraries\MetaDataCollection;
use App\Model\Base\ModelDbAbstract;

/**
 * Class UserRole
 * @package App\Model
 */
class UserRole extends ModelDbAbstract
{
    /* --------------------------------------------------
     * DEFINITION
     * ----------------------------------------------- */

    const TABLE_NAME = 'users_role';

    const COLUMN_ID             = 'role_id';
    const COLUMN_NAME           = 'role_name';
    const COLUMN_IDENTITY       = 'role_identity';
    const COLUMN_ACCESS         = 'role_access';
    const COLUMN_DESCRIPTION    = 'role_description';

    // identifier
    const COLUMN_PREFIX         = 'role_';

    const DUPLICATE_NAME = E_USER_ERROR;
    const DUPLICATE_IDENTITY = E_ERROR;
    const FAIL_SAVED = E_COMPILE_ERROR;

    /**
     * Base On Table Name
     *
     * @var string
     */
    protected $databaseTableName = self::TABLE_NAME;

    /**
     * @var array
     */
    protected $collectionData = [];

    /**
     * @var array
     */
    protected $dataHasSet = [];

    /**
     * @var array
     */
    protected $collectionDataOriginal = null;

    /**
     * @var boolean
     */
    protected $isResultFromDatabase = false;

    /**
     * @var bool
     */
    protected $hasChange = false;

    /**
     * @var bool
     */
    protected $isExists = false;

    /**
     * @var array
     */
    protected $dataHasNew = [];

    /**
     * @var array
     */
    protected $validIdentifierChangeAble = [
        self::COLUMN_NAME,
        self::COLUMN_IDENTITY,
        self::COLUMN_ACCESS,
        self::COLUMN_DESCRIPTION
    ];

    /**
     * Column selector that must be exists
     *
     * @var array
     */
    protected $mustBeExist = [
        self::COLUMN_NAME,
        self::COLUMN_IDENTITY,
    ];

    /**
     * {@inheritdoc}
     */
    protected function initBeforeLoad()
    {
        parent::initBeforeLoad();
        // load init
        $this->load->database();
        // sanity
        $this->databaseTableName = $this->db->dbprefix(self::TABLE_NAME);
    }

    /**
     * @return UserRole
     */
    protected function internalReset()
    {
        $this->collectionData = [
            self::COLUMN_ID => null,
            self::COLUMN_NAME => null,
            self::COLUMN_IDENTITY => null,
            self::COLUMN_ACCESS => null,
            self::COLUMN_DESCRIPTION => null
        ];
        $this->dataHasSet = [];
        $this->collectionDataOriginal = null;
        $this->isResultFromDatabase = false;
        $this->hasChange = false;
        $this->dataHasNew = [];
        $this->isExists = false;
        return $this;
    }

    /**
     * @param boolean $result
     * @return UserRole
     */
    private function internalSetFromDatabaseResult($result)
    {
        $this->isResultFromDatabase = (boolean)$result;
        return $this;
    }

    /**
     * @param boolean $bool
     * @return UserRole
     */
    private function internalSetExist($bool)
    {
        $this->isExists = (bool) $bool;
        return $this;
    }

    /**
     * @return bool
     */
    public function isResultFromDatabase()
    {
        return $this->isResultFromDatabase;
    }

    /**
     * @return bool
     */
    public function isExists()
    {
        return $this->isResultFromDatabase() && $this->isExists;
    }

    /**
     * @param array $args
     * @return UserRole
     */
    public function newInstance(array $args = [])
    {
        $object = clone $this;
        $object->internalReset();
        foreach ($args as $key => $value) {
            if (array_key_exists($key, $this->collectionData)) {
                $object->dataHasNew[$key] = true;
                $object->collectionData[$key] = $value;
            }
        }

        return $object;
    }

    /**
     * @param User $user
     * @return UserRole
     */
    public static function fromUser(User $user)
    {
        /**
         * @var UserRole $userRole
         */
        $userRole = Loader::get(UserRole::class);

        return $userRole->getFromUser($user);
    }

    /**
     * @return UserRole
     */
    protected function sanityResult()
    {
        foreach ($this->collectionData as $key => $value) {
            $this->collectionData[$key] = $this->unSerial($value);
        }

        return $this;
    }

    /**
     * @param User $user
     * @return UserRole
     */
    public function getFromUser(User $user)
    {
        if ($user->isExists()) {
            $id = $user->get($user::COLUMN_ID);
            $get = $this
                ->db
                ->select()
                ->where(self::COLUMN_ID, $id)
                ->limit(1)
                ->get();
            $result = $get->result_array();
            $get->free_result();
            if (!empty($result[0])) {
                return $this
                    ->newInstance($result[0])
                    ->internalSetFromDatabaseResult(true)
                    ->sanityResult()
                    ->internalSetExist(true);
            }
        }

        return $this->newInstance();
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->databaseTableName;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return UserRole
     * @throws \Exception
     */
    public function set($key, $value)
    {
        if (!isset($this->collectionDataOriginal) && $this->isResultFromDatabase()) {
            $this->collectionDataOriginal = $this->collectionData;
        }

        if (!is_string($key)) {
            throw new \Exception('Invalid KeyName for database');
        }

        if (in_array($key, $this->validIdentifierChangeAble)) {
            if ($key == self::COLUMN_IDENTITY || $key == self::COLUMN_ID) {
                if (!is_numeric($value) || is_int(abs($value))) {
                    throw new \Exception(
                        sprintf(
                            'Value for %s must be as a numeric integer!',
                            $key
                        ),
                        E_USER_ERROR
                    );
                }
            } elseif ($key != self::COLUMN_ACCESS && !is_string($value)) {
                throw new \Exception(
                    'Value must be as a string!',
                    E_USER_ERROR
                );
            }

            $this->dataHasSet[$key] = true;
            $this->collectionData[$key] = $value;
        }

        return $this;
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        if (!is_string($key)) {
            return $default;
        }

        return array_key_exists($key, $this->collectionData)
            ? $this->collectionData[$key]
            : $default;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        return isset($this->collectionData[$key]);
    }

    public function offsetSet($offset, $value)
    {
        // none
    }

    /**
     * @param string $offset
     */
    public function remove($offset)
    {
        if (!empty($this->collectionDataOriginal)) {
            unset($this->collectionData[$offset], $this->dataHasSet[$offset]);
        }
    }

    /**
     * @param string $by
     * @param mixed $value
     * @return bool
     */
    private function updateData($by, $value)
    {

        if (empty($this->dataHasSet) && ! $this->isResultFromDatabase()) {
            $dataNew = $this->collectionData;
            $this->dataHasSet = array_intersect_key($this->dataHasNew, $dataNew);
        }

        $data = [];
        foreach ($this->collectionData as $key => $v) {
            if (in_array($key, $this->validIdentifierChangeAble)
                && isset($this->dataHasSet[$key])
            ) {
                $data[$key] = $this->serial($v);
            }
        }

        return $this->db->update(
            $this->getTableName(),
            $data,
            [$by => $value]
        );
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        if ($this->hasChange) {
            if ($this->isResultFromDatabase()) {
                return $this->updateData(
                    self::COLUMN_ID,
                    $this->collectionDataOriginal[self::COLUMN_ID]
                );
            }
        }
        if (isset($this->collectionData[self::COLUMN_IDENTITY]) && $this->collectionData[self::COLUMN_NAME]) {
            $get = $this->db
                ->select()
                ->from($this->getTableName())
                ->where(
                    $this->createLowerColumn(self::COLUMN_NAME),
                    strtolower($this->collectionData[self::COLUMN_NAME])
                )
                ->or_where(self::COLUMN_IDENTITY, $this->collectionData[self::COLUMN_IDENTITY])
                ->get();
            $result = $get->result_array();
            $get->free_result();
            $count = count($result);
            if ($count > 1) {
                foreach ($result as $value) {
                    if (strtolower($value[self::COLUMN_NAME]) == strtolower($this->collectionData[self::COLUMN_NAME])
                        && $value[self::COLUMN_IDENTITY] != $this->collectionData[self::COLUMN_IDENTITY]
                    ) {
                        throw new \Exception(
                            'There was duplicate of data! identity has duplicate!',
                            self::DUPLICATE_NAME
                        );
                    }

                    if ($value[self::COLUMN_IDENTITY] != $this->collectionData[self::COLUMN_IDENTITY]
                        && $value[self::COLUMN_NAME] == $this->collectionData[self::COLUMN_NAME]
                    ) {
                        throw new \Exception(
                            'There was duplicate of data! identity has exists to another id!',
                            self::DUPLICATE_IDENTITY
                        );
                    }
                }

                throw new \Exception(
                    'There was duplicate of data! identity or name!'
                );
            }
            if ($count === 1) {
                return $this->updateData(self::COLUMN_ID, $result[0][self::COLUMN_ID]);
            }

            $values = [];
            foreach ($this->collectionData as $key => $value) {
                if ($key == self::COLUMN_NAME) {
                    $key = strtolower($value);
                }
                $values[$key] = $this->serial($value);
            }

            return $this->db->insert(
                $this->getTableName(),
                $values
            );
        }

        throw new \Exception(
            'Can not save data! Data user must be contains identity & role name',
            self::FAIL_SAVED
        );
    }

    /**
     * @return MetaDataCollection
     */
    public function all()
    {
        return Loader::get(MetaDataCollection::class)->newInstance($this->collectionData);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->all()->toArray();
    }
}
