<?php
namespace App\Factory\Form;

use App\Factory\AbstractForm;
use App\Libraries\Loader;
use App\Model\Options;

/**
 * Class Login
 * @package App\Factory\Form
 */
class Register extends AbstractForm
{
    const ACCEPT_AGREEMENT_KEY = 'accept_license_agreement_text_login';

    protected $collection = [];

    /**
     * Register constructor.
     */
    public function __construct()
    {
        /**
         * @var \MY_Security $security
         * @var \CI_Input $input
         * @var \CI_Session $session
         */
        $security = Loader::get('security');
        $input  = Loader::get('input');
        $session = Loader::get('session');
        $options = Loader::get(Options::class);
        if ($input->method(true) == 'POST') {
            $session->set_flashdata('post:user[email]', $input->post('user[email]'));
            $session->set_flashdata('post:user[name]', $input->post('user[name]'));
        }

        $this->collection =  [
            'user[name]'     => [
                'id' => 'user_register_name',
                'minlength' => 4,
                'label' => 'Your Name',
                'type' => 'text',
                'value' => $session->flashdata('post:user[name]'),
                'required' => 'required',
            ],
            'user[email]'    => [
                'id' => 'user_register_email',
                'label' => 'Email Address',
                'type' => 'email',
                'value' => $session->flashdata('post:user[email]'),
                'required' => 'required',
            ],
            'user[password]' => [
                'id' => 'user_register_password',
                'label' => 'Password',
                'minlength' => 6,
                'type' => 'password',
                'value' => null,
                'required' => 'required',
            ],
            'user[password_verify]' => [
                'id' => 'user_register_password_verify',
                'data-match-verify' => '#user_register_password',
                'data-match-text' =>  'Password verify must be match!',
                'label' => 'Verify Password',
                'minlength' => 6,
                'type' => 'password',
                'value' => null,
                'required' => 'required',
            ],
            'user[accept]'   => [
                'id'    => 'user_register_accept',
                'label' => $options->get(static::ACCEPT_AGREEMENT_KEY, 'Accept the license agreement'),
                'type'  => 'checkbox',
                'value' => 'yes',
                'checked' => null
            ],
            "user[{$security->get_csrf_token_name()}]" => [
                'type' => 'hidden',
                'value' => $security->get_csrf_hash()
            ],
        ];
    }

    public function validate(array $args, \Closure $success, \Closure $error = null)
    {
        $error  = $error ?: function () {
        };
        return $this->processValidation($success);
    }
}
