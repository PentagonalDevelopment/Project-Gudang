<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
?>
    </div>
    <!-- #page -->
<!-- assets core js -->
<script src="<?= base_url('/assets/js/domainCheck.js'); ?>"></script>
<!-- Vendors -->
<script src="<?= $this->getTemplateDirectoryUrl(); ?>/assets/vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= $this->getTemplateDirectoryUrl(); ?>/assets/vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?= $this->getTemplateDirectoryUrl(); ?>/assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= $this->getTemplateDirectoryUrl(); ?>/assets/vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?= $this->getTemplateDirectoryUrl(); ?>/assets/js/app.min.js"></script>
<script src="<?= $this->getTemplateDirectoryUrl(); ?>/assets/js/admin.js"></script>

<?php $this->getFooterHook(); ?>
</body>
</html>