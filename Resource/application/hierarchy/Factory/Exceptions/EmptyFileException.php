<?php
namespace App\Factory\Exceptions;

/**
 * Class EmptyFileException
 * @package App\Factory\Exceptions
 */
class EmptyFileException extends InvalidPathException
{
}
