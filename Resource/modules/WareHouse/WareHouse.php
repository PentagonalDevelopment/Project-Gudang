<?php
namespace {

    use App\Factory\Module\ModuleAbstract;

    /**
     * Class WareHouse
     */
    class WareHouse extends ModuleAbstract
    {
        /**
         * @var string
         */
        protected $module_name = 'Ware House Module';
        /**
         * @var string
         */
        protected $module_description = 'Ware House Module that implements to warehouse functionality';

        /**
         * @var string
         */
        protected $module_uri = 'https://www.pentagonal.org/';

        /**
         * @var string
         */
        protected $module_author = 'pentagonal';
        /**
         * @var string
         */
        protected $module_author_uri = 'https://www.pentagonal.org/';
        /**
         * {@inheritdoc}
         */
        public function init()
        {
        }
    }
}