<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
$this->setArgument('title', '403 Forbidden Access');
$this->headerPart();
?>
    <section class="error">
        <div class="error__inner">
            <h1>403</h1>
            <h2>You have not enough permission to access this page!</h2>
            <?= $this->getArgument('error_message'); ?>
        </div>
    </section>
<?php
$this->footerPart();