<?php
namespace {

    use App\Libraries\HookAble;
    use App\Libraries\Loader;

    /**
     * Class MY_Exceptions
     */
    class MY_Exceptions extends CI_Exceptions
    {
        /**
         * @param Exception $exception
         */
        public function show_exception($exception)
        {
            $message = $exception instanceof Exception
                ? $exception->getMessage()
                : null;
            if (!$exception instanceof Exception && class_exists('Error')) {
                $message = $exception instanceof Error
                    ? $exception->getMessage()
                    : null;
            }

            if (empty($message)) {
                $message = '(null)';
            }

            if (($result = $this
                    ->loadWithCaseBufferParams(
                        'error_exception',
                        [
                            'error_exception' => $exception instanceof Exception
                                ? $exception
                                : new Exception($message),
                            'error_message' => $message,
                        ]
                    )
                ) !== false) {
                echo $result;
                return;
            }
            $templates_path = config_item('error_views_path');
            $viewPath = VIEWPATH . 'errors' . DIRECTORY_SEPARATOR;
            if (empty($templates_path)) {
                $templates_path = $viewPath;
            }

            if (is_cli()) {
                $templates_path .= 'cli' . DIRECTORY_SEPARATOR;
            } else {
                $templates_path .= 'html' . DIRECTORY_SEPARATOR;
            }

            (ob_get_level() > $this->ob_level + 1) && ob_end_flush();

            $require = file_exists($templates_path . 'error_exception.php')
                ? $templates_path . 'error_exception.php'
                : (
                file_exists($viewPath . 'error_exception.php')
                    ? $viewPath . 'error_exception.php'
                    : null
                );

            unset($viewPath);
            ob_start();
            if ($require) {
                /** @noinspection PhpIncludeInspection */
                include($require);
            }

            $buffer = ob_get_contents();
            ob_end_clean();
            echo $buffer;
        }

        /**
         * Native PHP error handler
         *
         * @param int $severity
         * @param string $message
         * @param string $filepath
         * @param int $line
         * @return void
         */
        public function show_php_error($severity, $message, $filepath, $line)
        {
            $severity = isset($this->levels[$severity]) ? $this->levels[$severity] : $severity;
            $templates_path = config_item('error_views_path');
            $viewPath = VIEWPATH . 'errors' . DIRECTORY_SEPARATOR;

            // For safety reasons we don't show the full file path in non-CLI requests
            if (!is_cli()) {
                $filepath = str_replace('\\', '/', $filepath);
                if (false !== strpos($filepath, '/')) {
                    $x = explode('/', $filepath);
                    $filepath = $x[count($x) - 2] . '/' . end($x);
                }
                $template = 'html' . DIRECTORY_SEPARATOR . 'error_php';
            } else {
                $template = 'cli' . DIRECTORY_SEPARATOR . 'error_php';
            }

            if (($result = $this
                    ->loadWithCaseBufferParams(
                        'error_php',
                        [
                            'error_severity' => $severity,
                            'error_message' => $message,
                            'error_line' => $line,
                            'error_filepath' => $filepath
                        ]
                    )
                ) !== false) {
                echo $result;
                return;
            }

            if (empty($templates_path)) {
                $templates_path = $viewPath;
            }

            (ob_get_level() > $this->ob_level + 1) && ob_end_flush();

            $require = file_exists($templates_path . $template . '.php')
                ? $templates_path . $template . '.php'
                : (
                file_exists($viewPath . $template . '.php')
                    ? $viewPath . $template . '.php'
                    : null
                );

            unset($viewPath);
            ob_start();
            if ($require) {
                /** @noinspection PhpIncludeInspection */
                include($require);
            }
            $buffer = ob_get_contents();
            ob_end_clean();
            echo $buffer;
        }

        /**
         * General Error Page
         *
         * Takes an error message as input (either as a string or an array)
         * and displays it using the specified template.
         *
         * @param    string $heading Page heading
         * @param    string|string[] $message Error message
         * @param    string $template Template name
         * @param    int $status_code (default: 500)
         *
         * @return    string    Error page output
         */
        public function show_error($heading, $message, $template = 'error_general', $status_code = 500)
        {
            $message = is_cli()
                ? "\t" . (is_array($message) ? implode("\n\t", $message) : $message)
                : '<p>' . (is_array($message) ? implode('</p><p>', $message) : $message) . '</p>';

            if (($result = $this
                    ->loadWithCaseBufferParams(
                        $template,
                        [
                            'error_heading' => $heading,
                            'error_message' => $message,
                            'error_status_code' => $status_code
                        ]
                    )
                ) !== false) {
                return $result;
            }

            unset($result);
            $templates_path = config_item('error_views_path');
            $viewPath = VIEWPATH . 'errors' . DIRECTORY_SEPARATOR;
            if (empty($templates_path)) {
                $templates_path = $viewPath;
            }

            $template = is_cli()
                ? 'cli' . DIRECTORY_SEPARATOR . $template
                : 'html' . DIRECTORY_SEPARATOR . $template;

            (ob_get_level() > $this->ob_level + 1) && ob_end_flush();

            ob_start();
            $require = file_exists($templates_path . $template . '.php')
                ? $templates_path . $template . '.php'
                : (file_exists($viewPath . $template . '.php')
                    ? $viewPath . $template . '.php'
                    : null
                );

            unset($viewPath);
            if ($require) {
                /** @noinspection PhpIncludeInspection */
                include($require);
            }

            $buffer = ob_get_contents();
            ob_end_clean();
            return $buffer;
        }

        /**
         * 404 Error Handler
         *
         * @uses    CI_Exceptions::show_error()
         *
         * @param    string $page Page URI
         * @param    bool $log_error Whether to log the error
         * @return    void
         */
        public function show_404($page = '', $log_error = true)
        {
            $heading = is_cli()
                ? 'Not Found'
                : '404 Page Not Found';
            $message = is_cli()
                ? 'The controller/method pair you requested was not found.'
                : 'The page you requested was not found.';

            // By default we log this, but allow a dev to skip it
            if ($log_error) {
                log_message('error', $heading . ': ' . $page);
            }
            $hook = Loader::get(HookAble::class);
            $hook->add(
                MY_Loader::HOOK_BODY_CLASS,
                function ($body) {
                    $body[] = 'page-404';
                    return $body;
                }
            );
            echo $this->show_error($heading, $message, 'error_404', 404);
            exit(4); // EXIT_UNKNOWN_FILE
        }

        private function tryToLoadFromTemplateWithBufferParams($template, array $args = [])
        {
            $template = substr($template, -4) != '.php'
                ? $template . '.php'
                : $template;
            if (function_exists('get_instance')) {
                $instance =& get_instance();
                if (isset($instance->load) && $instance->load instanceof MY_Loader) {
                    foreach ($instance->load->getViewPaths() as $key => $v) {
                        if (file_exists($key . 'Error/' . $template)) {
                            if (ob_get_level() > $this->ob_level + 1) {
                                ob_end_flush();
                            }

                            ob_start();
                            /**
                             * @var MY_Controller $instance
                             */
                            $instance->load->replaceArgument($args);
                            $instance->load->includeRequire($key . 'Error/' . $template);
                            $buffer = ob_get_contents();
                            ob_end_clean();
                            return $buffer;
                        }
                    }
                }
            }

            return false;
        }

        /**
         * @param string $template
         * @param array $args
         * @return bool|string
         */
        private function loadWithCaseBufferParams($template, array $args = [])
        {
            $currentTemplate = $template;
            switch ($template) {
                case 'error_404':
                    $currentTemplate = '404';
                    break;
                case 'error_general':
                    $currentTemplate = '500';
                    break;
                case 'error_db':
                    $currentTemplate = 'Db';
                    break;
                case 'error_exception':
                    $currentTemplate = 'Exception';
                    break;
                case 'error_php':
                    $currentTemplate = 'Php';
                    break;
            }

            $result = $this->tryToLoadFromTemplateWithBufferParams(
                $currentTemplate . (is_cli() ? '-cli' : ''),
                $args
            );

            return $result;
        }
    }
}
