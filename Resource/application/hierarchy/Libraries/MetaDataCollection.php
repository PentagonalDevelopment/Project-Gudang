<?php
namespace App\Libraries;

/**
 * Class MetaDataCollection
 * @package App\Libraries
 */
class MetaDataCollection implements \ArrayAccess, \IteratorAggregate, \Serializable
{
    /**
     * @var array
     */
    protected $collections = [];

    /**
     * MetaDataCollection constructor.
     * @param array $array
     */
    public function __construct($array = [])
    {
        if (is_array($array)) {
            $this->collections = $array;
        }
    }

    /**
     * @param array $array
     * @return MetaDataCollection
     */
    public function newInstance($array = [])
    {
        return new static($array);
    }

    /**
     * @param array $array
     */
    public function replace(array $array)
    {
        foreach ($array as $key => $value) {
            $this->set($key, $value);
        }
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->collections);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset)
            ? $this->collections[$offset]
            : null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->collections[$offset] = $value;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unserialize($this->collections[$offset]);
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function has($offset)
    {
        return $this->offsetExists($offset);
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function set($offset, $value)
    {
        $this[$offset] = $value;
    }

    /**
     * @param mixed $offset
     * @param mixed $default
     * @return mixed
     */
    public function get($offset, $default = null)
    {
        return $this->offsetExists($offset)
            ? $this[$offset]
            : $default;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->collections;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->all());
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize($this->collections);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = @unserialize($serialized);
        if (is_array($data)) {
            $this->replace($data);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->all();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->serialize();
    }
    /**
     * Fetch from array
     *
     * Internal method used to retrieve values from global arrays.
     *
     * @param   mixed   $index   Index for item to be fetched from $array
     * @param   mixed   $default Default return if not exist
     * @return  mixed
     * @throws \ErrorException
     */
    public function fetchFromArray($index = null, $default = null)
    {
        $array = $this->collections;
        if (empty($array)) {
            return $default;
        }
        if (!is_array($array)) {
            throw new \ErrorException(
                'Invalid records for array collections',
                E_ERROR
            );
        }

        // If $index is NULL, it means that the whole $array is requested
        isset($index) || $index = array_keys($array);
        // allow fetching multiple keys at once
        if (is_array($index)) {
            $output = [];
            foreach ($index as $key) {
                $output[$key] = $this->fetchFromArray($key);
            }
            return $output;
        }
        if (isset($array[$index])) {
            $value = $array[$index];
        } elseif (($count = preg_match_all('/(?:^[^\[]+)|\[[^]]*\]/', $index, $matches)) > 1) {
            // Does the index contain array notation
            $value = $array;
            for ($i = 0; $i < $count; $i++) {
                $key = trim($matches[0][$i], '[]');
                // Empty notation will return the value as array
                if ($key === '') {
                    break;
                }
                if (isset($value[$key])) {
                    $value = $value[$key];
                } else {
                    return $default;
                }
            }
        } else {
            return $default;
        }

        return $value;
    }
}
