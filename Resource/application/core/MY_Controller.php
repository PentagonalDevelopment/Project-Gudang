<?php
namespace {

    use App\Libraries\Arguments;
    use App\Libraries\HookAble;
    use App\Libraries\Loader;
    use App\Libraries\MetaDataCollection;
    use App\Libraries\ModuleCollection;
    use App\Libraries\Pass;
    use App\Libraries\StringSanity;
    use App\Model\CurrentUser;
    use App\Model\Options;
    use App\Model\User;

    /**
     * Class MY_Controller
     *
     * @property CI_DB_query_builder $db
     * @property CI_Session $session

     * @property CI_Input $input
     * @property CI_Output $output
     *
     * @property MY_Security $security
     * @property MY_URI $uri
     * @property MY_Loader $load
     *
     * @property Pass $l_pass
     * @property Arguments $l_arguments
     * @property MetaDataCollection $l_metaDataCollection
     * @property StringSanity $l_stringSanity
     * @property HookAble $l_hookAble
     * @property User $m_user
     * @property Options $m_options
     * @property CurrentUser $m_currentUser
     */
    class MY_Controller extends CI_Controller implements \ArrayAccess
    {
        const LOGIN_PATH = 'accounts/login';
        const FORGOT_PATH = 'accounts/forgot';
        const LOGOUT_PATH = 'accounts/logout';
        const REGISTER_PATH = 'accounts/register';
        const VERIFY_PATH   = 'accounts/verify';
        const URI_KEY_REDIRECT = 'redirect:uri[login]';

        public $__login_route_path    = self::LOGIN_PATH;
        public $__forgot_route_path   = self::FORGOT_PATH;
        public $__logout_route_path   = self::LOGOUT_PATH;
        public $__register_route_path = self::REGISTER_PATH;
        public $__verify_route_path  = self::VERIFY_PATH;

        public function __construct()
        {
            parent::__construct();
            // load initial
            $this->load->database()
                ->helper(['date', 'url'])
                ->library(['session']);
            // load module
            $this->_initLoadModules();
            // load templates
            $this->_initGenerateView();
            // init before another controller
            $this->_initBeforeLoad();
        }

        /**
         * Initialize Module
         */
        final private function _initLoadModules()
        {
            /**
             * @var ModuleCollection $moduleCollection
             * @var Options $options
             */
            $moduleCollection = $this[ModuleCollection::class];
            $options = $this[Options::class];
            $moduleList = $options->get('modules_active', []);
            // next
        }

        /**
         * Initialize View
         */
        final private function _initGenerateView()
        {
            $oldTemplateName = $this[Options::class]->getFull('template_active', ['options_value' => null]);
            $templateName = is_string($oldTemplateName['options_value'])
                ? trim($oldTemplateName['options_value'], '\\/')
                : $oldTemplateName['options_value'];
            $hasNoTemplate = false;
            if (!is_string($templateName) || !is_dir(VIEWPATH . $templateName)) {
                $templateName = 'default';
                $hasNoTemplate = true;
            }

            if ($hasNoTemplate || $templateName !== $oldTemplateName['options_value'] || $oldTemplateName['auto_load'] != 'yes') {
                $this[Options::class]->set('template_active', $templateName, true);
            }

            $this->load->setViewPath(VIEWPATH . DIRECTORY_SEPARATOR . $templateName . DIRECTORY_SEPARATOR);
            $this[HookAble::class]->add(
                MY_Loader::HOOK_BODY_CLASS,
                function ($body) {
                    $is_logged = $this->_isLogin();
                    $body[] = $is_logged ? 'user-logged' : 'user-guest';
                    if ($is_logged) {
                        /**
                         * @var CurrentUser $currentUser
                         */
                        $currentUser = Loader::get(CurrentUser::class);
                        $body[] = 'user-id-' . $currentUser->getUserData()->get('id');
                        $isMulti = $currentUser->isLoggedLogin();
                        $body[] = $isMulti ? 'logged-multi' : 'logged-main';
                        if ($isMulti) {
                            $body[] = 'user-multi-id' . $currentUser->getLoggedUserData()->get('id');
                        }
                    }

                    return $body;
                }
            );
        }

        /**
         * @param mixed $string
         * @param mixed $value
         * @return MY_Controller
         */
        final public function _setArgument($string, $value)
        {
            $this[Arguments::class]->set($string, $value);
            return $this;
        }

        /**
         * @param mixed $string
         * @param mixed $default
         * @return mixed
         */
        final public function _getArgument($string, $default = null)
        {
            return $this[Arguments::class]->get($string, $default);
        }

        final public function _replaceArgument(array $values)
        {
            $this[Arguments::class]->replace($values);
            return $this;
        }

        /**
         * @return bool
         */
        final public function _isLogin()
        {
            return $this[CurrentUser::class]->isLogin();
        }

        /**
         * Check Redirect
         */
        public function _loginCheckRedirect()
        {
            if (!$this->_isLogin()) {
                $flash_redirect = $this->session->flashdata(static::URI_KEY_REDIRECT);
                $uri = $this->uri->uri_string();
                $flash_redirect = $flash_redirect ?: $uri;
                $inArray = in_array(strtolower($uri), [
                    $this->__logout_route_path,
                    $this->__login_route_path,
                    $this->__forgot_route_path,
                    $this->__register_route_path,
                    $this->__verify_route_path,
                ]);

                if (!$inArray) {
                    ($uri != 'favicon.ico') &&
                        $this->session->set_flashdata(static::URI_KEY_REDIRECT, $uri);
                    redirect(base_url($this->__login_route_path));
                } else {
                    $this->session->set_flashdata(static::URI_KEY_REDIRECT, $flash_redirect);
                }
            }
        }

        /**
         * Base Routes
         */
        public function index()
        {
            $this->_loginCheckRedirect();
            $this->_show404();
        }

        /**
         * Show 404 Not Found Exit!
         */
        public function _show404()
        {
            show_404();
        }

        public function _initBeforeLoad()
        {
        }

        /**
         * {@inheritdoc}
         */
        public function offsetExists($offset)
        {
            if (!isset($this->{$offset})) {
                return is_string($this->load->getNamePackageLibraryFor($offset));
            }

            return true;
        }

        public function offsetGet($offset)
        {
            if (!isset($this->{$offset}) && ($packageName = $this->load->getNamePackageLibraryFor($offset))) {
                return $this->load->getPackageFrom($offset);
            }

            return $this->{$offset};
        }

        public function offsetSet($offset, $value)
        {
            if (!isset($this->$offset)) {
                $offset = $this->load->getNamePackageLibraryFor($offset) ?: $offset;
            }

            $this->{$offset} = $value;
        }

        public function offsetUnset($offset)
        {
            if (!isset($this->$offset)) {
                $offset = $this->load->getNamePackageLibraryFor($offset) ?: $offset;
            }

            unset($this->{$offset});
        }
    }
}
