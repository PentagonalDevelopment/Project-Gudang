<?php
namespace App\Factory\Exceptions;

/**
 * Class InvalidModuleException
 * @package App\Factory\Exceptions
 */
class InvalidModuleException extends \Exception
{
}
