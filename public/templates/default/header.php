<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
?><!DOCTYPE html>
<html lang="<?= (string) $this->getArgument('language', 'en');?>" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=(string) $this->getArgument('title', 'Member Area');?></title>
    <script type="text/javascript">
        window.document.documentElement.className = window.document.documentElement.className.replace(/no-(js)/, '$1');
    </script>
    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?= $this->getTemplateDirectoryUrl('assets/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css');?>">
    <link rel="stylesheet" href="<?= $this->getTemplateDirectoryUrl('assets/vendors/bower_components/animate.css/animate.min.css');?>">
    <link rel="stylesheet" href="<?= $this->getTemplateDirectoryUrl('assets/css/app.min.css');?>" media="all">
    <link rel="stylesheet" href="<?= $this->getTemplateDirectoryUrl('assets/css/admin.css');?>" media="all">
<?php $this->getHeaderHook(); ?>
</head>
<body data-ma-theme="green"<?= $this->getBodyClass(true);?>>
    <div class="wrapper" id="page">
