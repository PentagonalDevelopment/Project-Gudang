# GUDANG
Aplikasi Gudang


Start Time ```July, 19th 2017```


## SPEC.

### Framework

Code Igniter 3.1.5


### REQUIREMENTS

- DATABASE : `MySQL 5.5 / MariaDB 10` >=
- PHP `5.6 or later`
- Web Server `Nginx / Apache / LigHttpd`


### CONTRIBUTORS

see [CONTRIBUTORS.md](CONTRIBUTORS.md)

### OK !
