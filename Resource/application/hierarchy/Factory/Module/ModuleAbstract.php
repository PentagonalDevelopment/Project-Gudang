<?php
namespace App\Factory\Module;

use ReflectionClass;

/**
 * Class ModuleAbstract
 * @package App\Factory
 */
abstract class ModuleAbstract implements ModuleInterface
{
    /**
     * @var string
     */
    private $module_name_selector;

    /**
     * Module Name
     *
     * @var string
     */
    protected $module_name = '';

    /**
     * Module URL
     *
     * @var string
     */
    protected $module_uri = '';

    /**
     * Module Version
     *
     * @var mixed
     */
    protected $module_version = '';

    /**
     * Module Author Name
     *
     * @var string
     */
    protected $module_author = '';

    /**
     * Module Author URL
     *
     * @var string
     */
    protected $module_author_uri = '';

    /**
     * Module Description
     *
     * @var string
     */
    protected $module_description = '';

    /**
     * @var ReflectionClass
     */
    private $privateModuleReflectionClass;

    /**
     * Module constructor.
     * @param string $moduleNameSelector
     * @final as prevent to inheritance
     */
    final public function __construct($moduleNameSelector)
    {
        if (!is_string($moduleNameSelector)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Argument must be as a string, %s given.",
                    gettype($moduleNameSelector)
                )
            );
        }
        $this->module_name_selector = $moduleNameSelector;
        $this->getModuleName();
    }

    /**
     * Get Selector Of Module
     *
     * @return string
     */
    final public function getModuleNameSelector()
    {
        return $this->module_name_selector;
    }

    /**
     * Get Module Info
     *
     * @return array
     */
    public function getModuleInfo()
    {
        return [
            static::NAME        => $this->getModuleName(),
            static::VERSION     => $this->getModuleVersion(),
            static::URI         => $this->getModuleUri(),
            static::AUTHOR      => $this->getModuleAuthor(),
            static::AUTHOR_URI  => $this->getModuleAuthorUri(),
            static::DESCRIPTION => $this->getModuleDescription(),
            static::CLASS_NAME  => get_class($this),
            static::FILE_PATH   => $this->getModuleRealPath(),
            static::SELECTOR    => $this->getModuleNameSelector(),
        ];
    }

    /**
     * Get Reflection
     *
     * @return ReflectionClass
     */
    final protected function getModuleReflection()
    {
        if (! $this->privateModuleReflectionClass instanceof ReflectionClass) {
            $this->privateModuleReflectionClass = new ReflectionClass($this);
        }

        return $this->privateModuleReflectionClass;
    }

    /**
     * Get Path
     *
     * @return string
     */
    final public function getModuleRealPath()
    {
        return $this->getModuleReflection()->getFileName();
    }

    /**
     * Get Name Space
     *
     * @return string
     */
    final public function getModuleNameSpace()
    {
        return $this->getModuleReflection()->getNamespaceName();
    }

    /**
     * Get ShortName of Class
     *
     * @return string
     */
    final public function getModuleShortName()
    {
        return $this->getModuleReflection()->getShortName();
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        if (!is_string($this->module_name)
            || trim($this->module_name) == ''
        ) {
            $this->module_name = $this->getModuleReflection()->getName();
        }

        return (string) $this->module_name;
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleVersion()
    {
        return (string) $this->module_version;
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleAuthor()
    {
        return (string) $this->module_author;
    }

    /**
     *{@inheritdoc}
     */
    public function getModuleAuthorUri()
    {
        return (string) $this->module_author_uri;
    }

    /**
     * Get Module URL
     *
     * @return string
     */
    public function getModuleUri()
    {
        return (string) $this->module_uri;
    }

    /**
     *{@inheritdoc}
     */
    public function getModuleDescription()
    {
        return (string) $this->module_description;
    }
}
