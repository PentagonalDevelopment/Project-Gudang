<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}

$this->headerPart();
?>
<div id="page_forgot" class="login">
    <!-- Forgot Password -->
    <div class="login__block active" id="l-forget-password">
        <div class="login__block__header palette-Purple bg">
            <i class="zmdi zmdi-account-circle"></i>
            Forgot Password?

            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="<?= base_url(get_instance()->__login_route_path);?>">Already have an account?</a>
                        <a class="dropdown-item" href="<?= base_url(get_instance()->__register_route_path);?>">Create an account</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- .login__block__header -->
        <div class="login__block__body"><?php
            generateBlockForm(
                (array) $this->getPackageFrom(\App\Factory\Form\Forgot::class)->getFormDefinition(),
                'zmdi-check',
                '<p class="mt-4">Lorem ipsum dolor fringilla enim feugiat commodo sed ac lacus.</p>'
            );
        ?></div>
        <!-- .login__block__body -->
    </div>
    <!-- .login__block -->
</div>
<?php
$this->footerPart();
