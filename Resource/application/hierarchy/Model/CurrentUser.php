<?php
namespace App\Model;

use App\Factory\VerifyRandom;
use App\Libraries\Loader;
use App\Libraries\UserMeta;
use App\Libraries\UserStatus;

/**
 * Class CurrentUser
 * @package App\Model
 *
 * @property UserStatus $l_userStatus
 * @property User       $m_user
 */
class CurrentUser extends Model
{
    const USER_SESSION_KEY = 'CurrentUser';

    const USER_LOGGED_AS_SESSION_KEY = 'LoggedUserAs';

    const USER_LAST_LOGGED_SELECTOR = 'LastLoggedUser';

    /**
     * @var User|null
     */
    protected $userData;

    /**
     * @var User|null
     */
    protected $loggedUserData;

    /**
     * {@inheritdoc}
     */
    protected function initBeforeLoad()
    {
        parent::initBeforeLoad();
    }

    /**
     * @return array
     */
    public function getUserSessionData()
    {
        $data = $this->session->userdata(self::USER_SESSION_KEY);
        if (!is_array($data)) {
            $data = [];
            $this->session->set_userdata(self::USER_SESSION_KEY, $data);
        }

        return $data;
    }

    /**
     * @return array|mixed
     */
    public function getUserSessionLoggedData()
    {
        $data = $this->session->userdata(self::USER_LOGGED_AS_SESSION_KEY);
        if (!is_array($data)) {
            $data = [];
            $this->session->unset_userdata(self::USER_LOGGED_AS_SESSION_KEY);
        }

        return $data;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getUserSessionDataBy($name, $default = null)
    {
        if (!is_string($name) || trim($name) == '') {
            return $default;
        }

        $data = $this->getUserSessionData();
        return array_key_exists($name, $data)
            ? $data[$name]
            : $default;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getUserSessionLoggedDataBy($name, $default = null)
    {
        if (!is_string($name) || trim($name) == '') {
            return $default;
        }

        $data = $this->getUserSessionLoggedData();
        return array_key_exists($name, $data)
            ? $data[$name]
            : $default;
    }

    /**
     * Check User Session
     *
     * @param bool $force
     * @return bool
     */
    final protected function checkUserSessionStatus($force = false)
    {
        $data = $this->getUserSessionData();
        if (!is_array($data)) {
            return false;
        }

        if ($this->validateSessionLogin($data)) {
            /**
             * @var User $user
             */
            $user =  $this->getPackage(User::class);
            $this->userData = !isset($this->userData) || $force
                ? $user->getByUserNameFromVerify($data[VerifyRandom::SELECTOR_USERNAME])
                : ($this->userData ?: false);
            return ! empty($this->userData);
        }

        return false;
    }

    /**
     * Check User Session
     *
     * @param bool $force
     * @return bool
     */
    final protected function checkLoggedUserSessionStatus($force = false)
    {
        if (!$this->isLogin()) {
            return false;
        }

        $data = $this->getUserSessionLoggedData();
        if (!is_array($data)) {
            return false;
        }

        if ($this->validateSessionLogin($data)) {
            /**
             * @var User $user
             */
            $user =  $this->getPackage(User::class);
            $this->loggedUserData = ! isset($this->loggedUserData) || $force
                ? $user->getByUserNameFromVerify($data[VerifyRandom::SELECTOR_USERNAME])
                : ($this->loggedUserData ?: false);
            // must be logged
            if ($this->loggedUserData
                && ! $this->isSuperAdmin()
                && $this->getUserStatusObject()->getStatus() != UserStatus::STATUS_ACTIVE
            ) {
                $this->session->unset_userdata(self::USER_LOGGED_AS_SESSION_KEY);
                $this->loggedUserData = false;
                return false;
            }

            return ! empty($this->loggedUserData);
        }

        return false;
    }

    /**
     * Validate Session
     *
     * @param array $data
     * @return bool
     */
    private function validateSessionLogin(array $data)
    {
        return VerifyRandom::verifyToken($data);
    }

    /**
     * @param bool $force
     * @return null|User
     */
    final public function getUserData($force = false)
    {
        if (! $this->checkUserSessionStatus($force)) {
            return null;
        }

        return $this->userData;
    }

    /**
     * @param bool $force
     * @return null|User
     */
    final public function getLoggedUserData($force = false)
    {
        if (!$this->isLogin()) {
            return null;
        }

        if (!$this->checkLoggedUserSessionStatus($force)) {
            return null;
        }

        return $this->loggedUserData;
    }

    /**
     * @param string $string
     * @param mixed $default
     * @return mixed|null
     */
    public function getUserDataBy($string, $default = null)
    {
        return $this->userData ? $this->userData->get($string, $default) : $default;
    }

    /**
     * @param string $string
     * @param mixed $default
     * @return mixed|null
     */
    public function getLoggedUserDataBy($string, $default = null)
    {
        return $this->userData && $this->loggedUserData
            ? $this->loggedUserData->get($string, $default)
            : $default;
    }

    /**
     * @return UserStatus
     */
    public function getUserStatusObject()
    {
        return $this->getPackage(UserStatus::class)->newInstance($this->getUserData());
    }

    /**
     * @return UserStatus
     */
    public function getLoggedUserStatusObject()
    {
        return $this->getPackage(UserStatus::class)->newInstance($this->getLoggedUserData());
    }

    /**
     * Get Login Status
     *
     * @return bool
     */
    public function isLogin()
    {
        return $this->getUserData() ? true : false;
    }

    /**
     * Get Login Status
     *
     * @return bool
     */
    public function isLoggedLogin()
    {
        return $this->getLoggedUserData() ? true : false;
    }

    /**
     * Create Session From User
     *
     * Only Internal User
     * @param User $user
     * @return bool|null
     */
    public function createSession(User $user)
    {
        if ($this->isLogin()) {
            return null;
        }

        if ($user->isResultFromDatabase()) {
            $userData = $this->getUserSessionData();
            /**
             * @var VerifyRandom $verify
             */
            $verify = Loader::get(VerifyRandom::class);
            $newArray = array_merge($userData, $verify::generateToken($user));
            $this->session->set_userdata(self::USER_SESSION_KEY, $newArray);
            $this->userData = $user;
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        if (!$this->isLogin()) {
            return false;
        }

        return $this->getUserStatusObject()->getLevel() == -1;
    }

    /**
     * Create Session From User
     *
     * Only Internal User
     * @param User $user
     * @return bool|null
     */
    public function createLoggedSession(User $user)
    {
        if (! $this->isLogin() || ! $this->getUserData()) {
            return false;
        }

        if ($this->getUserData()->get('user_name') == $user->get('user_name')) {
            return null;
        }

        if ($user->isResultFromDatabase()) {
            $userData = $this->getUserSessionLoggedData();
            /**
             * @var VerifyRandom $verify
             */
            $verify = Loader::get(VerifyRandom::class);
            $newArray = array_merge($userData, $verify::generateToken($user));
            $this->session->set_userdata(self::USER_SESSION_KEY, $newArray);
            $this->loggedUserData = $user;
            return true;
        }

        return false;
    }

    /**
     * Destroy
     * @return bool
     */
    public function destroyLogin()
    {
        if ($this->isLogin()) {
            $userData = $this->getUserSessionData();
            unset(
                $userData[VerifyRandom::SELECTOR_PUBLIC_TOKEN],
                $userData[VerifyRandom::SELECTOR_USERNAME],
                $userData[VerifyRandom::SELECTOR_TIME],
                $userData[VerifyRandom::SELECTOR_TOKEN]
            );
            $this->session->unset_userdata(self::USER_SESSION_KEY);
            $this->session->set_userdata(self::USER_SESSION_KEY, $userData);
            $this->destroyLoggedLogin();
            $this->userData = null;
            return true;
        }

        return false;
    }

    /**
     * @return UserRole|null
     */
    public function getRole()
    {
        if ($this->userData instanceof User) {
            return $this->userData->getRole();
        }

        return null;
    }

    /**
     * @return UserMeta|UserMeta[]|null
     */
    public function getMeta()
    {
        if ($this->userData instanceof User) {
            return $this->userData->getMeta();
        }

        return null;
    }

    /**
     * @return UserRole|null
     */
    public function getLoggedRole()
    {
        if ($this->loggedUserData instanceof User) {
            return $this->loggedUserData->getRole();
        }

        return null;
    }

    /**
     * @return UserMeta|UserMeta[]|null
     */
    public function getLoggedMeta()
    {
        if ($this->loggedUserData instanceof User) {
            return $this->loggedUserData->getMeta();
        }

        return null;
    }

    /**
     * @return bool
     */
    public function destroyLoggedLogin()
    {
        if ($this->isLoggedLogin()) {
            $userData = $this->getUserSessionData();
            $userData[self::USER_LAST_LOGGED_SELECTOR] = $this->loggedUserData->get('user_name');
            $this->session->set_userdata(self::USER_SESSION_KEY, $userData);
            $this->session->unset_userdata(self::USER_LOGGED_AS_SESSION_KEY);
            $this->loggedUserData = null;
            return true;
        }

        return false;
    }
}
