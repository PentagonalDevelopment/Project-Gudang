<?php
/**
 * @var MY_Loader $this
 */
if (!defined('BASEPATH')) {
    exit;
}
$this->headerPart();
?>
<div id="page_login" class="login">

    <!-- Login -->
    <div class="login__block active" id="l-login">
        <div class="login__block__header">
            <i class="zmdi zmdi-account-circle"></i>
            Hi there! Please Sign in
            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="<?= base_url(get_instance()->__register_route_path);?>">Create an account</a>
                        <a class="dropdown-item" href="<?= base_url(get_instance()->__forgot_route_path);?>">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- .login__block__header -->
        <div class="login__block__body"><?php
            generateBlockForm(
                    (array) $this->getPackageFrom(\App\Factory\Form\Login::class)->getFormDefinition(),
                    'zmdi-long-arrow-right'
            );
        ?></div>
        <!-- .login__block__body -->
    </div>
    <!-- .login__block -->
</div>

<?php
$this->footerPart();
